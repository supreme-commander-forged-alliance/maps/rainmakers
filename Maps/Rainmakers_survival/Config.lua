
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

do 

    -- make the config if it doesn't exist
    if not ScenarioInfo.Config then
        ScenarioInfo.Config = { };
    end

    -- set defaults, this is important in the Steam FA version of the game
    if (ScenarioInfo.Options.RainmakersObjectives == nil) then
        ScenarioInfo.Options.RainmakersObjectives = 2;
        WARN("Options: RainmakersObjectives is not set.");
    end

    if (ScenarioInfo.Options.RainmakersInitialStaticDefenses == nil) then
        ScenarioInfo.Options.RainmakersInitialStaticDefenses = 3;
        WARN("Options: RainmakersInitialStaticDefenses is not set.");
    end

    if (ScenarioInfo.Options.RainmakersStartupTime == nil) then
        ScenarioInfo.Options.RainmakersStartupTime = 2;
        WARN("Options: RainmakersStartupTime is not set.");
    end

    
    if (ScenarioInfo.Options.RainmakersCommanders == nil) then
        ScenarioInfo.Options.RainmakersCommanders = 1;
        WARN("Options: RainmakersCommanders is not set.");
    end

    -- the number of outposts
    local outposts = { 0, 2, 5}

    -- the time we need to survive to complete the primrary objective: protecting the artilleries
    local objectiveTime = { 1220, 1340, 1480, 1610 };

    -- the time before the primary objective starts
    local startupTime = { 265, 200, 145, 70 };

    -- the number of commanders that must survive
    local commanders = { 'all', 'one' }

    -- save the actual values in the config
    ScenarioInfo.Config.GeneralSettings = {
        Commanders = commanders[ScenarioInfo.Options.RainmakersCommanders],
        InitialStaticDefenses = outposts[ScenarioInfo.Options.RainmakersInitialStaticDefenses],
        Difficulty = ScenarioInfo.Options.RainmakersObjectives,
        ObjectiveTime = objectiveTime[ScenarioInfo.Options.RainmakersObjectives] + math.floor(Random() * 60 - 30),
        StartupTime = startupTime[ScenarioInfo.Options.RainmakersStartupTime] + math.floor(Random() * 15),
    }

    -- save a more human-friendly version in the config
    local outpostsFriendly      = { 'Few', 'Moderate', 'Many' };
    local objectiveTimeFriendly = { 'Easy', 'Normal', 'Heroic', 'Legendary' };
    local startupTimeFriendly   = { 'Easy', 'Normal', 'Heroic', 'Legendary' };

    ScenarioInfo.Config.GeneralSettingsFriendly = {
        InitialStaticDefenses = outpostsFriendly[ScenarioInfo.Options.RainmakersInitialStaticDefenses],
        ObjectiveTime = objectiveTimeFriendly[ScenarioInfo.Options.RainmakersObjectives],
        StartupTime = startupTimeFriendly[ScenarioInfo.Options.RainmakersStartupTime],
    }

end