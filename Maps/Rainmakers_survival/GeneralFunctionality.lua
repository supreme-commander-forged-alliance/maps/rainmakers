
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- VECTOR MATH --

-- constructs a basic vector.
function Vector(x, y, z)
    if not y == 0 then
        return { x, y, z };
    else
        return {x, GetSurfaceHeight(x, z), z};
    end
end

-- constructs a random vector between the domain (-1, 1). Y value is 0.
function VectorRandom()
    return {(Random() - 0.5) * 2, 0, (Random() -0.5) * 2 }
end

-- adds two vectors to one another.
function VectorAddition(v1, v2)
    return { v1[1] + v2[1], v1[2] + v2[2], v1[3] + v2[3]}
end

-- subtracts two vectors from one another.
function VectorSubtraction(v1, v2)
    return { v1[1] - v2[1], v1[2] - v2[2], v1[3] - v2[3]}
end

-- subtracts two vectors from one another.
function VectorSubtract(v1, v2)
    return VectorSubtraction(v1, v2);
end

-- returns the vector multiplied by the given scalar.
function VectorScalarMultiply(v, s)
    return {v[1] * s, v[2] * s, v[3] * s};
end

-- returns the length of a vector.
function VectorLength(v)
    return math.sqrt(v[1] * v[1] + v[2]*v[2] + v[3]*v[3]);
end

function VectorLength2D(v)
    return math.sqrt(v[1] * v[1] + v[3] * v[3]);
end

-- returns the distance between the two vectors.
function VectorDistance(v1, v2)
    local v = VectorSubtraction(v1, v2);
    return VectorLength(v);
end

-- returns the normalized vector.
function VectorNormalize(v)
    local length = VectorLength2D(v);
    return VectorScalarMultiply(v, 1 / length);
end

-- returns a vector that has a 90 degree corner on the 2D plane.
function Vector2DCross(v)
    return { -v[3], v[2], v[1] };
end

function VectorEqual(v1, v2)
    return v1[1] == v2[1] and v1[2] == v2[2] and v1[3] == v2[3];
end

function VectorEqual2D(v1, v2)
    return v1[1] == v2[1] and v1[3] == v2[3];
end

--------------------------------------------------------------------------------
-- USEFUL VECTOR RELATED FUNCTIONS --

-- chooses a random point around a given target.
function RandomPointAroundTarget(target, radius)

    -- compute a random position within a circle.
    local random = VectorRandom();
    local normalized = VectorNormalize(random);
    local scaled = VectorScalarMultiply(normalized, Random() * radius);

    -- add this position to the target.
    return VectorAddition(target, scaled);
end

function RandomPointAroundTargetAtEdge(target, radius)

    -- compute a random position at the edge of a circle.
    local random = VectorRandom();
    local normalized = VectorNormalize(random);
    local scaled = VectorScalarMultiply(normalized, radius);

    -- at this to the target.
    return VectorAddition(target, scaled);
end

--------------------------------------------------------------------------------
-- PLATOON FUNCTIONS --

-- checks whether or not the given platoon is still fully alive.
function PlatoonAliveCheck(platoon)

    local alive = false;

    -- go over every unit: if one is alive then so is the platoon.
    units = platoon:GetPlatoonUnits();
    for k, unit in units do
        if not unit.Dead then
            alive = true;
            break;
        end
    end

    return alive;
end