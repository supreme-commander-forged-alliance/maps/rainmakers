--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- analyses all units (and all categories) of an army.
function AnalyseUnitsOfArmy(strArmy)

    -- return it all - we do not process any of it here.
    return {
        air = { AnalyseAirUnitsOfArmy(strArmy) },
        structure = { AnalyseStructureUnitsOfArmy(strArmy) },
        ground = { AnalyseGroundUnitsOfArmy(strArmy) }
    };
end

-- analyses all aerial units of an army.
function AnalyseAirUnitsOfArmy(strArmy)

    -- retrieve the actual brain object.
    local brain = GetArmyBrain(strArmy);

    -- retrieve all air units, then filter that list seperately.
    local air = brain:GetListOfUnits(categories.AIR, false);
    local airToAir = EntityCategoryFilterDown(categories.ANTIAIR, air);
    local airToGround = EntityCategoryFilterDown(categories.BOMBER + categories.DIRECTFIRE, air);
    local airToNaval = EntityCategoryFilterDown(categories.ANTINAVY, air);

    return {    
        antiAir = airToAir,
        antiGround = airToGround,
        antiNaval = airToNaval,
    };

end

-- analyses all ground units of an army.
function AnalyseGroundUnitsOfArmy(strArmy)

    -- retrieve the actual brain object.
    local brain = GetArmyBrain(strArmy);

    -- retrieve all ground categories.
    local ground = brain:GetListOfUnits(categories.GROUND, false);
    local groundToAir = EntityCategoryFilterDown(categories.ANTIAIR, structures);
    local groundTank = EntityCategoryFilterDown(categories.DIRECTFIRE, structures);
    local groundArtillery = EntityCategoryFilterDown(categories.INDIRECTFIRE, structures);
    local groundHover = EntityCategoryFilterDown(categories.HOVER + categories.OVERLAYANTINAVY, structures);

    return { 
        antiAir = groundToAir,
        antiGround = groundTank,
        antiNaval = groundHover,
        artillery = groundArtillery,
    };

end

-- analyses all structure units from an army.
function AnalyseStructureUnitsOfArmy(strArmy)

    -- retrieve the actual brain object.
    local brain = GetArmyBrain(strArmy);

    -- retrieve all structure categories.
    local structures = brain:GetListOfUnits(categories.STRUCTURE, false);
    local structureAntiAir = EntityCategoryFilterDown(categories.ANTIAIR, structures);
    local structureAntiGround = EntityCategoryFilterDown(categories.DIRECTFIRE, structures);
    local structureArtillery = EntityCategoryFilterDown(categories.INDIRECTFIRE, structures);
    local structureAntiNaval = EntityCategoryFilterDown(categories.ANTINAVY, structures);

    return {
        antiAir = structureAntiAir,
        antiGround = structureAntiGround,
        antiNaval = structureAntiNaval,
        artillery = structureArtillery,
    };
end

function AnalysePriorityTarget(strArmy)

    -- analyses the following units:
    -- Raders
    -- Commanders

    -- retrieve the actual brain object.
    local brain = GetArmyBrain(strArmy);

    -- retrieve potentional interesting targets.
    local commanders = brain:GetListOfUnits(categories.COMMAND, false);
    local radars = brain:GetListOfUnits(categories.STRUCTURE * categories.RADAR, false);

end

function AnalyseForGround(strArmy)

    -- retrieve the actual brain object.
    local brain = GetArmyBrain(strArmy);

    -- retrieve all structures of this army.
    local structures = brain:GetListOfUnits(categories.STRUCTURE, false);
    
    -- retrieve potentional interesting targets.
    return {
        -- useful structures that, if taken down, has a serious impact.
        power = EntityCategoryFilterDown(categories.ENERGYPRODUCTION, structures);
        mass = EntityCategoryFilterDown(categories.MASSPRODUCTION, structures);
        production = EntityCategoryFilterDown(categories.FACTORY, structures);

        -- potentional structures to be taken down via artillery.
        defense = EntityCategoryFilterDown(categories.DIRECTFIRE, structures);
        shield = EntityCategoryFilterDown(categories.SHIELD, structures);
    }

end

function AnalyseTargetsForGround(strArmy)

    productionTargets = { };
    defensiveTargets = { };

    -- retrieve all assets of the given player.
    assets = AnalyseForGround(strArmy);

    -- analyse every single one of them!
    for k, unit in assets.power do

        -- IS THIS TOO BASIC?

        -- retrieve all units in the area.
        local otherUnits = GetEnemyUnitsInRectangle(unit:GetPosition(), 10);

        -- retrieve all defensive units.
        defense = EntityCategoryFilterDown(categories.STRUCTURE * categories.DIRECTFIRE, structures);
        shield = EntityCategoryFilterDown(categories.STRUCTURE * categories.SHIELD, structures);

        -- compute the number of defense points.
        dt1 = EntityCategoryFilterDown(categories.TECH1, defense);
        dt2 = EntityCategoryFilterDown(categories.TECH2, defense);
        dt3 = EntityCategoryFilterDown(categories.TECH3, defense);
        local dPoints = table.getn(dt1) * 2 + table.getn(dt2) * 4 + table.getn(dt3) * 8;

        -- compute the number of nearby shields.
        st2 = EntityCategoryFilterDown(categories.TECH2, shield);
        st3 = EntityCategoryFilterDown(categories.TECH3, shield);
        local sPoints = table.getn(st2) * 4 + table.getn(st3) * 8;

        -- comput the threat chance.
        local threat = dPoints * (sPoints + 1);

        table.insert(productionTargets,  {
            threat = threat,
            unit = unit, 
        })

    end

    -- do something about measuring threats.
    return productionTargets, defensiveTargets;

end

function GetEnemyUnitsInRectangle(position, radius)
	local x1 = position.x - radius
	local y1 = position.y - radius
	local x2 = position.x + radius
    local y2 = position.y + radius
    
    return GetUnitsInRect( Rect(x1, z1, x2, z2) )

end