
local AIAttackUtils = import('/lua/ai/aiattackutilities.lua');

local HeapAsset = import(ScenarioInfo.path .. "Algorithms/Heap.lua");
local BezierAsset = import(ScenarioInfo.path .. "Algorithms/BezierSpline.lua");

-- we want to roughly know what a cell contains:
-- a commander
-- mobile units, and their tech
-- static defenses, and their tech
-- other structures

-- and we do not count:
-- walls

Grid = { };
Grid.__index = Grid;

function Grid:Setup(lowerMarker, higherMarker, precision)

    local grid = { };
    setmetatable(grid, Grid);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- store all the parameters

    grid.areaLeftTop = lowerMarker;           
    grid.areaRightBottom = higherMarker;    
    grid.precision = precision;

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- prepare the grid

    grid.stepSizeX = (higherMarker[1] - lowerMarker[3]) / (precision - 1);
    grid.stepSizeZ = (higherMarker[1] - lowerMarker[3]) / (precision - 1);
    grid.precisionInv = 1 / precision;   

    -- create the nodes themselves
    grid.nodes = { };
    for z = 0, precision - 1 do  
        for x = 0, precision - 1 do
            local index = grid:GetIndex(x, z);
            grid.nodes[index] = grid:ConstructNode(x, z);
        end
    end

    -- pre-compute the neighbours
    for z = 0, precision - 1 do  
        for x = 0, precision - 1 do
            local index = grid:GetIndex(x, z);
            local node = grid.nodes[index];
            local neighbours = grid:NeighboursOfNode(node);
            node.neighbours = neighbours;
        end
    end

    return grid;

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- constructs a node with typical properties

function Grid:ConstructNode(x, z)

    return { 
        x = x,              -- the x coordinate of this node
        z = z,              -- the y coordinate of this node
        neighbours = { },   -- the neighbours of this node

        hazard = 0,         -- mass that is considered hazardous (turrets, tanks)
        oppertunity = 0,    -- mass that is considered as an oppertunity (economy, factories)
        commanders = { },   -- the commanders connected to this node
    }
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- computes the index in the grid

function Grid:GetIndex(x, z) 
    return x + self.precision * z + 1;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- checks whether the given indices are inside the grids

function Grid:IndexInGrid(x, z)
    local precision = self.precision;

    local bx = x >= 0 and x < precision;
    local bz = z >= 0 and z < precision;
    return bx and bz;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- given a point and a grid, compute the corresponding node

function Grid:GetNode(point)

    -- make coordinates relative to the grid
    local gx = point[1] - self.areaLeftTop[1];
    local gz = point[3] - self.areaLeftTop[3];

    -- turn them into (integral) grid coordinates
    local x = math.floor(gx / self.stepSizeX + 0.5);
    local z = math.floor(gz / self.stepSizeZ + 0.5)

    -- retrieve it, harrr.
    local index = self:GetIndex(x, z);
    return self.nodes[index];

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- computes the neighbours of a node

function Grid:NeighboursOfNode(node)

    local nodes = self.nodes;
    local precision = self.precision;

    local neighbours = { }

    -- check its neighbours
    for x = 1, 3 do 
        for z = 1, 3 do 

            -- don't take the center node
            if not (x == 2 and z == 2) then

                local nx = (x + node.x - 2);
                local nz = (z + node.z - 2);

                -- check if it is within the bounds of the grid
                if nx >= 0 and nx < precision then
                    if nz >= 0 and nz < precision then 
                        local index = self:GetIndex(nx, nz);
                        table.insert(neighbours, nodes[index]);
                    end
                end
            end
        end
    end

    return neighbours;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- given a node and a grid, compute the corresponding point

function Grid:NodeToCoordinates(node)

    -- make coordinates relative to the grid
    local gx = node.x * self.stepSizeX;
    local gz = node.z * self.stepSizeZ;

    -- make coordinates relative to the world
    local x = gx + self.areaLeftTop[1]
    local z = gz + self.areaLeftTop[3]

    -- return it, hurrr.
    return {x, GetSurfaceHeight(x, z),  z};
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- constructs a target with typical target information

function Grid:ToTarget(node)
    -- convert the node to coordinates
    local position = self:NodeToCoordinates(node);

    -- copy the data
    return { 
        position = position,
        oppertunity = node.oppertunity,
        hazard = node.hazard,
        commanders = node.commanders
    };
end

function Grid:TickCopy(grids)

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- check whether the grids align

    local allIsGood = true;
    for k, grid in grids do 
        if not grid.precision == self.precision then
            error("Grid:TickCopy: Precisions do not align");
            allIsGood = false;
        end

        if not table.getn(grid.nodes) == table.getn(self.nodes) then
            error("Grid:TickCopy: Number of nodes do not align");
            allIsGood = false;
        end

        if not grid.areaLeftTop == self.areaLeftTop then
            WARN("Grid:TickCopy: left-top corners aren't the same: " .. repr(grid.areaLeftTop) .. " <-> " .. repr(self.areaLeftTop));
            allIsGood = false;
        end

        if not grid.areaRightBottom == self.areaRightBottom then
            WARN("Grid:TickCopy: bottom-right corners aren't the same: " .. repr(grid.areaRightBottom) .. " <-> " .. repr(self.areaRightBottom));
            allIsGood = false;
        end
    end

    if allIsGood then
        LOG("Starting a grid copy tick for " .. table.getn(grids) .. " grids.");
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- completely clears out the previous tick

    function ClearNodes(nodes)
        for k, node in nodes do 
            node.oppertunity = 0;
            node.hazard = 0;
            node.commanders = { }
        end
    end
    
    -- make sure all ticks are not in sync
    WaitSeconds(Random() * 5.0);

    while true do
        -- wait up a bit
        WaitSeconds(4.0);

        -- clear the previous iteration
        ClearNodes(self.nodes);

        -- merge the grids
        for k, grid in grids do 
            for l, other in grid.nodes do 
                local node = self.nodes[l];
                node.hazard = node.hazard + other.hazard;
                node.oppertunity = node.oppertunity + other.oppertunity;
                node.commanders = table.cat(node.commanders, other.commanders);
            end
        end

        -- finilize the state with pre-computing the oppertunities and hazards
        self.oppertunities = self:FindGlobalOppertunities();
        self.hazards = self:FindGlobalHazards();
    end
end

function Grid:Tick( 
    armies,
    hazardousCategories,
    oppertunityCategories
)

    LOG("Starting a grid tick for " .. repr(armies) .. ".");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- completely clears out the previous tick

    function ClearNodes(nodes)
        for k, node in nodes do 
            node.oppertunity = 0;
            node.hazard = 0;
            node.commanders = { }
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- finds the weapon on a unit with the most range. If no weapons, range will be 0.

    local validWeaponCategories = {
        'Death',
        'Missile',
        'Anti Air',
        'Anti Navy',
        'Artillery',
        'Defense'
    }
    
    function FindWeaponWithMostRange(unit)
    
        local range = 0;
        local weapons = unit:GetBlueprint().Weapon;
        if weapons then 
            for k, weapon in weapons do 
                -- check if it is the right category, e.g., don't take 'Anti Air' into account
                local category = weapon.WeaponCategory;
                if not table.find(validWeaponCategories, category) then 
                    -- multiply it to tweak the range slightly
                    local distance = weapon.MaxRadius;
                    if distance > range then 
                        range = distance;
                    end
                end
            end
        end
    
        return range;
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- computes the threats on the grid

    function UpdateHazards(grid, units)
        for k, unit in units do 

            -- compute the hazard value
            local fraction = unit:GetFractionComplete()
            local mass = fraction * unit:GetBlueprint().Economy.BuildCostMass
            
            -- find the center node 
            local point = unit:GetPosition();
            local node = grid:GetNode(point);
            local range = FindWeaponWithMostRange(unit);

            -- if the unit has no weapon, then no threat is applied
            if range > 0 then 

                -- find all neighbouring nodes that are in range
                local rx = math.floor(range / grid.stepSizeX);
                local rz = math.floor(range / grid.stepSizeZ);

                -- give the centering node atleast halve of the threat value
                node.hazard = node.hazard + 0.5 * mass;

                if rx * rz > 0 then

                    -- spread the other half of mass value over the set of nodes 
                    local inv = 0.5 / ((2 * rx) * (2 * rz))

                    -- compute coordinates of neighbours in range
                    for z = -rz, rz, 1 do 
                        local gz = node.z + z;
                        for x = -rx, rx, 1 do 
                            local gx = node.x + x;
                            -- check if the neighbour exists
                            if grid:IndexInGrid(gx, gz) then 
                                local index = grid:GetIndex(gx, gz);
                                local neighbour = grid.nodes[index]; 
                                neighbour.hazard = neighbour.hazard + mass * inv;
                            end
                        end
                    end
                end
            end
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- computes the oppertunities on the grid

    function UpdateOppertunities(grid, units)
        for k, unit in units do 

            -- compute the oppertunity value
            local fraction = math.min(1.0, 2 * unit:GetFractionComplete())
            local mass = fraction * unit:GetBlueprint().Economy.BuildCostMass

            -- retrieve the node
            local point = unit:GetPosition();
            local node = grid:GetNode(point);

            -- update it
            node.oppertunity = node.oppertunity + mass;
            if EntityCategoryContains(categories.COMMAND, unit) then
                table.insert(node.commanders, unit)
            end
        end
    end

    -- make sure all ticks are not in sync
    WaitSeconds(Random() * 5.0);

    while true do
        -- wait up a bit
        WaitSeconds(4.0);

        -- clear out previous state
        ClearNodes(self.nodes);

        -- for every army, update the new state
        for k, army in armies do 

            local brain = GetArmyBrain(army);

            -- compute the new state
            local hazards = brain:GetListOfUnits(hazardousCategories, false);
            UpdateHazards(self, hazards);

            local oppertunities = brain:GetListOfUnits(oppertunityCategories, false);
            UpdateOppertunities(self, oppertunities);
        end

        -- finilize the state with pre-computing the oppertunities and hazards
        self.oppertunities = self:FindGlobalOppertunities();
        self.hazards = self:FindGlobalHazards();
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Finds all oppertunities on the grid. The largest oppertunity is at the start of the array.

function Grid:FindGlobalOppertunities()

    -- find all nodes that have some value
    local candidates = { }
    for k, node in self.nodes do 
        if node.oppertunity > 0 then
            table.insert(candidates, node);
        end
    end

    -- sort them on the amount of oppertunity using quicksort O(nlgn)
    table.sort(
        candidates, 
        function(a, b)        
            return a.oppertunity > b.oppertunity;
        end 
    );

    -- convert them to targets
    local oppertunities = { };
    for k, candidate in candidates do 
        local oppertunity = self:ToTarget(candidate);
        table.insert(oppertunities, oppertunity);
    end

    -- return that stuff
    return oppertunities;

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- retrieves a random high oppertunity

-- assumes there are oppertunities
-- assumes the oppertunities are in order
function Grid:GetHighOppertunity () 
    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = 1;
    local high = math.max(1, math.floor(count / 10.0));
    
    local index = low + math.floor(Random() * (high - low));

    return oppertunities[index];
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- retrieves a random mediocre oppertunity

-- assumes there are oppertunities
-- assumes the oppertunities are in order
function Grid:GetMediocreOppertunity () 
    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = math.max(1, math.floor(count / 10.0));
    local high = math.max(1, math.floor(count / 4.0));
    
    local index = low + math.floor(Random() * (high - low));
    return oppertunities[index];
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- retrieves an a random low oppertunity

-- assumes there are oppertunities
-- assumes the oppertunities are in order
function Grid:GetLowOppertunity () 
    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = math.max(1, math.floor(count / 4.0));
    local high = count;
    
    local index = low + math.floor(Random() * (high - low));
    return oppertunities[index];
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Computes the oppertunity that is nearest.

function RetrieveNearest(point, oppertunities, low, high)

    -- sane default values
    low = low or 1
    high = high or table.getn(oppertunities);

    -- assume the first one is the lowest
    local nearest = oppertunities[low];
    local distance = 1024 * 1024;

    -- verify and correct assumption
    for k = low, high, 1 do 

        local oppertunity = oppertunities[k];

        local dis = VDist2Sq(
            point[1], point[3], 
            oppertunity.position[1], oppertunity.position[3]
        );

        if dis < distance then 
            nearest = oppertunity;
            distance = dis 
        end

    end

    return nearest;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Checks whether some oppertunity is available

function Grid:OppertunitiesAvailable()
    return table.getn(self.oppertunities) > 0;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Checks whether some oppertunity is available

function Grid:HazardsAvailable()
    return table.getn(self.hazards) > 0;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Retrieves the nearest high-valued oppertunity at the 1/10th percentile

function Grid:GetNearestHighOppertunity (point) 

    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = 1;
    local high = math.max(1, math.floor(count / 10.0));

    return RetrieveNearest(point, oppertunities, low, high);
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Retrieves the nearest mediocre-valued oppertunity between the 1/10th and 2.5/10th percentile

function Grid:GetNearestMediocreOppertunity (point) 

    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = math.max(1, math.floor(count / 10.0));
    local high = math.max(1, math.floor(count / 4.0));

    return RetrieveNearest(point, oppertunities, low, high);
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Retrieves the nearest low-valued oppertunity between the 2.5/10th and 10/10th percentile

function Grid:GetNearestLowOppertunity (point) 

    local oppertunities = self.oppertunities;          -- assumption: these are sorted
    local count = table.getn(oppertunities);

    local low = math.max(1, math.floor(count / 4.0));
    local high = count;

    return RetrieveNearest(point, oppertunities, low, high);
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Finds all hazards on the grid. The largest threat is at the start of the array.

function Grid:FindGlobalHazards()

    -- find all nodes that have some value
    local candidates = { }
    for k, node in self.nodes do 
        if node.hazard > 0 then
            table.insert(candidates, node);
        end
    end

    -- sort them on how hazardous they are using quicksort O(nlgn)
    table.sort(
        candidates, 
        function(a, b)        
            return a.hazard > b.hazard;
        end 
    );

    -- convert them to targets
    local hazards = { };
    for k, candidate in candidates do 
        local hazard = self:ToTarget(candidate);
        table.insert(hazards, hazard);
    end

    -- return that stuff
    return hazards;

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Finds all local hazards on the grid, surrounding the point. The largest hazard is at the 
-- start of the array.

function Grid:FindLocalHazards(point, range, threshold)

    -- compute the squared range, used later for comparison as a small optimalisation
    local rangeSquared = range * range;

    -- retrieve the hazards from the grid
    local candidates = self.hazards;          -- assumption: these are sorted
    local hazards = { };
    local total = 0

    -- go over all the candidates, keep the ones that are within range and above the threshold
    if candidates then 
        for k, candidate in candidates do 
            if candidate.hazard > threshold then 

                local distanceSquared = VDist2Sq(
                    point[1], point[3],
                    candidate.position[1], candidate.position[3]
                );

                if distanceSquared < rangeSquared then 
                    total = total + candidate.hazard
                    table.insert(hazards, candidate);
                end
            end
        end
    end

    return hazards, total
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Finds all local oppertunities on the grid, surrounding the point. The largest oppertunity 
-- is at the start of the array.

function Grid:FindLocalOppertunities(point, range, threshold)

    -- compute the squared range, used later for comparison as a small optimalisation
    local rangeSquared = range * range;

    -- retrieve the hazards from the grid
    local candidates = self.oppertunities;          -- assumption: these are sorted
    local oppertunities = { };

    -- go over all the candidates, keep the ones that are within range and above the threshold
    if candidates then 
        for k, candidate in candidates do 
            if candidate.oppertunity > threshold then 

                local distanceSquared = VDist2Sq(
                    point[1], point[3],
                    candidate.position[1], candidate.position[3]
                );

                if distanceSquared < rangeSquared then 
                    table.insert(oppertunities, candidate);
                end
            end
        end
    end

    return oppertunities;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Draws all the nodes of the grid.

function Grid:ForkDrawNodes( color)

    local oc = color or "ffffff";

    while true do 

        -- keep drawing them every simulation step
        WaitSeconds(0.1);

        for k, node in self.nodes do
            local position = self:NodeToCoordinates(node);
            DrawCircle(position, 1, oc );
        end
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Draws all the oppertunities on the grid.

function Grid:ForkDrawOppertunities(color)

    local oc = color or "999999";

    while true do 

        -- keep drawing them every simulation step
        WaitSeconds(0.1);
        if self.oppertunities then
            for k, candidate in self.oppertunities do 
                DrawCircle( candidate.position, 0.25 * math.max(1, math.log(candidate.oppertunity)) - 0.15, oc );
            end
        end
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Draws all the hazards on the grid.

function Grid:ForkDrawHazards(color)

    local oc = color or "ff0000";

    while true do 

        -- keep drawing them every simulation step
        WaitSeconds(0.1);
        if self.hazards then 
            for k, candidate in self.hazards do 
                DrawCircle( candidate.position, 0.25 * math.max(1, math.log(candidate.hazard)) - 0.15, oc );
            end
        end
    end
end