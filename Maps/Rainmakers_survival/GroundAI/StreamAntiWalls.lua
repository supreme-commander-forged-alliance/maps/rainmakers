
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the stream (unit) generators

    local unitGen1 = GenUnitFramework.Generator:Setup(regions, "AW1");
    local unitGen2 = GenUnitFramework.Generator:Setup(regions, "AW2");
    local unitGen3 = GenUnitFramework.Generator:Setup(regions, "AW3");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- start checking periodically

    function Check()

        local veterancy = 0;
        local army = "HOSTILESTREAM"
        local oppertunities = ScenarioInfo.GridPlayers;
        local startUpTime = ScenarioInfo.Config.GeneralSettings.StartupTime;
        local unitGens = { unitGen1, unitGen2, unitGen3 };

        WaitSeconds(startUpTime);

        -- the valid categories that we consider for computing the range of the unit
        local validWeaponCategories = {
            'Direct Fire',
            'Artillery',
            'Missile'
        }
        
        -- finds the weapon with the most range
        function FindWeaponWithMostRange(unit)
        
            local range = 0;
        
            local weapons = unit:GetBlueprint().Weapon;
            if weapons then 
                for k, weapon in weapons do 
                    -- check if it is the right category, e.g., don't take 'Anti Air' into account
                    local category = weapon.WeaponCategory;
                    if table.find(validWeaponCategories, category) then 
                        local distance = weapon.MaxRadius;
                        if distance > range then 
                            range = distance;
                        end 
                    end
                end
            end
        
            return range;
        end
        
        local wallsToDestroy = 3

        -- a tinker function that keeps finding the next wall once one is destroyed
        function TinkerAntiWall(unit, wall, army, wallsDestroyed)

            -- issue to attack the wall
            IssueAttack({ unit }, wall);

            -- keep track of the amount of walls we've taken out
            if not wallsDestroyed then
                wallsDestroyed = 0
            end

            -- in case the wall dies
            ScenarioFramework.CreateUnitDeathTrigger(
                function () 
                    
                    -- find the brain
                    local brain = GetArmyBrain(army)
                    local walls = brain:GetListOfUnits(categories.WALL, false)
                    local n = table.getn(walls)

                    -- no walls left or we've done our job: go and do other things
                    if n == 0 or wallsDestroyed >= wallsToDestroy then 
                        ForkThread(TinkerUnitFramework.ForkMoveAggressive, oppertunities, unit)
                        return;
                    end

                    -- choose a nearby wall at the edge of weapon range
                    local unitPosition = unit:GetPosition()
                    local unitRange = FindWeaponWithMostRange(unit)
                    local unitRange = unitRange * unitRange

                    -- convert all walls to a wall + distance table
                    local data = { };
                    for k, wall in walls do 
                        -- compute the distance
                        local wallPosition = wall:GetPosition()
                        local distance = VDist2Sq(unitPosition[1], unitPosition[3], wallPosition[1], wallPosition[3])

                        -- bundle them together
                        local information = { }
                        information.wall = wall
                        information.distance = distance
                        table.insert(data, information)
                    end

                    -- sort the table
                    table.sort(data, function(a, b) return a.distance < b.distance; end) 

                    -- choose a wall in the 10th percentile
                    local index = math.floor(Random() * n * 0.1) + 1
                    local wall = data[index].wall 

                    -- and start it all over again
                    ForkThread(TinkerAntiWall, unit, wall, army, wallsDestroyed + 1)

                end, 
                wall
            );
            
        end

        while true do

            -- hold up a bit
            WaitSeconds(60.0);

            -- determine what the highest tech level should be
            local time = startUpTime + GetGameTimeSeconds()
            local iu = math.min(3, math.floor(time / 580) + 1)

            -- keep spawning lower tech anti wall units
            for l = 1, iu do 

                -- spawn them!
                local unitGen = unitGens[l];
                for k, player in players do 
                    
                    -- get all the walls
                    local brain = GetArmyBrain(player)
                    local walls = brain:GetListOfUnits(categories.WALL, false)
                    local n = table.getn(walls)

                    -- determine the number of artilleries
                    local unitsToSpawn = 0;
                    if n > 0 then 
                        unitsToSpawn = 1 + math.floor (n / 143)
                    end

                    -- spawn the artilleries, if applicable
                    for k = 1, unitsToSpawn, 1 do 

                        -- choose a random wall
                        local wallIndex = math.floor(Random() * n) + 1 
                        local wall = walls[wallIndex];
                        local wallPosition = wall:GetPosition();

                        -- determine the closest region
                        local region = Regions.ClosestRegionToPosition(regions, wallPosition);

                        -- spawn the unit
                        local unit = GenUnitFramework.CreateRandomUnitAtRegion(
                            unitGen, 
                            army,
                            region, 
                            veterancy
                        )

                        -- make it roll :)
                        ForkThread(TinkerAntiWall, unit, wall, player)
                    end
                end
            end
        end
    end

    ForkThread(Check)

end
