
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- END OF TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local cached = false

------------------ ------------------ ------------------ ------------------ ------------------                              
-- USEFULL FUNCTIONS --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- analyses all spawning regions.
function AnalyseRegions()

    -- don't do work twice
    if cached then 
        return table.deepcopy(cached)
    end

    local areaTable = { };

    for k = 1, 50 do

        -- attempt to retrieve the area.
        local name = "SPAWN" .. k;
        local area = Scenario.Areas[name]

        -- if not, stop. We've found all our stream groups.
        if not area then
            break;
        end

        -- construct the rectangle
        local data = area.rectangle;
        local rect = Rect(data[1],data[2],data[3],data[4]);

        -- pre compute the center
        local cx = (rect.x1 + rect.x0) * 0.5;
        local cz = (rect.y1 + rect.y0) * 0.5;
        local cy = GetSurfaceHeight(cx, cz);
        rect.center = Vector(cx, cy,  cz);

        -- pre compute the longest axis
        if math.abs(rect.x1 - rect.x0) > math.abs(rect.y1 - rect.y0) then 
            rect.p1 = Vector(rect.x1, cy, cz)
            rect.p2 = Vector(rect.x0, cy, cz)

            rect.p1[2] = GetSurfaceHeight(rect.p1[1], rect.p1[3])
            rect.p2[2] = GetSurfaceHeight(rect.p2[1], rect.p2[3])
        else
            rect.p1 = Vector(cx, cy, rect.y1)
            rect.p2 = Vector(cx, cy, rect.y0)

            rect.p1[2] = GetSurfaceHeight(rect.p1[1], rect.p1[3])
            rect.p2[2] = GetSurfaceHeight(rect.p2[1], rect.p2[3])
        end

        -- add name for debugging
        rect.name = name;

        -- store it
        table.insert(areaTable, rect);
    end

    -- cache it, return a copy
    cached = areaTable;
    return table.deepcopy(cached)
end

-- retrieves a random point within a spawning region.
function RandomPointInRegion(area)
    return GeneralFunctionality.Vector(Random() * (area.x1 - area.x0) + area.x0, 0, Random() * (area.y1 - area.y0) + area.y0);
end

function ClosestRegionToPosition(regions, position)

    -- find the closest region
    local ic = 1;
    local id = VDist2Sq(regions[1].center[1], regions[1].center[3], position[1], position[3]);

    for k, region in regions do 
        idn = VDist2Sq(region.center[1], region.center[3], position[1], position[3]);
        if idn < id then 
            ic = k;
            id = idn;
        end
    end

    -- return the region that is closest
    return regions[ic];
end

function PointsOnLongestAxis(region, n)

    local points = { }
    for k = 1, n do 
        local factor = (k - 1) / (n - 1) 
        local point = Vector(
            region.p1[1] * factor + region.p2[1] * (1 - factor), 
            region.p1[2] * factor + region.p2[2] * (1 - factor), 
            region.p1[3] * factor + region.p2[3] * (1 - factor)
        )

        table.insert(points, point)
    end

    return points
end


function DrawRegions()

    if not cached then 
        AnalyseRegions();
    end

    while true do 

        WaitSeconds(0.1)
        for k, region in cached do 
            local p1 = Vector(region.x1, 0, region.y0)
            local p2 = Vector(region.x1, 0, region.y1)
            local p3 = Vector(region.x0, 0, region.y1)
            local p4 = Vector(region.x0, 0, region.y0)

            p1[2] = GetSurfaceHeight(p1[1], p1[3])
            p2[2] = GetSurfaceHeight(p2[1], p2[3])
            p3[2] = GetSurfaceHeight(p3[1], p3[3])
            p4[2] = GetSurfaceHeight(p4[1], p4[3])


            DrawLine(p1, p2, 'cccccc')
            DrawLine(p2, p3, 'cccccc')
            DrawLine(p3, p4, 'cccccc')
            DrawLine(p4, p1, 'cccccc')
        end
    end
end

function DrawAxis()

    if not cached then 
        AnalyseRegions();
    end

    while true do 

        WaitSeconds(0.1)
        for k, region in cached do 
            DrawLine(region.p1, region.p2, 'ffffff')
        end
    end
end