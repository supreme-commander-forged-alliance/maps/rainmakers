
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the stream (unit) generators

    local unitGen1 = GenUnitFramework.Generator:Setup(regions, "S1");
    local unitGen2 = GenUnitFramework.Generator:Setup(regions, "S2");
    local unitGen3 = GenUnitFramework.Generator:Setup(regions, "S3");
    local unitGen4 = GenUnitFramework.Generator:Setup(regions, "S4");
    local unitGen5 = GenUnitFramework.Generator:Setup(regions, "S5");
    local unitGen6 = GenUnitFramework.Generator:Setup(regions, "S6");
    local unitGen7 = GenUnitFramework.Generator:Setup(regions, "S7");
    local unitGen8 = GenUnitFramework.Generator:Setup(regions, "S8");
    local unitGen9 = GenUnitFramework.Generator:Setup(regions, "S9");
    local unitGen10 = GenUnitFramework.Generator:Setup(regions, "S10");
    local unitGen11 = GenUnitFramework.Generator:Setup(regions, "S11");
    local unitGen12 = GenUnitFramework.Generator:Setup(regions, "S12");
    local unitGen13 = GenUnitFramework.Generator:Setup(regions, "S13");
    local unitGen14 = GenUnitFramework.Generator:Setup(regions, "S14");
    local unitGen15 = GenUnitFramework.Generator:Setup(regions, "S15");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- the generic utility functions that are available 

    function TwoUtility(time)
        -- -0.000175x^{2}+0.026x+0 in https://www.desmos.com/calculator
        return -0.000175 * (time * time) + 0.026 * time + 0;
    end

    function FourUtility(time)
        -- -0.0001x^{2}+0.02x+0 in https://www.desmos.com/calculator
        return -0.0001 * (time * time) + 0.02 * time + 0;
    end

    function EightUtility(time)
        -- -0.000025x^{2}+0.01x+0 in https://www.desmos.com/calculator
        return -0.000025 * (time * time) + 0.01 * time + 0
    end

    function InfiniteUtility(time)
        -- 0.01x in https://www.desmos.com/calculator
        return 0.0025 * time;
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- some common information

    local army = "HOSTILESTREAM"
    local threshold = ScenarioInfo.Config.GroundSettings.StreamDirectQuantity;
    local veterancy = ScenarioInfo.Config.GroundSettings.StreamDirectQuality;

    local unitTinkers = { 
        TinkerUnitFramework.ForkMove,
        TinkerUnitFramework.ForkMoveAggressive,
        TinkerUnitFramework.ForkMoveSensor
    }

    local offset = 65;

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- start the streams

    local startUpTime = ScenarioInfo.Config.GeneralSettings.StartupTime;

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen1, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + 15,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen2, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (2 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen3, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (4 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen4, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (5 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen5, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (7 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen6, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (8 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen7, 
                army,
                EightUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (8 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen8, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (11 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen9, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (13 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen10, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (14 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen11, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (16 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen12, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (17 * offset) + 55,
        true
    );

    
    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen13, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (19 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen14, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (20 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen15, 
                army,
                TwoUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (22 * offset) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen14, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (23 * offset) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen15, 
                army,
                InfiniteUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (25 * offset) + 25,
        true
    );


end
