
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the stream (unit) generators

    local gent1 = GenPlatoonFramework.Generator:Setup(regions, "Tech1-1:");
    local gent1t2 = GenPlatoonFramework.Generator:Setup(regions, "Tech1-2:");
    local gent2 = GenPlatoonFramework.Generator:Setup(regions, "Tech2-2:");
    local gent2t3 = GenPlatoonFramework.Generator:Setup(regions, "Tech2-3:");
    local gent3 = GenPlatoonFramework.Generator:Setup(regions, "Tech3-3:");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- the generic utility functions that are available 

    function TwoUtility(time)
        -- -0.000175x^{2}+0.026x+0 in https://www.desmos.com/calculator
        return -0.000175 * (time * time) + 0.026 * time + 0;
    end

    function FourUtility(time)
        -- -0.0001x^{2}+0.02x+0 in https://www.desmos.com/calculator
        return -0.0001 * (time * time) + 0.02 * time + 0;
    end

    function EightUtility(time)
        -- -0.000025x^{2}+0.01x+0 in https://www.desmos.com/calculator
        return -0.000025 * (time * time) + 0.01 * time + 0
    end

    function InfiniteUtility(time)
        -- 0.01x in https://www.desmos.com/calculator
        return math.min(2, 0.004 * time);
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- some common information

    local army = "HOSTILEPLATOONS"
    local threshold = ScenarioInfo.Config.GroundSettings.PlatoonQuantity * 1.4;
    local veterancy = ScenarioInfo.Config.GroundSettings.PlatoonQuality;
    local shields = ScenarioInfo.Config.GroundSettings.MobileShieldGens;
    local stealths = ScenarioInfo.Config.GroundSettings.MobileStealthFieldGens;

    local tinkers = { 
        TinkerPlatoonFramework.MoveNearestHighOppertunity
    }

    local tinkersHigh = { 
        TinkerPlatoonFramework.MoveNearestHighOppertunity,
        TinkerPlatoonFramework.MoveHighOppertunity,
        TinkerPlatoonFramework.MoveNearestMediocreOppertunity
    }

    local tinkersMediocre = { 
        TinkerPlatoonFramework.MoveNearestMediocreOppertunity,
        TinkerPlatoonFramework.MoveNearestLowOppertunity,
        TinkerPlatoonFramework.MoveMediocreOppertunity
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- start the streams

    local startUpTime = ScenarioInfo.Config.GeneralSettings.StartupTime;
    local offset = 350;

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- tech 1 platoons

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent1, 
                army,
                EightUtility,
                tinkersHigh,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime + 75 + 0 * offset,
        true
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- tech 1 /2 platoons

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent1t2, 
                army,
                EightUtility,
                tinkersHigh,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime + 45 + 1 * offset,
        true
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- tech 2 platoons

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent2, 
                army,
                EightUtility,
                tinkersHigh,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime + 35 + 2 * offset,
        true
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- tech 2 / 3 platoons

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent2t3, 
                army,
                InfiniteUtility,
                tinkersHigh,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime + 25 + 3 * offset,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent2t3, 
                army,
                InfiniteUtility,
                tinkersMediocre,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime + 15 + 3.5 * offset,
        true
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- tech 3 platoons

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenPlatoonFramework.Tick, 
                gent3, 
                army,
                InfiniteUtility,
                tinkersHigh,
                shields,
                stealths,
                threshold,
                veterancy
            );
        end,
        startUpTime - 70 + 4 * offset,
        true
    );

end
