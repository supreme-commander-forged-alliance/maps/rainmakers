
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');
local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- computes a random spawn location.
-- todo: have pseudorandom gen as parameter.
function FindSpawnLocation(regions, prnd)

    -- compute a random point in a random area.
    local area = regions[prnd:GetValue()];
    return Regions.RandomPointInRegion(area), area;

end

-- spawns a unit of the given bp, for the given army at the given vector (position).
function SpawnUnit(bp, army, vector)

    -- spawn the unit.
    unit = CreateUnitHPR( 
        bp,				                    -- blueprint of the unit we want to build. (string)
        army,					            -- name of army. (string)
        vector[1], vector[2], vector[3],	-- position to spawn the unit at. (x, height, z)
        0, 0, 0						        -- orientation of the unit.
        );

    -- make it unselectable.
    unit:SetUnSelectable(true);

    -- return the unit.
    return unit;
end

-- spawns a platoon of the given bps, for the given army at the given vector (position).
function SpawnGroupOfUnits(bps, army, point)

    -- spawn the units that make up the platoon.
    local units = { };
    for v, unitBP in bps do
        table.insert(units, SpawnUnit(unitBP, army, point));
    end

    return units;
end

-- constructs a platoon of the given units.
function BuildPlatoon(army, units)
    
    -- make it a platoon.
    local brain = GetArmyBrain(army);
    local brainPlatoon = brain:MakePlatoon('', '');
    -- None, GrowthFormation, AttackFormation
    -- Attack, Scout
    brain:AssignUnitsToPlatoon(brainPlatoon, units, 'Attack', 'GrowthFormation')

    return brainPlatoon;
end


-- checks whether or not the given platoon is still fully alive.
function PlatoonAliveCheck(platoon)

    local alive = false;

    -- go over every unit: if one is alive then so is the platoon.
    units = platoon:GetPlatoonUnits();
    for k, unit in units do
        if not unit.Dead then
            alive = true;
            break;
        end
    end

    return alive;
end

-- checks whether or not the given platoon is completely idle.
function PlatoonIdleCheck(platoon)

    local idle = false;

    -- check if there are idle units.
    local units = platoon:GetPlatoonUnits();
    for k, unit in units do
        if not unit.Dead and unit:IsIdleState() then
            idle = true;
            break;
        end
    end

    return idle;
end

-- find all local targets near the platoon.
function PlatoonFindLocalTargets(grid, platoon, distance, thresholdMultiplier)

    -- retrieve the position on the grid.
    local position = platoon:GetPlatoonPosition();

    return GridFramework.FindLocalTargets(grid, position, platoon.value, distance, thresholdMultiplier);

end

-- TODO: fix me
-- find all local targets near the platoon.
function FindLocalTargets(grid, position, value, distance)

    -- check for local targets.
    local localTargets = GridFramework.FindLocalTargets(grid, position, distance);

    -- check whether or not they are worth going after for.
    local valuableTargets = { };
    for k, target in localTargets do
        if target.value > value then
            table.insert(valuableTargets, target);
        end
    end

    return valuableTargets;

end

function TargetEngageVector(origin, targets)

    -- add up all directions.
    local nLocal = table.getn(targets);
    local localDirection = GeneralFunctionality.Vector(0, 0, 0);
    for k, target in targets do   
        
        -- convert to grid type variable.
        local tCoords = target.coords;

        -- compute the direction.
        local direction = GeneralFunctionality.VectorSubtract(tCoords, origin);
        local normalized = GeneralFunctionality.VectorNormalize(direction);
        local perspective = GeneralFunctionality.VectorScalarMultiply(normalized, 1 / nLocal);
        localDirection = GeneralFunctionality.VectorAddition(localDirection, perspective);
    end

    if table.getn(targets) == 0 then
        return localDirection;
    end

    -- determine location to go to.
    local tNormalized = GeneralFunctionality.VectorNormalize(localDirection);
    local tPerspective = GeneralFunctionality.VectorScalarMultiply(tNormalized, 45);

    return tPerspective;

end

function TargetEngagePosition(origin, targets)

    local vector = TargetEngageVector(origin, targets);
    local target = GeneralFunctionality.VectorAddition(origin, vector);
    return target;

end

-- computes the engage vector for a platoon that has local targets.
function LocalTargetsEngageVector(position, localTargets)

    if table.getn(localTargets) == 0 then
        return position;
    end

    local nLocal = table.getn(localTargets);

    -- add up all directions.
    local localDirection = GeneralFunctionality.Vector(0, 0, 0);
    for k, target in localTargets do   
        
        -- convert to grid type variable.
        local tCoords = target.coords;

        -- compute the direction.
        local direction = GeneralFunctionality.VectorSubtract(tCoords, position);
        local normalized = GeneralFunctionality.VectorNormalize(direction);
        local perspective = GeneralFunctionality.VectorScalarMultiply(normalized, 1 / nLocal);
        localDirection = GeneralFunctionality.VectorAddition(localDirection, perspective);
    end

    -- determine location to go to.
    local tNormalized = GeneralFunctionality.VectorNormalize(localDirection);
    local tPerspective = GeneralFunctionality.VectorScalarMultiply(tNormalized, 45);

    -- return the final target.
    return GeneralFunctionality.VectorAddition(position, tPerspective);
end

-- given a platoon, it will dive the given position:
--      artilleries will be on attack move
--      all other units will be on regular move
function PlatoonDiveTarget(platoon, position)

    platoon:Stop();

    local units = platoon:GetPlatoonUnits();
    local art = EntityCategoryFilterDown(categories.ARTILLERY, units);
    local other = EntityCategoryFilterDown(categories.LAND - categories.ARTILLERY, units);
    local numberOfOther = table.getn(other);

    if numberOfOther > 5 then
        for k, unit in other do 
            local randomPosition = GeneralFunctionality.RandomPointAroundTarget(position, numberOfOther);
            IssueMove({unit}, randomPosition);
        end
    end

    IssueAggressiveMove(art, position);
    IssueAggressiveMove(other, position);

end

-- sets the veterancy of a single unit.
function SetUnitVeterancy(unit, veterancy)
    unit:SetVeterancy(veterancy);
end

-- sets the veterancy of an entire platoon.
function SetPlatoonVeterance(platoon, veterancy)
    units = platoon:GetPlatoonUnits();
    for k, unit in units do
        unit:SetVeterancy(veterancy);
    end
end

-- checks whether shields should be spawned or not.
function CheckShieldOptions()

    if ScenarioInfo.Config.GroundSettings.MobileShieldGens == 1 then

        local r = Random();
        if r < 0.5 then
            return true;
        end
    end

    return false;
end

-- checks whether stealth fields should be spawned or not.
function CheckStealthOptions()

    if ScenarioInfo.Config.GroundSettings.MobileStealthFieldGens == 1 then

        local r = Random();
        if r < 0.5 then
            return true;
        end
    end

    return false;
end