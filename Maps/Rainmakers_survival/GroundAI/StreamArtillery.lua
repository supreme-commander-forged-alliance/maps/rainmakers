
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the stream (unit) generators

    local unitGen1 = GenUnitFramework.Generator:Setup(regions, "A1");
    local unitGen2 = GenUnitFramework.Generator:Setup(regions, "A2");
    local unitGen3 = GenUnitFramework.Generator:Setup(regions, "A3");
    local unitGen4 = GenUnitFramework.Generator:Setup(regions, "A4");
    local unitGen5 = GenUnitFramework.Generator:Setup(regions, "A5");
    local unitGen6 = GenUnitFramework.Generator:Setup(regions, "A6");
    local unitGen7 = GenUnitFramework.Generator:Setup(regions, "A7");
    local unitGen8 = GenUnitFramework.Generator:Setup(regions, "A8");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- the generic utility functions that are available 

    function TwoUtility(time)
        -- -0.000175x^{2}+0.026x+0 in https://www.desmos.com/calculator
        return -0.000175 * (time * time) + 0.026 * time + 0;
    end

    function FourUtility(time)
        -- -0.0001x^{2}+0.02x+0 in https://www.desmos.com/calculator
        return -0.0001 * (time * time) + 0.02 * time + 0;
    end

    function EightUtility(time)
        -- -0.000025x^{2}+0.01x+0 in https://www.desmos.com/calculator
        return -0.000025 * (time * time) + 0.01 * time + 0
    end

    function InfiniteUtility(time)
        -- 0.01x in https://www.desmos.com/calculator
        return 0.0025 * time;
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- some common information

    local army = "HOSTILESTREAM"
    local threshold = ScenarioInfo.Config.GroundSettings.StreamArtilleryQuantity;
    local veterancy = ScenarioInfo.Config.GroundSettings.StreamArtilleryQuality;

    local unitTinkers = { 
        TinkerUnitFramework.ForkMove,
        TinkerUnitFramework.ForkMoveAggressive,
        TinkerUnitFramework.ForkMoveSensor
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- start the streams

    local startUpTime = ScenarioInfo.Config.GeneralSettings.StartupTime;

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen1, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (2 * 60) + 15,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen2, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (5 * 60) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen3, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (8 * 60) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen4, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (11 * 60) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen5, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (14 * 60) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen6, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (17 * 60) + 55,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen7, 
                army,
                FourUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (20 * 60) + 25,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            ForkThread(
                GenUnitFramework.Tick, 
                unitGen8, 
                army,
                InfiniteUtility,
                unitTinkers,
                threshold,
                veterancy
            );
        end,
        startUpTime + (23 * 60) + 55,
        true
    );

end
