
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');

local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');
local PathingFramework = import(ScenarioInfo.path .. 'GroundAI/GroundPathing.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- determine hazardous and oppertunity catagories

    local hazards = (categories.DIRECTFIRE + categories.INDIRECTFIRE + categories.DEFENSE) - categories.WALL;
    local oppertunities = (categories.ECONOMIC + categories.FACTORY + categories.ENGINEER) - categories.WALL;

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the grids

    local topLeft = ScenarioUtils.GetMarker("AIGROUND_LOW").position;
    local bottomRight = ScenarioUtils.GetMarker("AIGROUND_HIGH").position;
    local precision = 64;

    -- grid that is used by units / platoons that only care about the objective oppertunities
    ScenarioInfo.GridObjective = GridFramework.Grid:Setup(topLeft, bottomRight, precision);
    ForkThread(
        function () 
            ScenarioInfo.GridObjective:Tick(
                { "Objective" },
                hazards,
                oppertunities
            );
        end
    );

    -- grid that is used by units / platoons that only care about player oppertunities
    ScenarioInfo.GridPlayers = GridFramework.Grid:Setup(topLeft, bottomRight, precision);
    ForkThread(
        function () 
            ScenarioInfo.GridPlayers:Tick(
                players,
                hazards,
                oppertunities
            );
        end
    );

    -- grid that is generally used to determine local threats
    ScenarioInfo.GridThreats = GridFramework.Grid:Setup(topLeft, bottomRight, precision);
    ForkThread(
        function()
            ScenarioInfo.GridThreats:TickCopy({
                ScenarioInfo.GridObjective,
                ScenarioInfo.GridPlayers
            });
        end
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the pathing

    local layer = "Land"
    ScenarioInfo.Pathing = PathingFramework.Pathing:Setup(ScenarioInfo.GridThreats, layer);
    ForkThread(
        function()
            ScenarioInfo.Pathing:Tick();
        end
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the streams

    doscript(ScenarioInfo.path .. 'GroundAI/StreamDirect.lua');
    doscript(ScenarioInfo.path .. 'GroundAI/StreamArtillery.lua');
    doscript(ScenarioInfo.path .. 'GroundAI/StreamAntiWalls.lua');
    doscript(ScenarioInfo.path .. 'GroundAI/StreamAntiShields.lua');

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the platoons
   
    doscript(ScenarioInfo.path .. 'GroundAI/PlatoonSmall.lua')
    doscript(ScenarioInfo.path .. 'GroundAI/PlatoonLarge.lua')

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- debugging

    -- ForkThread(function () ScenarioInfo.GridObjective:ForkDrawNodes() end);
    -- ForkThread(function () ScenarioInfo.GridObjective:ForkDrawOppertunities() end);
    -- ForkThread(function () ScenarioInfo.GridObjective:ForkDrawHazards() end);

    -- ForkThread(function () ScenarioInfo.GridPlayers:ForkDrawNodes() end);
    -- ForkThread(function () ScenarioInfo.GridPlayers:ForkDrawOppertunities() end);
    -- ForkThread(function () ScenarioInfo.GridPlayers:ForkDrawHazards() end);

    -- ForkThread(function () ScenarioInfo.GridThreats:ForkDrawNodes() end);
    -- ForkThread(function () ScenarioInfo.GridThreats:ForkDrawOppertunities() end);
    -- ForkThread(function () ScenarioInfo.GridThreats:ForkDrawHazards() end);

    -- ForkThread(function () ScenarioInfo.Pathing:DrawPathingNodes() end);
    -- ForkThread(function () ScenarioInfo.Pathing:DrawPathingNodesHazard() end);
    -- ForkThread(function () ScenarioInfo.Pathing:DrawNearestMarker() end);
    -- ForkThread(function () ScenarioInfo.Pathing:DrawPathingConnections() end);

    -- ForkThread(function () Regions.DrawRegions() end)
    -- ForkThread(function () Regions.DrawAxis() end)

end
