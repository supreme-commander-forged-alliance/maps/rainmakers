
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- get all dem players and regions

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    local regions = Regions.AnalyseRegions();

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialise the stream (unit) generators

    local unitGen1 = GenUnitFramework.Generator:Setup(regions, "AS1");
    local unitGen2 = GenUnitFramework.Generator:Setup(regions, "AS2");
    local unitGen3 = GenUnitFramework.Generator:Setup(regions, "AS3");

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- start checking periodically

    function Check()

        local veterancy = 0;
        local army = "HOSTILESTREAM"
        local grid = ScenarioInfo.GridPlayers;
        local startUpTime = ScenarioInfo.Config.GeneralSettings.StartupTime;
        local unitGens = { unitGen1, unitGen2, unitGen3 };

        WaitSeconds(startUpTime + 10);

        while true do

            WaitSeconds(60.0);

            local time = startUpTime + GetGameTimeSeconds()
            local iu = math.min(3, math.floor(time / 480) + 1)
            local unitGen = unitGens[iu];

            for k, player in players do 
                
                -- get all the walls
                local brain = GetArmyBrain(player)
                local shields = brain:GetListOfUnits(categories.SHIELD * categories.STRUCTURE, false)
                local n = table.getn(shields)

                -- determine the number of untis
                local unitsToSpawn = 0;
                if n > 0 then 
                    unitsToSpawn = 1 + math.floor (n / 4)
                end

                -- spawn the untis, if applicable
                for k = 1, unitsToSpawn do 

                    -- choose a random shield
                    local shieldIndex = math.floor(Random() * n) + 1
                    local shield = shields[shieldIndex];
                    local shieldPosition = shield:GetPosition();

                    -- determine the closest region
                    local region = Regions.ClosestRegionToPosition(regions, shieldPosition);

                    -- spawn the unit
                    local unit = GenUnitFramework.CreateRandomUnitAtRegion(
                        unitGen, 
                        army,
                        region, 
                        veterancy
                    )

                    -- make it roll :)
                    ForkThread(TinkerUnitFramework.ForkTarget, grid, unit, shield)
                end
            end
        end
    end

    ForkThread(Check)

end
