
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

do 

    -- make the config if it doesn't exist
    if not ScenarioInfo.Config then
        ScenarioInfo.Config = { };
    end

    -- set defaults, this is important in the Steam FA version of the game
    if (ScenarioInfo.Options.RainmakersStreamArtillery == nil) then
        ScenarioInfo.Options.RainmakersStreamArtillery = 2
        WARN("Options: RainmakersStreamArtillery is not set.");
    end

    if (ScenarioInfo.Options.RainmakersStreamDirectFire == nil) then
        ScenarioInfo.Options.RainmakersStreamDirectFire = 2
        WARN("Options: RainmakersStreamDirectFire is not set.");
    end

    if (ScenarioInfo.Options.RainmakersPlatoons == nil) then
        ScenarioInfo.Options.RainmakersPlatoons = 2
        WARN("Options: RainmakersPlatoons is not set.");
    end

    if (ScenarioInfo.Options.RainmakersMobileStealthFieldGens == nil) then
        ScenarioInfo.Options.RainmakersMobileStealthFieldGens = 2
        WARN("Options: RainmakersMobileStealthFieldGens is not set.");
    end

    if (ScenarioInfo.Options.RainmakersMobileShieldGens == nil) then
        ScenarioInfo.Options.RainmakersMobileShieldGens = 2
        WARN("Options: RainmakersMobileShieldGens is not set.");
    end

    -- the actual values
    local streamDirectQuantity      = { 3.00, 2.45, 1.90 }      -- the threshold value before a unit is spawned, lower is more units
    local streamDirectQuality       = { 0, 1, 2 }               -- the veterancy of the unit

    local streamArtilleryQuantity   = { 5.90, 4.85, 4.00 }      -- the threshold value before a unit is spawned, lower is more units
    local streamArtilleryQuality    = { 0, 2, 4 }               -- the veterancy of the unit

    local platoonQuantity           = { 42.0, 35.5, 29.5 }      -- the threshold value before a unit is spawned, lower is more units
    local platoonQuality            = { 0, 1, 2 }               -- the veterancy of the unit

    local mobileStealthFieldGens    = { 0.85, 0 }               -- chance that a platoon spawns with shields. Not all platoons support shields
    local mobileShieldGens          = { 0.85, 0 }               -- chance that a platoon spawns with stealth fields. Not all platoons support stealth fields

    -- save the actual values in the config
    ScenarioInfo.Config.GroundSettings = {
        StreamDirectQuantity    = streamDirectQuantity[ScenarioInfo.Options.RainmakersStreamDirectFire],
        StreamDirectQuality     = streamDirectQuality[ScenarioInfo.Options.RainmakersStreamDirectFire],

        StreamArtilleryQuantity = streamArtilleryQuantity[ScenarioInfo.Options.RainmakersStreamArtillery],
        StreamArtilleryQuality  = streamArtilleryQuality[ScenarioInfo.Options.RainmakersStreamArtillery],

        PlatoonQuantity         = platoonQuantity[ScenarioInfo.Options.RainmakersPlatoons],
        PlatoonQuality          = platoonQuality[ScenarioInfo.Options.RainmakersPlatoons],

        MobileStealthFieldGens  = mobileStealthFieldGens[ScenarioInfo.Options.RainmakersMobileStealthFieldGens],
        MobileShieldGens        = mobileShieldGens[ScenarioInfo.Options.RainmakersMobileShieldGens],
    }

    -- save a more human-friendly version in the config
    local streamArtilleryFriendly           = { 'Few', 'Moderate', 'Many' }
    local streamDirectFriendly              = { 'Few', 'Moderate', 'Many' }
    local platoonsFriendly                  = { 'Few', 'Moderate', 'Many' }

    local mobileStealthFieldGensFriendly    = { 'On', 'Off' }
    local MobileShieldGensFriendly          = { 'On', 'Off' }

    ScenarioInfo.Config.GroundSettingsFriendly = {
        StreamDirect            = streamDirectFriendly[ScenarioInfo.Options.RainmakersStreamDirectFire],
        StreamArtillery         = streamArtilleryFriendly[ScenarioInfo.Options.RainmakersStreamArtillery],
        Platoons                = platoonsFriendly[ScenarioInfo.Options.RainmakersPlatoons],
        MobileStealthFieldGens  = mobileStealthFieldGensFriendly[ScenarioInfo.Options.RainmakersMobileStealthFieldGens],
        MobileShieldGens        = MobileShieldGensFriendly[ScenarioInfo.Options.RainmakersMobileShieldGens], 
    }

end