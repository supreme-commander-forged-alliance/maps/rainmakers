
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- colorsss

    -- bad fella's.
    ScenarioFramework.SetCybranEvilColor(ScenarioInfo.Armies.Stream)
    ScenarioFramework.SetAeonEvilColor(ScenarioInfo.Armies.Platoon)

    -- good fella's.
    ScenarioFramework.SetUEFAlly2Color(ScenarioInfo.Armies.Civilians)
    ScenarioFramework.SetUEFAllyColor(ScenarioInfo.Armies.Objective)
    ScenarioFramework.SetUEFNeutralColor("Reinforcements")
    ScenarioFramework.SetSeraphimColor("Environment")

end
