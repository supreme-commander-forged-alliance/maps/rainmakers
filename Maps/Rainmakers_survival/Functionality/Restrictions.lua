
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all the players

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
	end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- add the restrictions

    for k, player in players do
        -- remove all air
        ScenarioFramework.AddRestriction(player, categories.AIR)

        -- remove all navy
        ScenarioFramework.AddRestriction(player, categories.NAVAL)

        -- remove anything that can only shoot air
        ScenarioFramework.AddRestriction(player, categories.ANTIAIR - (categories.DIRECTFIRE + categories.INDIRECTFIRE + categories.ARTILLERY))
    end

end
