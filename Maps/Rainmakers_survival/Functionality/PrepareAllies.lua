
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- construct the inner base, artilleries, etc

    -- spawn it all
    local generalObjectiveBuildings = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Objective, "Buildings", false);
    local generalObjectiveDefenses = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Objective, "Defences", false);
    ScenarioInfo.Globals.ArtilleryObjectives = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Objective, "Objectives", false);

    -- make things more sturdy
    for k, unit in generalObjectiveDefenses do
      unit:SetVeterancy(3);
    end

    for k, objective in ScenarioInfo.Globals.ArtilleryObjectives do
      objective:SetVeterancy(3);
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- make the artillery shoot

    function SetupAttack()
      for k, objective in ScenarioInfo.Globals.ArtilleryObjectives do
        -- attack ground
        objective:SetFireState(2);
        IssueAttack({objective}, ScenarioUtils.GetMarker("Target").position);

        -- add in a little wait so that they do not fire at the same time
        WaitSeconds(5.0);
      end
    end

    ForkThread(SetupAttack);
end
