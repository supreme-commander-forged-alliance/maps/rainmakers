
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all the players

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end
    
    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the commander or do prebuilt units

    local factions = {
        "uef",
        "aeon",
        "cybran",
        "seraphim"
    }

    -- spawn the buildings
    for k, player in players do 

        local brain = GetArmyBrain(player);
        local index = brain:GetFactionIndex();

        -- (copied from ScenarioUtlities.InitializeArmies)
        if brain.SkirmishSystems then
            brain:InitializeSkirmishSystems()
        end

        -- spawn the commander (copied from ScenarioUtlities.lua)
        local initialUnitName = import('/lua/factions.lua').Factions[index].InitialUnit
        cdrUnit = CreateInitialArmyUnit(player, initialUnitName)
        cdrUnit:SetCustomName(brain.Nickname)

        -- spawn the buildings if prebuilt is on
        if ScenarioInfo.Options.PrebuiltUnits == 'On' then

            local faction = factions[index];
            local group = "prebuilt-" .. faction;
            ScenarioUtils.CreateArmyGroup(player, group, false);
        -- do commander warp in effect if prebuilt is off
        else
            cdrUnit:HideBone(0, true)
            ForkThread(ScenarioUtils.CommanderWarpDelay, cdrUnit, 3)
        end
    end
end
