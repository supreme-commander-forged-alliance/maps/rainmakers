
do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all the players

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
	end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- set all the diplomacy

    -- ally / hostile relationships for AI's
    SetAlliance(ScenarioInfo.Armies.Platoon, ScenarioInfo.Armies.Objective, "Enemy");
    SetAlliance(ScenarioInfo.Armies.Stream, ScenarioInfo.Armies.Objective, "Enemy");

    SetAlliance(ScenarioInfo.Armies.Civilians, ScenarioInfo.Armies.Objective, "Ally");
    SetAlliance(ScenarioInfo.Armies.Civilians, ScenarioInfo.Armies.Platoon, "Neutral");
    SetAlliance(ScenarioInfo.Armies.Civilians, ScenarioInfo.Armies.Stream, "Neutral");

    SetAlliance(ScenarioInfo.Armies.Stream, ScenarioInfo.Armies.Platoon, "Neutral");

    SetAlliance(ScenarioInfo.Armies.Stream, "Easter", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Platoon, "Easter", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Civilians, "Easter", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Objective, "Easter", "Neutral");
    SetAlliance("Environment", "Easter", "Neutral");
    
    SetAlliance(ScenarioInfo.Armies.Stream, "Environment", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Platoon, "Environment", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Civilians, "Environment", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Objective, "Environment", "Neutral");

    SetAlliance("Environment", "Reinforcements", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Stream, "Reinforcements", "Enemy");
    SetAlliance(ScenarioInfo.Armies.Platoon, "Reinforcements", "Enemy");
    SetAlliance(ScenarioInfo.Armies.Civilians, "Reinforcements", "Neutral");
    SetAlliance(ScenarioInfo.Armies.Objective, "Reinforcements", "Neutral");
    SetAlliance("Easter", "Reinforcements", "Neutral");

    -- ally / hostile relationships for players
    for k, army in players do
        SetAlliance(army, ScenarioInfo.Armies.Civilians, "Neutral");
        SetAlliance(army, ScenarioInfo.Armies.Objective, "Ally");
        SetAlliance(army, "Environment", "Enemy");
        SetAlliance(army, "Easter", "Neutral");
        SetAlliance(army, ScenarioInfo.Armies.Platoon, "Enemy");
        SetAlliance(army, ScenarioInfo.Armies.Stream, "Enemy");
        SetAlliance(army, "Reinforcements", "Ally");
    end

    -- ally relationship between players
    for k1, army1 in players do
        for k2, army2 in players do
            SetAlliance(army1, army2, "Ally");
        end
    end
end
