
Computer_UEFComputer_Commanders_01315 = {text = '<LOC Computer_Commanders_010_020>[{i UEFComputer}]: Commander destroyed', bank = 'UEL', cue = 'UEFComputer_Commanders_01315'}
Computer_UEFComputer_Commanders_01316 = {text = '<LOC Computer_Commanders_010_030>[{i UEFComputer}]: Allied Commander destroyed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Commanders_01316'}

Computer_UEFComputer_Commanders_01322 = {text = '<LOC Computer_Commanders_010_090>[{i UEFComputer}]: ACU integrity at 50%!', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Commanders_01322'}
Computer_UEFComputer_Commanders_01323 = {text = '<LOC Computer_Commanders_010_100>[{i UEFComputer}]: ACU integrity at 75%!', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Commanders_01323'}

Computer_UEFComputer_Intel_01194 = {text = '<LOC Computer_Intel_010_010>[{i UEFComputer}]: Enemy detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01194'}
Computer_UEFComputer_Intel_01195 = {text = '<LOC Computer_Intel_010_020>[{i UEFComputer}]: Enemy forces detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01195'}
Computer_UEFComputer_Intel_01196 = {text = '<LOC Computer_Intel_010_030>[{i UEFComputer}]: Enemy units detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01196'}
Computer_UEFComputer_Intel_01197 = {text = '<LOC Computer_Intel_010_040>[{i UEFComputer}]: Enemy installations detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01197'}
Computer_UEFComputer_Intel_01198 = {text = '<LOC Computer_Intel_010_050>[{i UEFComputer}]: Commander Detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01198'}
Computer_UEFComputer_Intel_01199 = {text = '<LOC Computer_Intel_010_060>[{i UEFComputer}]: Experimental Unit Detected', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Intel_01199'}

Computer_UEFComputer_NewExpansion_01388 = {text = '<LOC Computer_NewObjectives_010_010>[{i UEFComputer}]: New Objective', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_NewExpansion_01388'}
Computer_UEFComputer_NewExpansion_01389 = {text = '<LOC Computer_NewObjectives_010_020>[{i UEFComputer}]: New primary Objective', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_NewExpansion_01389'}
Computer_UEFComputer_NewExpansion_01390 = {text = '<LOC Computer_NewObjectives_010_030>[{i UEFComputer}]: New secondary Objective', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_NewExpansion_01390'}

Computer_UEFComputer_Failed_01405 = {text = '<LOC Computer_Failed_010_010>[{i UEFComputer}]: Objective failed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01405'}
Computer_UEFComputer_Failed_01406 = {text = '<LOC Computer_Failed_010_020>[{i UEFComputer}]: Primary Objective failed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01406'}
Computer_UEFComputer_Failed_01407 = {text = '<LOC Computer_Failed_010_030>[{i UEFComputer}]: Secondary Objective failed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01407'}
Computer_UEFComputer_Failed_01408 = {text = '<LOC Computer_Failed_010_040>[{i UEFComputer}]: Operation failed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01408'}
Computer_UEFComputer_Failed_01420 = {text = '<LOC Computer_Failed_010_050>[{i UEFComputer}]: Mission failed', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01420'}
Computer_UEFComputer_Failed_01421 = {text = '<LOC Computer_Failed_010_060>[{i UEFComputer}]: You suck, sir', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01421'}
Computer_UEFComputer_Failed_01422 = {text = '<LOC Computer_Failed_010_070>[{i UEFComputer}]: That was pathetic', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Failed_01422'}

Computer_UEFComputer_Completed_01398 = {text = '<LOC Computer_Completed_010_010>[{i UEFComputer}]: Objective completed', bank = 'UEL', cue = 'UEFComputer_Completed_01398'}
Computer_UEFComputer_Completed_01399 = {text = '<LOC Computer_Completed_010_020>[{i UEFComputer}]: Primary Objective complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01399'}
Computer_UEFComputer_Completed_01400 = {text = '<LOC Computer_Completed_010_030>[{i UEFComputer}]: Secondary Objective complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01400'}
Computer_UEFComputer_Completed_01401 = {text = '<LOC Computer_Completed_010_040>[{i UEFComputer}]: Hidden Objective complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01401'}
Computer_UEFComputer_Completed_01402 = {text = '<LOC Computer_Completed_010_050>[{i UEFComputer}]: Bonus Objective complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01402'}
Computer_UEFComputer_Completed_01403 = {text = '<LOC Computer_Completed_010_060>[{i UEFComputer}]: Operation complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01403'}
Computer_UEFComputer_Completed_01404 = {text = '<LOC Computer_Completed_010_070>[{i UEFComputer}]: Mission complete', bank = 'COMPUTER_UEF_VO', cue = 'UEFComputer_Completed_01404'}

X02_M02_070 = {
  {text = '<LOC X02_M02_070_010>[{i HQ}]: Multiple attack waves incoming. HQ out.', vid = 'X02_HQ_M02_03541.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03541', faction = 'NONE'},
}

X01_M02_281 = {
  {text = '<LOC X01_M02_281_010>[{i HQ}]: Got a Galactic Colossus heading your way. Deal with it. HQ out.', vid = 'X01_HQ_M02_04889.sfd', bank = 'X01_VO', cue = 'X01_HQ_M02_04889', faction = 'NONE'},
}

# Attack Warnings #1 / Actor: HQ / Update: 07/06/2007 / VO Ready
X02_M02_070 = {
  {text = '<LOC X02_M02_070_010>[{i HQ}]: Multiple attack waves incoming. HQ out.', vid = 'X02_HQ_M02_03541.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03541', faction = 'NONE'},
}

# Attack Warnings #2 / Actor: HQ / Update: 07/06/2007 / VO Ready
X02_M02_080 = {
  {text = '<LOC X02_M02_080_010>[{i HQ}]: Another attack is vectoring towards your position. HQ out.', vid = 'X02_HQ_M02_03542.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03542', faction = 'NONE'},
}

# Attack Warnings #3 / Actor: HQ / Update: 07/06/2007 / VO Ready
X02_M02_090 = {
  {text = '<LOC X02_M02_090_010>[{i HQ}]: Enemy forces are converging on your position. HQ out.', vid = 'X02_HQ_M02_03543.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03543', faction = 'NONE'},
}

# Attack Warnings #4 / Actor: HQ / Update: 07/06/2007 / VO Ready
X02_M02_100 = {
  {text = '<LOC X02_M02_100_010>[{i HQ}]: Enemy assault inbound. HQ out.', vid = 'X02_HQ_M02_03544.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03544', faction = 'NONE'},
}

X02_M02_160 = {
  {text = '<LOC X02_M02_160_010>[{i QAI}]: It is time to end this. My primary attack force is moving into position.', vid = 'X02_QAI_M02_04278.sfd', bank = 'X02_VO', cue = 'X02_QAI_M02_04278', faction = 'Cybran'},
  {text = '<LOC X02_M02_160_030>[{i HQ}]: Got a barrel of fun rolling your way, Commander. HQ out.', vid = 'X02_HQ_M02_04280.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_04280', faction = 'NONE'},
}

# Bomber+Engineer Destruction #5 / Actor: HQ / Update: 07/17/2007 / VO Ready
X03_M02_280 = {
  {text = '<LOC X03_M02_280_010>[{i HQ}]: Such a pretty explosion. HQ out.', vid = 'X03_HQ_M02_04929.sfd', bank = 'X03_VO', cue = 'X03_HQ_M02_04929', faction = 'NONE'},
}

# Large attack defeated # 2 / Actor: HQ / Update: 07/05/2007 / VO Ready
X03_M03_120 = {
  {text = '<LOC X03_M03_120_010>[{i HQ}]: They ain\'t going to forget that butt-whipping. Good job, Commander. HQ out.', vid = 'X03_HQ_M03_03345.sfd', bank = 'X03_VO', cue = 'X03_HQ_M03_03345', faction = 'NONE'},
}

X04_M01_010 = {
  {text = '<LOC X04_M01_010_010>[{i HQ}]: Both commanders are on-planet, and the OP is a go. HQ out.', vid = 'X04_HQ_M01_03594.sfd', bank = 'X04_VO', cue = 'X04_HQ_M01_03594', faction = 'NONE'},
}

X04_M01_050 = {
  {text = '<LOC X04_M01_050_010>[{i HQ}]: All operational objectives have been completed. Begin preparations to recall. We\'re getting you off that rock. HQ out.', vid = 'X04_HQ_M01_03733.sfd', bank = 'X04_VO', cue = 'X04_HQ_M01_03733', faction = 'NONE'},
  {text = '<LOC X04_M01_050_020>[{i Dostya}]: I have a situation here ... the recall command is auto-failing. HQ, can you confirm? Please run diagnostics on the return Gate. HQ, please respond.', vid = 'X04_Dostya_M01_03734.sfd', bank = 'X04_VO', cue = 'X04_Dostya_M01_03734', faction = 'Cybran'},
}

X04_M02_050 = {
  {text = '<LOC X04_M02_050_010>[{i Dostya}]: The device is destroyed! Dostya to HQ, respond!', vid = 'X04_Dostya_M02_03744.sfd', bank = 'X04_VO', cue = 'X04_Dostya_M02_03744', faction = 'Cybran'},
  {text = '<LOC X04_M02_050_020>[{i HQ}]: There are at least two Quantum Wakes, you must --', vid = 'X04_HQ_M02_03745.sfd', bank = 'X04_VO', cue = 'X04_HQ_M02_03745', faction = 'NONE'},
  {text = '<LOC X04_M02_050_030>[{i Dostya}]: Listen to me! The Seraphim have a jamming device that is preventing recall. It\'s out of our operational area, so you must find a countermeasure.', vid = 'X04_Dostya_M02_03746.sfd', bank = 'X04_VO', cue = 'X04_Dostya_M02_03746', faction = 'Cybran'},
  {text = '<LOC X04_M02_050_040>[{i HQ}]: Keep your pants on, we\'ll get you out of there as soon as we can.', vid = 'X04_HQ_M02_03747.sfd', bank = 'X04_VO', cue = 'X04_HQ_M02_03747', faction = 'NONE'},
}

# PO1 Survive Until Recalled / Actor: HQ / Update: 03/07/2007 / VO Ready
X04_M03_020 = {
  {text = '<LOC X04_M03_020_010>[{i HQ}]: Commander, you still have enemy forces moving toward your position. Hold the line until we can extract you. HQ out.', vid = 'X04_HQ_M03_03757.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03757', faction = 'NONE'},
}

# PO1 50% to recall / Actor: HQ / Update: 03/07/2007 / VO Ready
X04_M03_070 = {
  {text = '<LOC X04_M03_070_010>[{i HQ}]: Going to be a bit longer, Commander. Hang in there.', vid = 'X04_HQ_M03_03765.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03765', faction = 'NONE'},
}

# PO1 Recall / Actor: HQ / Update: 03/07/2007 / VO Ready
X04_M03_120 = {
  {text = '<LOC X04_M03_120_010>[{i HQ}]: Grab your barf bag, Commander, this is going to be rough. Recalling now!', vid = 'X04_HQ_M03_03770.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03770', faction = 'NONE'},
}

# Seraphim Annihilation / Actor: HQ / Update: 07/17/2007 / VO Ready
X04_M03_210 = {
  {text = '<LOC X04_M03_210_010>[{i HQ}]: Damn, you certainly took care of business. We\'re recalling you. HQ out.', vid = 'X04_HQ_M03_04920.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04920', faction = 'NONE'},
}

# Stay in the Box #1 / Actor: HQ / Update: 07/17/2007 / VO Ready
X04_M03_220 = {
  {text = '<LOC X04_M03_220_010>[{i HQ}]: Commander, stay within the area of your base. If you stray too far, we won\'t be able to get a recall lock. HQ out.', vid = 'X04_HQ_M03_04935.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04935', faction = 'NONE'},
}

# Stay in the Box #2 / Actor: HQ / Update: 07/17/2007 / VO Ready
X04_M03_230 = {
  {text = '<LOC X04_M03_230_010>[{i HQ}]: Get back to your base, Commander. You\'re too far out and we can\'t get a lock on you. HQ out.', vid = 'X04_HQ_M03_04936.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04936', faction = 'NONE'},
}

# Stay in the Box #3 / Actor: HQ / Update: 07/17/2007 / VO Ready
X04_M03_240 = {
  {text = '<LOC X04_M03_240_010>[{i HQ}]: Stay within your base\'s perimeter, Commander. We need to lock down your position so we can get you the hell out of there. HQ out.', vid = 'X04_HQ_M03_04937.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04937', faction = 'NONE'},
}

# NIS All clear / Actor: HQ / Update: 07/19/2007 / VO Ready
X04_M03_270 = {
  {text = '<LOC X04_M03_270_010>[{i HQ}]: That was damn close, Commander. Glad we got you out of there in one piece.', vid = 'X04_HQ_M03_05107.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_05107', faction = 'NONE'},
}

# M1 NIS Intro #3 / Actor: HQ / Update: 07/05/2007 / VO Ready
X05_M01_012 = {
  {text = '<LOC X05_M01_012_010>[{i HQ}]: Get it in gear, Commander. HQ out.', vid = 'X05_HQ_M01_04909.sfd', bank = 'X05_VO', cue = 'X05_HQ_M01_04909', faction = 'NONE'},
}

# Attacks Experimental01 / Actor: HQ / Update: 07/11/2007 / VO Ready
XGG_GD1_380 = {
  {text = '<LOC XGG_GD1_380_010>[{i HQ}]: Commander, an enemy Fatboy has been sighted. HQ out.', bank = 'XGG', cue = 'XGG_HQ_GD1_04179', faction = 'NONE'},
}

# Attacks Experimental04 / Actor: HQ / Update: 07/11/2007 / VO Ready
XGG_GD1_410 = {
  {text = '<LOC XGG_GD1_410_010>[{i HQ}]: Commander, an enemy Monkeylord has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04182.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04182', faction = 'NONE'},
}

# Attacks Experimental07 / Actor: HQ / Update: 07/11/2007 / VO Ready
XGG_GD1_440 = {
  {text = '<LOC XGG_GD1_440_010>[{i HQ}]: Commander, an enemy Colossus has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04185.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04185', faction = 'NONE'},
}

# Attacks Experimental10 / Actor: HQ / Update: 07/11/2007 / VO Ready
XGG_GD1_470 = {
  {text = '<LOC XGG_GD1_470_010>[{i HQ}]: Commander, an enemy Ythotha has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04188.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04188', faction = 'NONE'},
}

X06_DB01_030 = {
  {text = '<LOC X06_DB01_030_010>[{i HQ}]: Commander? You there? Dammit.', vid = 'X06_HQ_DB01_04963.sfd', bank = 'Briefings', cue = 'X06_HQ_DB01_04963', faction = 'NONE'},
}

X06_M01_056 = {
    {text = '<LOC X06_M01_056_010>[{i HQ}]: Two support commanders gated in ahead of you, Commander, and they\'ve started work on your base. HQ out.', vid = 'X06_HQ_M01_04470.sfd', bank = 'X06_VO', cue = 'X06_HQ_M01_04470', faction = 'NONE'},
  }

  # Player Death #1 / Actor: HQ / Update: 07/17/2007 / VO Ready
X06_M01_130 = {
  {text = '<LOC X06_M01_130_010>[{i HQ}]: Looks like the Commander just ate it. Poor bastard.', vid = 'X06_HQ_M01_04960.sfd', bank = 'X06_VO', cue = 'X06_HQ_M01_04960', faction = 'NONE'},
}

# Player Death #2 / Actor: HQ  / Update: 07/17/2007 / VO Ready
X06_M01_140 = {
  {text = '<LOC X06_M01_140_010>[{i HQ}]: Commander, you read me? Commander? Ah hell...', vid = 'X06_HQ_M01_04961.sfd', bank = 'X06_VO', cue = 'X06_HQ_M01_04961', faction = 'NONE'},
}

# PO3 Control Center Destroyed / Actor: HQ / Update: 07/06/2007 / VO Ready
X06_M02_210 = {
  {text = '<LOC X06_M02_210_010>[{i HQ}]: Commander, the Control Center has been destroyed. HQ out.', vid = 'X06_HQ_M02_03965.sfd', bank = 'X06_VO', cue = 'X06_HQ_M02_03965', faction = 'NONE'},
}

# PO3 Control Center Under Attack #1 / Actor: HQ / Update: 07/06/2007 / VO Cut
X06_M02_220 = {
  {text = '<LOC X06_M02_220_010>[{i HQ}]: Enemy forces are bearing down on the Control Center. HQ out.', vid = 'X06_HQ_M02_03966.sfd', bank = 'X06_VO', cue = 'X06_HQ_M02_03966', faction = 'NONE'},
}

# Rhiza Death Scream / Actor: Rhiza / Update: 08/07/2007 / VO Ready
X06_M02_270 = {
  {text = '<LOC X06_M02_270_010>[{i Rhiza}]: [High Pitched Death Scream]', vid = 'X06_Rhiza_M02_05125.sfd', bank = 'X06_VO', cue = 'X06_Rhiza_M02_05125', faction = 'Aeon'},
}

# Completed PO2 / Actor: HQ / Update: 07/06/2007 / VO Ready
X06_M03_240 = {
  {text = '<LOC X06_M03_240_010>[{i HQ}]: Scratch one alien. HQ out.', vid = 'X06_HQ_M03_04002.sfd', bank = 'X06_VO', cue = 'X06_HQ_M03_04002', faction = 'NONE'},
}

# Vedetta - On death / Actor: Vedetta / Update: 07/10/2007 / VO Ready
TAUNT30 = {
  {text = '<LOC X06_T01_580_010>[{i Vendetta}]: Aaaaaaaaah!', vid = 'X06_Vedetta_T01_03020.sfd', bank = 'X06_VO', cue = 'X06_Vedetta_T01_03020', faction = 'Aeon'},
}

# Player Death  / Actor: HQ / Update: 07/17/2007 / VO Ready
X05_DB01_040 = {
  {text = '<LOC X05_DB01_040_010>[{i HQ}]: Commander? Can you read me? Ah hell, I think the Commander is dea --', vid = 'X05_HQ_DB01_04957.sfd', bank = 'Briefings', cue = 'X05_HQ_DB01_04957', faction = 'NONE'},
}

# PO1,2 Opening Attack / Actor: HQ / Update: 07/05/2007 / VO Ready
X05_M03_140 = {
  {text = '<LOC X05_M03_140_010>[{i HQ}]: Attack inbound on your position. HQ out.', vid = 'X05_HQ_M03_03871.sfd', bank = 'X05_VO', cue = 'X05_HQ_M03_03871', faction = 'NONE'},
}