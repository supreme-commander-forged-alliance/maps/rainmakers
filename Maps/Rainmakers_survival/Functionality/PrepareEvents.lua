

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all the players

	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
	end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- run the event manager
    
    -- prepares all of event AI state.
    function InitialiseEventAI()

        ScenarioInfo.Globals.EventAI = import(ScenarioInfo.path .. 'Events/EventSetup.lua').EventAI:Initialise(
            players
        );

        ScenarioInfo.Globals.EventAI:Start();
    end

    ForkThread(InitialiseEventAI);

end