
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- make some players ignore the playable area that is set

    ScenarioFramework.SetPlayableArea('PlayableRegion', false);

    SetIgnorePlayableRect(ScenarioInfo.Armies.Platoon, true);
    SetIgnorePlayableRect(ScenarioInfo.Armies.Stream, true);
    SetIgnorePlayableRect(ScenarioInfo.Armies.Objective, true);
    SetIgnorePlayableRect(ScenarioInfo.Armies.Civilians, true);
    SetIgnorePlayableRect("Reinforcements", true);
    SetIgnorePlayableRect("Easter", true);

end