
do

    ForkThread(function ()

        WaitSeconds(16.0);

        local text = 'center \r\n enter \r\n row3';
        local size = 20;
        local fade = 9;

        -- what other options are there?
        local alignment = 'lefttop';

        -- what format should the color be in?
        local color = 'FFFFFF';

        -- alignment
        local offset = "          "
        for k = 1, 7 do 
            PrintText("", size + 10, color, fade, alignment);
        end

        -- print the general settings
        PrintText(offset .. "General settings: ", size + 10, color, fade, alignment);

        for k, field in ScenarioInfo.Config.GeneralSettingsFriendly do 
            PrintText(offset .. offset .. k .. ' = ' .. field , size, color, fade, alignment) ;	
            -- PrintText(, size, color, fade, alignment) ;	
        end
        PrintText("", size + 10, color, fade, alignment);
        WaitSeconds(2.0)

        -- print the ground settings
        PrintText(offset .. "Ground settings: ", size + 10, color, fade, alignment);

        for k, field in ScenarioInfo.Config.GroundSettingsFriendly do 
            PrintText(offset .. offset .. k .. ' = ' .. field , size, color, fade, alignment) ;	
            -- PrintText('', size, color, fade, alignment) ;	
        end
        PrintText("", size + 10, color, fade, alignment);
        WaitSeconds(2.0)

        -- print epic whiteline
        LOG(PrintText('' , size, color, fade, alignment)) ;	

        -- print the info about the settings
        PrintText(offset .. 'You can change the (difficulty) options in the' , size, "#999999", fade, alignment) ;
        PrintText(offset .. 'lobby under \'options\' and then under the ' , size, "#999999", fade, alignment) ;		
        PrintText(offset .. '\'advanced\' tab at the very bottom of the list.' , size, "#999999", fade, alignment) ;	

        -- print epic whiteline
        LOG(PrintText('' , size, color, fade, alignment)) ;	

        PrintText(offset .. 'Check the vault for the latest version of Rainmakers.' , size, "#999999", fade + 5, alignment) ;	

    end);

end
