
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do 

    function MakeImmune(unit)
        unit:SetReclaimable(false);
        unit:SetCapturable(false);
        unit:SetDoNotTarget(true);
        unit:SetRegenRate(5000);
        unit:SetUnSelectable(true);
        unit:SetCanTakeDamage(false);
        unit:SetCanBeKilled(false);
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn it all

    local rPlatoons = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Platoon, "Resources", false);
    local rStream = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Stream, "Resources", false);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- make things immune / produce resources

    for k, unit in rPlatoons do
      unit:SetProductionPerSecondMass(2000);
      unit:SetProductionPerSecondEnergy(25000);
      MakeImmune(unit);
    end
  
    for k, unit in rStream do
      unit:SetConsumptionPerSecondMass(2000);
      unit:SetProductionPerSecondEnergy(25000);
      MakeImmune(unit);
    end

end