
function ToDegrees(radians)
    return (180 / math.pi) * radians
end

function ToRadians(degrees)
    return (degrees / 180) * math.pi
end

-- computes the full angle from the base (0 - 360 degrees)
function FullDegrees (base, direction) 

    -- | computes the full angle in radians of the direction where the
    -- base vector represents the point of 0 degrees. Assumes that both
    -- vectors are normalized in advance.
    -- radiansNormalized :: Base -> Direction -> Radians 
    -- radiansNormalized b d 
    --     | sign < 0  = rads'
    --     | otherwise = (2 * pi) - rads'
    --     where
    --         rads'   = acos (dot b d)
    --         sign    = signum (dot ob d)
    --         ob      = orthogonal b 

    -- the simple method only allows angles between 0 and 180 degrees, but that is unrealistic. This
    -- more complicated method allows to determine the angle in the full range
    local orthogonal = { base[3], 0, -base[1] }
    local rads = math.acos(base[1] * direction[1] + base[3] * direction[3]);

    local radians = rads;
    if orthogonal[1] * direction[1] + orthogonal[3] * direction[3] < 0 then 
        radians = 2 * math.pi - rads;
    end

    local degrees = (180 / math.pi) * radians;

    return degrees;
end

-- computes the direction from a to b.
function DirectionTo(a, b) 
    return { b[1] - a[1], 0, b[3] - a[3] };
end

-- computes the normalized direction from a to b.
function NormalizedDirection(a, b)
    local direction = DirectionTo(a, b);
    local magnitude = math.sqrt(direction[1] * direction[1] + 0 + direction[3] * direction[3]);
    local inverse = 1.0 / magnitude;
    return { inverse * direction[1], 0, inverse * direction[3] };
end

-- moves a platoon over a path with correct headings after each node
function MovePlatoon (platoon, path, formation, last, first)

    -- check for default values
    first = first or 1;
    last = last or table.getn(path);

    -- compute all the degrees
    local degrees = { };
    for k = first, last, 1 do 

        local curr = path[k - 1];
        local next = path[k];

        if k == 1 then 
            -- use the platoons position as the first node
            curr = platoon:GetPlatoonPosition();
        end

        local base = { 0, 0, 1 }
        local direction = NormalizedDirection(curr, next);
        local angle = FullDegrees(base, direction);
        degrees[k] = angle;
    end

    -- move all the units correspondingly
    local units = platoon:GetPlatoonUnits();
    for k = first, last, 1 do 
        local node = path[k];
        local angle = degrees[k];
        IssueFormMove(units, node, formation, angle);
    end

end

--- Sets the target priorites for each weapon of each unit
-- @param units A table of units
-- @param priority A stringified version of a bunch of categories 
function SetPriorities(units, priTable)
    -- for every unit
    for k, unit in units do 
        -- and every weapon
        local count = unit:GetWeaponCount();
        for c = 1, count do 
            -- set its priorities
            local weapon = unit:GetWeapon(c)
            weapon:SetWeaponPriorities(priTable)
        end
    end
end

-- Sets the target priorities per weapon of each unit
-- @param units A table of units
-- @param priorities A table of <weapon, priority> key-value pairs
function SetPrioritiesPerWeapon(units, priorities)
    -- for every unit
    for k, unit in units do 
        -- and every weapon
        local count = unit:GetWeaponCount();
        for c = 1, count do 
            -- determine the priority for this weapon and set it
            local priority = priorities[c]
            if priority then 
                local weapon = unit:GetWeapon(c)
                weapon:SetWeaponPriorities(priority)
            end
        end
    end
end