Lighting = {
right_ascension = -0.436332,
declination = 0.506145,
multiplier = 1.56,
light_color = { 1.04, 0.97, 0.75 },
ambient_light_color = { 0.2, 0.2, 0.2 },
shadow_color = { 0.52, 0.52, 0.37 },
specular_color = { 0, 3.50325e-044, 2.38673e-039 },
specular_glow = 0.05,
bloom = 0.105,
fog_color = { 0, 0, 0 },
fog_start = 40,
fog_end = 560,
}
