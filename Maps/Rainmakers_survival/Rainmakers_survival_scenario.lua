version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Rainmakers Survival",
    description = "An artillery location is providing cover fire for an off-site siege. It will not be long until the enemy triangulates their locations. It is on you to keep them active for as long as they are required. Map made by (Jip) Willem Wijnia. You can find the latest version at: https://gitlab.com/supreme-commander-forged-alliance",
    preview = '',
    map_version = 65,
    type = 'skirmish',
    starts = true,
    size = {1024, 1024},
    reclaim = {1016424, 119858.5},
    map = '/maps/Rainmakers_survival/Rainmakers_survival.scmap',
    save = '/maps/Rainmakers_survival/Rainmakers_survival_save.lua',
    script = '/maps/Rainmakers_survival/Rainmakers_survival_script.lua',
    norushradius = 0,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'HOSTILEPLATOONS HOSTILESTREAM Artifact Neutrals Objective UEFOperative Infrastructure Easter Generators Reinforcements Environment' ),
            },
        },
    },
}
