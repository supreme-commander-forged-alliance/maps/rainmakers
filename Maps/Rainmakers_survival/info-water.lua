Water = {
surface_elev = -10000,
deep_elev = -10000,
abyss_elev = -10000,
surface_color = { 0, 0.7, 1.5 },
color_lerp = { 0.064, 0.119 },
refraction_scale = 0.375,
fresnel_bias = 0.15,
fresnel_power = 1.5,
unit_reflection = 0.5,
sky_reflection = 1.5,
sun_shininess = 50,
sun_strength = 10,
sun_direction = { 0.0995482, -0.962631, 0.251857 },
sun_color = { 0.812743, 0.4741, 0.338643 },
sun_reflection = 5,
sun_glow = 0.1,
local_cubemap = '/textures/engine/waterCubemap.dds',
water_ramp = '/textures/engine/waterramp.dds',
normal_repeat = { 0.0009, 0.009, 0.05, 0.5 },
normals = {
['0'] = {
normal_movement = { 0.5, -0.95 },
local_normals = '/textures/engine/waves.dds',
},
['1'] = {
normal_movement = { 0.05, -0.095 },
local_normals = '/textures/engine/waves.dds',
},
['2'] = {
normal_movement = { 0.01, 0.03 },
local_normals = '/textures/engine/waves.dds',
},
['3'] = {
normal_movement = { 0.0005, 0.0009 },
local_normals = '/textures/engine/waves.dds',
},
},
}
