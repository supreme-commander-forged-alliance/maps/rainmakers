
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'GroundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local Rnd = import(ScenarioInfo.path .. 'Algorithms/PseudoRandom.lua');
local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = true;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Platoon;

local cached = { }

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Generator = { };
Generator.__index = Generator;

-- initialises the groundSpawner.
-- regions:     the regions that groupData can spawn in
-- random:      the pseudo random algorithm, as described in Algorithms/PseudoRandom
-- threshold:   a platoon spawns when this has been met
-- identifier:  used to find the groupData that belong to this groundSpawner
-- utility:     the function used that represents our rate for spawning
function Generator:Setup(
    regions,
    identifier)

    -- linking
    generator = { };
    setmetatable(generator, Generator);

    -- store all arguments
    generator.Regions = regions;
    generator.Identifier = identifier;

    -- gather our platoon data
    generator.Platoons = Analyse(identifier)
                            
    -- generate some random'ness.
    generator.PlatoonRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Platoons))
    );

    generator.RegionRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Regions))
    );

    generator.ShieldRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, 10)
    );

    generator.StealthRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, 10)
    );

    return generator;
end

function Analyse(identifier)

    -- don't do work twice, but make sure we have a unique table
    if cached[identifier] then 
        return table.deepcopy(cached[identifier])
    end

    local groupData = { };

    for k = 1, 50 do

        -- check if the group we're looking for exists.
        local name = identifier .. k;
        local exists = ScenarioUtils.FindUnitGroup(name, Scenario.Armies["Generators"].Units);

        if not exists then
            break;
        end

        -- retrieve the units of the goup.
        local units = ScenarioUtils.CreateArmyGroup("Generators", name, false);

        -- analyse the units
        local genericData = { };
        local artData = { };
        local shieldData = { };
        local stealthData = { };

        for k, unit in units do

            local inserted = false;

            -- are we a shield?
            if EntityCategoryContains(categories.SHIELD, unit) then
                table.insert(shieldData, unit:GetBlueprint().BlueprintId );
                inserted = true;
            end

            -- are we stealth providers?
            if EntityCategoryContains(categories.url0306, unit) then
                table.insert(stealthData, unit:GetBlueprint().BlueprintId );
                inserted = true;
            end

            -- are we artillery, tactical missile launchers or sniper bots?
            if EntityCategoryContains(
                categories.ARTILLERY + 
                (categories.MOBILE * categories.SILO * categories.INDIRECTFIRE) + 
                categories.xal0305 + categories.xsl0305, unit) then
                table.insert(artData, unit:GetBlueprint().BlueprintId );
                inserted = true;
            end

            -- are we just a generic, default and uninteresting land unit?
            if not inserted then
                table.insert(genericData, unit:GetBlueprint().BlueprintId );
            end

            -- remove the unit again. Hurr.
            unit:Destroy();
        end

        local platoonData = {
            Generics = genericData,
            Artilleries = artData,
            Shields = shieldData,
            Stealths = stealthData,
        }

        table.insert(groupData, platoonData);
    end

    LOG("Found " .. table.getn(groupData) .. " platoons for identifier: " .. identifier);

    -- cache it
    cached[identifier] = groupData;

    -- return a copy of the cache
    return table.deepcopy(cached[identifier]);

end

function CreatePlatoonRandomAtPoint(generator, army, spawnShields, spawnStealth)



end

function CreatePlatoonRandom(generator, army, location, spawnShields, spawnStealth)
    
end

function CreatePlatoon(generator, veterancy, army, platoonData, location, spawnShields, spawnStealth)

    -- spawn the default units
    local genericUnits = { }
    if platoonData.Generics then 
        genericUnits = GroundFunctionality.SpawnGroupOfUnits(platoonData.Generics, army, location);
    end

    local artilleryUnits = { };
    if platoonData.Artilleries then 
        artilleryUnits = GroundFunctionality.SpawnGroupOfUnits(platoonData.Artilleries, army, location);
    end

    -- spawn shields if applicable
    local shieldUnits = { };
    if spawnShields then
        if platoonData.Shields then 
            shieldUnits = GroundFunctionality.SpawnGroupOfUnits(platoonData.Shields, army, location);
        end
    end

    -- spawn stealths if applicable
    local stealthUnits = { };
    if spawnStealth then
        if platoonData.Stealths then 
            stealthUnits = GroundFunctionality.SpawnGroupOfUnits(platoonData.Stealths, army, location);
        end
    end

    -- make it a platoon.
    local brain = GetArmyBrain(army);
    local platoon = brain:MakePlatoon('', '');

    -- assign the units
    brain:AssignUnitsToPlatoon(platoon, genericUnits, 'Attack', 'GrowthFormation')
    brain:AssignUnitsToPlatoon(platoon, artilleryUnits, 'Artillery', 'GrowthFormation')
    brain:AssignUnitsToPlatoon(platoon, table.cat(stealthUnits, shieldUnits), 'Support', 'GrowthFormation')

    return platoon;

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- return a random grid

function GetRandomGrid() 

    -- over the span of the game, become more objective oriented
    local startTime = ScenarioInfo.Config.GeneralSettings.StartupTime
    local time = GetGameTimeSeconds() - startTime
    local threshold = math.min(1, time / 1100) * 0.5 + 0.1

    local rnd = Random();
    if rnd > threshold then
        -- as defined in GroundSetup.lua
        return ScenarioInfo.GridPlayers;
    end
    -- as defined in GroundSetup.lua
    return ScenarioInfo.GridObjective;
end

function Tick(
    generator,
    army, 
    utility, 
    tinkers,
    shieldRate, stealthRate,
    threshold,
    veterancy
)

    -- initialise and store locally
    local time = 0;
    local utilityCapacity = 0;
    local timeOffset = GetGameTimeSeconds();

    -- initialise pseudo randomness
    local tinkerRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(tinkers))
    );

    platoonRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Platoons))
    );

    regionRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Regions))
    );

    shieldRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, 11)
    );

    stealthRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, 11)
    );
    
    -- capacity can turn negative when the utility does
    while utilityCapacity >= 0 do

        -- sleep a bit
        WaitSeconds(1.0);

        -- keep track of our capacity
        time = GetGameTimeSeconds() - timeOffset;
        utilityCapacity = utilityCapacity + utility(time);

        -- generate / spawn platoons while our capacity exceeds the threshold
        while utilityCapacity > threshold do

            local grid = GetRandomGrid();

            utilityCapacity = utilityCapacity - threshold;

            -- determine a point to spawn at
            local regionIndex = regionRNG:GetValue();
            local region = generator.Regions[regionIndex];
            local point = {
                Random() * (region.x1 - region.x0) + region.x0, 
                0, 
                Random() * (region.y1 - region.y0) + region.y0
            };

            -- determine if we spawn mobile shields / stealth fields
            local shields = shieldRNG:GetValue() / 10.0 < shieldRate;
            local stealths = stealthRNG:GetValue() / 10.0 < stealthRate;

            -- spawn the platoon
            local platoonIndex = platoonRNG:GetValue();
            local platoonData = generator.Platoons[platoonIndex];
            local platoon = CreatePlatoon(generator, veterancy, army, platoonData, point, shields, stealths);

            -- add platoon AI
            local platoonAIIndex = tinkerRNG:GetValue();
            local platoonAI = tinkers[platoonAIIndex];

            -- aaaaannndddd liffttt offfff!!
            ForkThread(platoonAI, grid, platoon);

        end
    end

    LOG(generator.Identifier .. " is done generating.");
end
