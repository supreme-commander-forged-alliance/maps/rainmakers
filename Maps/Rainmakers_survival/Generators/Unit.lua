    
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Rnd = import(ScenarioInfo.path .. 'Algorithms/PseudoRandom.lua');

local debug = true;

Generator = { };
Generator.__index = Generator;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- constructs a target with typical target information

-- regions:     the regions that groupData can spawn in
-- identifier:  used to find the groupData that belong to this groundSpawner
function Generator:Setup( 
    regions, 
    identifier )

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- linking

    generator = { };
    setmetatable(generator, Generator);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- store relevant parameters

    generator.Identifier = identifier;
    generator.Regions = regions;    -- the regions in which we can spawn generated units

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- analyse the units that we can generate

    -- determine if the group exists
    local army = "Generators";
    local exists = ScenarioUtils.FindUnitGroup(identifier, Scenario.Armies[army].Units);
    if not exists then
        error("Identifier of generator is invalid: " .. identifier)
    end

    -- spawn the units
    local units = ScenarioUtils.CreateArmyGroup(army, identifier, false);

    -- store their blueprints, then destroy them again
    -- hurrrr
    generator.Blueprints = { }
    for k, unit in units do
        table.insert(generator.Blueprints, unit:GetBlueprint().BlueprintId);
        unit:Destroy();
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- generate some random'ness.

    generator.RegionsRng = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Regions))
    );

    generator.BlueprintsRng = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Blueprints))
    );

    return generator;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- creates a random units from the pool of units in the given region

function CreateRandomUnitAtRegion(
    generator,
    army,
    region,
    veterancy
)
    -- get a blueprint
    local bpi = generator.BlueprintsRng:GetValue();
    local bp = generator.Blueprints[bpi];

    -- create the unit
    return CreateUnit(generator, army, bp, region, veterancy);
end

function CreateRandomUnit(
    generator,
    army,
    veterancy
)
    -- get a blueprint
    local bpi = generator.BlueprintsRng:GetValue();
    local bp = generator.Blueprints[bpi];

    -- get a region
    local ri = generator.RegionsRng:GetValue();
    local region = generator.Regions[ri];

    -- create the unit
    return CreateUnit(generator, army, bp, region, veterancy);
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- creates the given unit in the given region

function CreateUnit(
    generator,
    army,
    bp, 
    region,
    veterancy
)

    -- get a random point within the region
    local point = {
        Random() * (region.x1 - region.x0) + region.x0, 
        0, Random() * (region.y1 - region.y0) + region.y0
    };

    -- spawn it, add some form of AI to it.
    local unit = CreateUnitHPR( 
        bp,				                    -- blueprint of the unit we want to build. (string)
        army,					            -- name of army. (string)
        point[1], point[2], point[3],	    -- position to spawn the unit at. (x, height, z)
        0, 0, 0						        -- orientation of the unit.
        );    

    unit:SetVeterancy(veterancy);

    return unit;
end 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- return a random grid

function GetRandomGrid() 
    local rnd = Random();
    if rnd > 0.3 then
        -- as defined in GroundSetup.lua
        return ScenarioInfo.GridPlayers;
    end
    -- as defined in GroundSetup.lua
    return ScenarioInfo.GridObjective;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- the generate / spawn loop

function Tick(
    generator,
    army, 
    utility, 
    tinkers,
    threshold,
    veterancy
)

    -- initialise and store locally
    local time = 0;
    local utilityCapacity = 0;
    local timeOffset = GetGameTimeSeconds();

    local tinkerRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(tinkers))
    );

    -- capacity can turn negative when the utility does
    while utilityCapacity >= 0 do

        -- sleep a bit
        WaitSeconds(1.0);

        -- keep track of our capacity
        time = GetGameTimeSeconds() - timeOffset;
        utilityCapacity = utilityCapacity + utility(time);

        -- generate / spawn units while our capacity exceeds the threshold
        while utilityCapacity > threshold do

            utilityCapacity = utilityCapacity - threshold;

            -- spawn a random unit from the generator
            local grid = GetRandomGrid();
            local unit = CreateRandomUnit(generator, army);

            -- make it unselectable.
            unit:SetUnSelectable(true);

            -- choose a random AI and send it out to find its dreams!
            local tinkerIndex = tinkerRNG:GetValue();
            ForkThread(tinkers[tinkerIndex], grid, unit);
        end
    end

    LOG(generator.Identifier .. " is done generating.");
end