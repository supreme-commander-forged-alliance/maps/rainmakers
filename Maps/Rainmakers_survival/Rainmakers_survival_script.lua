
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- when we go live
ScenarioInfo.path = "/maps/Rainmakers_survival/";

-- when we debug
-- ScenarioInfo.path = "/maps/Rainmakers_survival/";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

-- testing
local Utilities = import('/lua/utilities.lua')
local Weather = import('/lua/weather.lua')

------------------ ------------------ ------------------ ------------------ ------------------                              
-- UNDEFINED GLOBALS --
------------------ ------------------ ------------------ ------------------ ------------------

ScenarioInfo.Globals = { };
ScenarioInfo.Globals.ArtilleryObjectives = { };

------------------ ------------------ ------------------ ------------------ ------------------                              
-- UNDEFINED GLOBALS --
------------------ ------------------ ------------------ ------------------ ------------------ 

ScenarioInfo.Armies = { };
ScenarioInfo.Armies.Platoon = "HOSTILEPLATOONS";
ScenarioInfo.Armies.Stream = "HOSTILESTREAM";
ScenarioInfo.Armies.Objective = "Objective";
ScenarioInfo.Armies.Civilians = "Neutrals";
ScenarioInfo.Armies.Easter = "Easter";
ScenarioInfo.Armies.UEFOperative = "UEFOperative";
ScenarioInfo.Armies.OperativeHostileTakeOver = "OperativeHostileTakeOver";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- START UP --
------------------ ------------------ ------------------ ------------------ ------------------ 

function OnPopulate()

    ScenarioUtils.InitializeArmies()
    -- doscript(ScenarioInfo.path .. "Functionality/InitializeArmies.lua");
    
    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- turn on the campaign mode

    ScenarioInfo.CampaignMode = true
    Sync.CampaignMode = true
    import('/lua/sim/simuistate.lua').IsCampaign(true)

end

function OnStart(self)
  
  Weather.CreateWeather()

  LOG("Survival version: " .. ScenarioInfo.map_version .. ".");

  ------------------ ------------------ ------------------ ------------------ ------------------                              
  -- load in the configs

  doscript(ScenarioInfo.path .. "Config.lua");
  doscript(ScenarioInfo.path .. "GroundAI/GroundConfig.lua")

  doscript(ScenarioInfo.path .. "Functionality/ShowSettings.lua");

  -- show the current settings
  LOG(repr(ScenarioInfo.Config))

  ------------------ ------------------ ------------------ ------------------ ------------------                              
  -- do all kinds of small, but critical, functionality. Especially the colors

  doscript(ScenarioInfo.path .. "Functionality/PlayableArea.lua");
  doscript(ScenarioInfo.path .. "Functionality/Colors.lua");
  doscript(ScenarioInfo.path .. "Functionality/Diplomacy.lua");
  doscript(ScenarioInfo.path .. "Functionality/PlayableArea.lua");
  doscript(ScenarioInfo.path .. "Functionality/PrepareAllies.lua");
  doscript(ScenarioInfo.path .. "Functionality/PrepareHostiles.lua");
  doscript(ScenarioInfo.path .. "Functionality/PrepareEvents.lua");
  doscript(ScenarioInfo.path .. "Functionality/Restrictions.lua");
  doscript(ScenarioInfo.path .. "Functionality/Wreckages.lua");
  doscript(ScenarioInfo.path .. "Functionality/Environment.lua");

  doscript(ScenarioInfo.path .. "GroundAI/GroundSetup.lua")

  ------------------ ------------------ ------------------ ------------------ ------------------                              
  -- prevent resource sharing, set the victoir condition and set the unit caps

  local brainObjective = GetArmyBrain(ScenarioInfo.Armies.Objective);
  brainObjective:SetResourceSharing(false);

  -- allows us to choose our own victoir :).
  ScenarioInfo.Options.Victory = 'sandbox'

  SetArmyUnitCap(ScenarioInfo.Armies.Platoon, 2000);
  SetArmyUnitCap(ScenarioInfo.Armies.Stream, 2000);

end
