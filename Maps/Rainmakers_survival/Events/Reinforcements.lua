
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local titleReinforcementsObjective = "Reinforcements incoming";
local descriptionReinforcementsObjective = "A friendly commander operating nearby has made these units available to our operation.";

local timerReinforcementsObjectiveComplete = 35;
local timerDestroyReinforcementsUnits = 78;
local reinforcementsVeterancy = 3;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = "Reinforcements";
local reinforcementsArea = "REINFORCEMENTS";

-- transports:
-- aeon: 2 t3, 4t2 or 12 t1.
-- uef: 3 t3, 6 t2 or 16 t1.
-- cybran: 2 t3, 4 t2 or 10 t1
-- seraphin: 4 t3, 8 t2 or 16 t1.   
local X06_M01_030 = { {text = '<LOC X06_M01_030_010>[{i Fletcher}]: I\'ve been ordered to provide you with reinforcements, Commander. Just don\'t go thinking this makes us friends -- I know how you chip-heads operate.', vid = 'X06_Fletcher_M01_03905.sfd', bank = 'X06_VO', cue = 'X06_Fletcher_M01_03905', faction = 'UEF'},}
local X06_M01_050 = { {text = '<LOC X06_M01_050_010>[{i Rhiza}]: Here are reinforcements. Rhiza out.', vid = 'X06_Rhiza_M01_03909.sfd', bank = 'X06_VO', cue = 'X06_Rhiza_M01_03909', faction = 'Aeon'},}
local X06_M01_055 = { {text = '<LOC X06_M01_055_010>[{i Rhiza}]: Here are reinforcements, Champion. Use them to bring glory to the Princess. Rhiza out.', vid = 'X06_Rhiza_M01_04468.sfd', bank = 'X06_VO', cue = 'X06_Rhiza_M01_04468', faction = 'Aeon'},}
local X06_M01_020 = { {text = '<LOC X06_M01_020_010>[{i Fletcher}]: Here are some reinforcements, Colonel. And watch your back -- I\'ve got a bad feeling about this OP.', vid = 'X06_Fletcher_M01_03904.sfd', bank = 'X06_VO', cue = 'X06_Fletcher_M01_03904', faction = 'UEF'},}
local TAUNT30 = { {text = '<LOC X01_T01_260_010>[{i ShunUllevash}]: [Language Not Recognized]', vid = 'X01_Shun-Ullevash_T01_04351.sfd', bank = 'X01_VO', cue = 'X01_Shun-Ullevash_T01_04351', faction = 'Seraphim'},}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    event.spawnRegions = spawnRegions;
    event.pRnd = pRnd;

    return event;
end

function Event:Launch(eventInfo)

    -- prepare all the dataz.
    local r = Random();
    local player = eventInfo.StrPlayers[math.floor(r * table.getn(eventInfo.StrPlayers)) + 1];
    local brain = GetArmyBrain(player);

    -- find out which faction we're giving to.
    local factionIndex = brain:GetFactionIndex();
    local factions = { "UEF", "Aeon", "Cybran", "Seraphin"};
    local faction = factions[factionIndex];
    
    -- spawn the platoons.
    local airSupName = "ReinforcementsAirSup" .. faction;
    local platoonName = "Reinforcements" .. faction;
    local transportName = "ReinforcementsTrans" .. faction;
    local airSupUnits = ScenarioUtils.CreateArmyGroup(army, airSupName, false);
    local platoonUnits = ScenarioUtils.CreateArmyGroup(army, platoonName, false);
    local transportUnits = ScenarioUtils.CreateArmyGroup(army, transportName, false);

    -- find the spawn and unload points.
    local unloadRegion = ScenarioUtils.AreaToRect(reinforcementsArea);
    local spawnLocation = GroundFunctionality.FindSpawnLocation(self.spawnRegions, self.pRnd);
    local unloadLocation = Regions.RandomPointInRegion(unloadRegion);

    for k, unit in airSupUnits do

        -- make them a wee bit tougher.
        unit:SetVeterancy(reinforcementsVeterancy);

        -- make them guard a random transport.
        local transport = transportUnits[math.floor(Random() * table.getn(transportUnits) + 1)];
        IssueGuard({unit}, transport);
    end

    -- these are old hardy units!
    for k, unit in platoonUnits do
        unit:SetVeterancy(reinforcementsVeterancy);
    end

    -- attach 'dem units.
    ScenarioFramework.AttachUnitsToTransports(platoonUnits, transportUnits);
    
    -- teleport the transports so that they appear to be coming from a random direction.
    for k, transport in transportUnits do
        transport:SetCanBeKilled(false);
        -- todo: teleport them to a random spawn region?
    end

    -- unload the units at location
    IssueTransportUnload(transportUnits, unloadLocation);

    self.ObjectiveReinforcements = Objectives.Basic(
        'secondary',
        'incomplete',
        titleReinforcementsObjective,
        descriptionReinforcementsObjective,
        Objectives.GetActionIcon('protect'),
        {
            Units = platoonUnits,
            MarkUnits = true,
        }
    )

    -- checks if the units are attached. Once they are free, they are given to 
    function OnDetached()
        local attached = true;
        while attached do
            WaitSeconds(1)
            attached = false
            for k, unit in platoonUnits do
                if not unit.Dead  then
                    if unit:IsUnitState('Attached') then
                        -- keep checking until they are _all_ free.
                        attached = true
                    end
                end                
            end
        end

        -- do some screenings for good old Bollywood!
        if faction == "UEF" then
            ScenarioFramework.Dialogue(X06_M01_020, nil, false);
        end

        if faction == "Aeon" then
            local dialogues = { X06_M01_050, X06_M01_055 };
            EventFunctionality.PlayRandomDialogue(dialogues);
        end

        if faction == "Cybran" then
            ScenarioFramework.Dialogue(X06_M01_030, nil, false);
        end

        if faction == "Seraphin" then
            ScenarioFramework.Dialogue(TAUNT30, nil, false);
        end

        -- all unattached, gift them!
        for k, unit in platoonUnits do
            ScenarioFramework.GiveUnitToArmy(unit, brain:GetArmyIndex());
        end

        -- make all players neutral to the transports to prevent vision
        for k, player in eventInfo.StrPlayers do 
            SetAlliance(player, "Reinforcements", "Neutral");
        end

        -- move away again!
        WaitSeconds(0.5)
        IssueMove(transportUnits, spawnLocation);
    end

    ForkThread(OnDetached)

    -- remove the 'objective'. It takes roughly 30 seconds to reach the middle.
    ScenarioFramework.CreateTimerTrigger(
        function() 
            self.ObjectiveReinforcements:ManualResult(true);
        end,
        timerReinforcementsObjectiveComplete,
        true
    );

    -- remove the oldy transports from the game again.
    ScenarioFramework.CreateTimerTrigger(
        function() 

            for k, airsup in airSupUnits do
                airsup:Destroy();
            end

            for k, transport in transportUnits do
                transport:Destroy();
            end
        end,
        timerDestroyReinforcementsUnits,
        true
    );

end