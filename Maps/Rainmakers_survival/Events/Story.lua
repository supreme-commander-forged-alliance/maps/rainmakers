
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives

local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local PingGroups = import('/lua/ScenarioFramework.lua').PingGroups;

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

local startTime = ScenarioInfo.Config.GeneralSettings.StartupTime - 5;
local spyPlanes = ScenarioInfo.Config.GeneralSettings.StartupTime - 35;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    event.spawnRegions = spawnRegions;
    event.pRnd = pRnd;

    -- function PingCallBack(location)
    --     LOG(repr("ping!"));
    --     LOG(repr(location));
    -- end

    -- ScenarioInfo.M2AttackPing = PingGroups.AddPingGroup("title", nil, 'attack', "description")
    -- ScenarioInfo.M2AttackPing:AddCallback(PingCallBack)

    return event;
end

function Event:Launch(eventInfo)

    -- depending on the amount of players, do a different start sound thingy.
    -- only because we can :)!
    local numberOfPlayers = table.getn(eventInfo);
    if numberOfPlayers == 2 then
        ScenarioFramework.Dialogue(X04_M01_010, nil, false);
    else

    end

    -- the timer actually counts down in this version!
    self.ObjectiveTimer = Objectives.Timer(
        'secondary',
        'incomplete',
        'The calm before the storm',
        'The remaining time before the enemies triangulate our artillery installations. They will start sending units soon after.',
        {
            Timer = startTime,
            ExpireResult = 'complete',
        }
    );

    -- 
    ScenarioFramework.CreateTimerTrigger(
        function() 
            
            local scoutRegion = ScenarioUtils.AreaToRect("ScoutArea");

            -- spawn the spy planes
            local scouts = {
                ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Stream, "Scouts", false),
                ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Platoon, "Scouts", false)
            }

            for k, groupOfScouts in scouts do

                for k, scout in groupOfScouts do

                    -- determine a random position to scout.
                    local scoutLocation = Regions.RandomPointInRegion(scoutRegion);
                    IssueMove({scout}, scoutLocation);

                    -- move them to a random exit area
                    local destroyLocation = GroundFunctionality.FindSpawnLocation(self.spawnRegions, self.pRnd);
                    IssueMove({scout}, destroyLocation);

                    -- destroy it after some time.
                    local keepScoutInProgrammingScope = scout;
                    ScenarioFramework.CreateTimerTrigger(
                        function()
                            keepScoutInProgrammingScope:Destroy();
                        end,
                        50,
                        true
                    );


                end
            end
        end,
        spyPlanes,
        true
    );

    self.ObjectiveTimer:AddResultCallback(
        function(result)
            if result then

                -- let's get this party going!!
                local dialogues = { X02_M02_070, X02_M02_090, X02_M02_100 };
                EventFunctionality.PlayRandomDialogue(dialogues);
                
            else
                -- do nothing.
            end
        end
    );
end

X04_M01_010 = { {text = '<LOC X04_M01_010_010>[{i HQ}]: Both commanders are on-planet, and the OP is a go. HQ out.', vid = 'X04_HQ_M01_03594.sfd', bank = 'X04_VO', cue = 'X04_HQ_M01_03594', faction = 'NONE'},}

X02_M02_070 = { {text = '<LOC X02_M02_070_010>[{i HQ}]: Multiple attack waves incoming. HQ out.', vid = 'X02_HQ_M02_03541.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03541', faction = 'NONE'},}
X02_M02_090 = { {text = '<LOC X02_M02_090_010>[{i HQ}]: Enemy forces are converging on your position. HQ out.', vid = 'X02_HQ_M02_03543.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03543', faction = 'NONE'},}
X02_M02_100 = { {text = '<LOC X02_M02_100_010>[{i HQ}]: Enemy assault inbound. HQ out.', vid = 'X02_HQ_M02_03544.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03544', faction = 'NONE'},}

X02_M02_080 = { {text = '<LOC X02_M02_080_010>[{i HQ}]: Another attack is vectoring towards your position. HQ out.', vid = 'X02_HQ_M02_03542.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_03542', faction = 'NONE'},}
