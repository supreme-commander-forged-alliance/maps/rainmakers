
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local title = "Intel";
local description = "A local radar upgrade is stalling. Assist upgrading it in order to gain more intel."

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local X05_M01_012 = { {text = '<LOC X05_M01_012_010>[{i HQ}]: Get it in gear, Commander. HQ out.', vid = 'X05_HQ_M01_04909.sfd', bank = 'X05_VO', cue = 'X05_HQ_M01_04909', faction = 'NONE'},}
local X1T_TU07_040 = {{text = '<LOC X1T_TU07_040_010>[{i Fletcher}]: Can\'t fight if you\'re blind, Commander. Build that radar.', vid = 'X1T_Fletcher_TU07_05067.sfd', bank = 'X1T_VO', cue = 'X1T_Fletcher_TU07_05067', faction = 'UEF'},}

local army = ScenarioInfo.Armies.Objective;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    -- required to get the icon of the radar at the objective.
    local radar = ScenarioUtils.CreateArmyGroup(army, "Radar", false)[1];
    local upgradeBP = radar:GetBlueprint().General.UpgradesTo;
    local upRadar = IssueUpgrade({radar}, upgradeBP);
    event.radarT2 = radar;

    return event;
end

function Event:Launch(StrPlayers)

    ScenarioFramework.Dialogue(X1T_TU07_040, nil, true);

    function RadarBeat()

        local radart2 = self.radarT2

        -- get the radars
        local brain = GetArmyBrain(army);
        local radart3 = brain:GetListOfUnits(categories.STRUCTURE * categories.OMNI, false)[1];

        -- TODO: use the OnStopBeingBuilt callback?
        while true do

            WaitSeconds(11.0);
            
            if radart2.Dead then
                if not radart3.Dead then 
                    -- we're already fully upgraded. Good of us! Hooray!
                    self.ObjectiveUpgradeRadar:ManualResult(true);
                    radart3:SetConsumptionPerSecondEnergy(0);
                    break;
                else 
                    -- noes! We lost the radar!
                    self.ObjectiveUpgradeRadar:ManualResult(false);
                    break;
                end
            end

            -- pause the radar to make sure players need to help
            self.radarT2:SetPaused(true);

        end
    end

    ForkThread(RadarBeat);

    self.ObjectiveUpgradeRadar = Objectives.Basic(
        'secondary',                       
        'incomplete',                      
        title,            
        description,
        Objectives.GetActionIcon('protect'),
        {
            -- we add this for the icon of the radar.
            Units = {self.radarT2},
            MarkUnits = true,
        }
    );

end