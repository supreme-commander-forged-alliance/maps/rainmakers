
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

local PlatoonTinkers = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua')

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local objectiveTitle = "Reclaim in operational area";
local objectiveDescription = "Significant reclaim has been sighted in your operational area. Securing it is beneficial to the operation.";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local streamArmy = ScenarioInfo.Armies.Stream;
local platoonArmy = ScenarioInfo.Armies.Platoon;

local markerMoveTo = "EventReclaimEngineerPosition";
local markerVision = "EventReclaimVision";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    event.spawnRegions = spawnRegions;
    event.pRnd = pRnd;

    -- spawn the wreckage. Take note: players can see this from the start!
    local wreckage = ScenarioUtils.CreateArmyGroup(streamArmy, 'Wreckage', false);
    for num,unit in wreckage do
        event.WreckagePosition = unit:GetPosition();
        event.Wreckage = unit:CreateWreckageProp(0);
        unit:Destroy()
    end

    return event;
end

function Event:Launch(eventInfo)

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Spawn the engineer, move towards the reclaim and issue the reclaim command. Make it a 
    -- a wee bit more beefy to prevent sniping it easily.

    local engineerMovementMarker = ScenarioUtils.GetMarker(markerMoveTo);
    local engineer = ScenarioUtils.CreateArmyGroup(streamArmy, 'Engineer', false)[1];
    engineer:SetVeterancy(3);
    engineer:SetMaxHealth(10 * engineer:GetHealth());
    engineer:SetHealth(nil, 10 * engineer:GetHealth());
    IssueMove({engineer}, engineerMovementMarker.position);
    IssueReclaim({engineer}, event.Wreckage);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Spawn the platoons and make them guard the engineer.

    local platoons = { }
    for k = 1, ScenarioInfo.Config.GeneralSettings.Difficulty do
        local platoon = ScenarioUtils.CreateArmyGroupAsPlatoon(platoonArmy, "Platoon" .. k, 'AttackFormation');

        ForkThread(
            PlatoonTinkers.GuardUnit,   -- the AI function
            platoon,                    -- the platoon
            engineer,                   -- the unit to guard
            (2 * k + 3) * 8            -- the guard distance (for wandering behavior)        
        );

        table.insert(platoons, platoon)
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Make a platoon attack the instignator when the engineer is attacked, 

    function DamagedEngineer(self, instignator)
        -- check if the instignator is still alive
        if instignator and not instignator.Dead then 
            LOG("The engineer got attacked!")

            -- see if there is a platoon to save the engineer
            for k, platoon in platoons do 
                if GroundFunctionality.PlatoonAliveCheck(platoon) then 
                    PlatoonTinkers.EngageBehaviorA(platoon, instignator:GetPosition())
                    break
                end
            end

            LOG("here")
            -- make the trigger available again after a wee bit
            ScenarioFramework.CreateTimerTrigger(
                function() 
                    LOG("triggered")
                    ScenarioFramework.CreateUnitDamagedTrigger(
                        DamagedEngineer, self, -1, 1
                    );
                end,
                10,
                true
            );
        end
    end

    ScenarioFramework.CreateUnitDamagedTrigger(
        DamagedEngineer, engineer, -1, 1
    );

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Start the event once the engineer starts reclaiming.

    ScenarioFramework.CreateUnitStartReclaimTrigger(
        function () 

            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- Create vision on and around the reclaim, ping it

            local visionMarker = ScenarioUtils.GetMarker(markerVision);
            for k, player in eventInfo.StrPlayers do
                ScenarioFramework.CreateVisibleAreaLocation(18, visionMarker.position, 40, GetArmyBrain(player));
            end

            local army = 1 
            local position = ScenarioUtils.GetMarker(markerMoveTo).position
            Ping.AttackPing(position, army)

            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- If the wreckage is gone, the engineer dies causing the platoons to become regular platoons
            -- TODO: check if this can be done with some Trigger function.

            ForkThread(
                function()
                    while true do 

                        if event.Wreckage:BeenDestroyed() then
                            self.Objective:ManualResult(false);
                            engineer:Kill();
                            break;
                        end

                        WaitSeconds(2);
                    end
                end
            )

            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- Make the objective to alert players of what is happening.

            if eventInfo.MakeObjective then
                self.Objective = Objectives.Kill(
                    'secondary', 
                    'incomplete', 
                    objectiveTitle,        
                    objectiveDescription, 
                    {                          
                        Units = { engineer },
                        MarkUnits = true,
                    }
                );
            end
        end, 
        engineer
    );
end