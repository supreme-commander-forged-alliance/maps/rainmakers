
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;
local PingGroups = import('/lua/SimPingGroup.lua')

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local distance = 80;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Objective;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    event.scouts = ScenarioUtils.CreateArmyGroup(army, 'Scout', false)

    setmetatable(event, Event);

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    -- computes a single point on a circle.
    function ComputePoint(center, radius, radians)
        return {
            center[1] + radius * math.cos(radians),
            center[2] + 0,
            center[3] + radius * math.sin(radians),
        };
    end

    -- order the scouts to patrol the area
    function PatrolLocation(scouts, location)

        -- prevent the engine from auto targeting them
        for k, scout in scouts do
            if not scout.Dead then 
                scout:SetDoNotTarget(true)
            end
        end

        -- forgot all previous commands
        IssueClearCommands(scouts)

        -- compute the patrol locations
        local radius = 50
        local points = {
            ComputePoint(location, radius, 0/3 * 3.14),
            ComputePoint(location, radius, 2/3 * 3.14),
            ComputePoint(location, radius, 4/3 * 3.14)
        }

        -- go patrol!
        for k, point in points do 
            IssuePatrol(scouts, point)
        end
    end

    -- allow the player to control the scout
    group = PingGroups.AddPingGroup('Scout', 'ura0302', 'move', 'Order the scout to recon the located area.')
    group:AddCallback(function(location) PatrolLocation(self.scouts, location) end)

    -- check if scouts have died, check for removal
    local numberOfScouts = table.getn(event.scouts)
    local numberOfFallenScouts = 0

    for k, scout in event.scouts do 
        ScenarioFramework.CreateUnitDeathTrigger(
            function() 
                numberOfFallenScouts = numberOfFallenScouts + 1

                -- if no more scouts are alive, remove them all
                if numberOfFallenScouts >= numberOfScouts then 
                    group:Destroy()
                    LOG("Removing the ping group for scouts.")
                end
            end, 
            scout
        )
    end

    -- give the scouts infinite fuel, assuming they are still alive
    ForkThread(
        function(scouts)
            while numberOfFallenScouts < numberOfScouts do 
                WaitSeconds(60)
                for k, scout in scouts do 
                    if not scout.Dead then 
                        scout:SetFuelRatio(1.0)
                    end
                end
            end
        end,
        self.scouts
    )

end