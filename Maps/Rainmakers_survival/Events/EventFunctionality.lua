
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 
local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local GroundFunctionality = import(ScenarioInfo.path .. 'GroundAI/GroundFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- choses a random dialogue from the table of dialogues and plays the dialogue.
function PlayRandomDialogue(dialogues)
    ScenarioFramework.Dialogue(dialogues[math.floor(Random() * table.getn(dialogues)) + 1], nil, false);
end

-- analyses the (experimental) platoons.
function AnalyseExperimentalPlatoons(name, army)

    local data = { };

    -- first, find all platoons that accompany this experimental.
    data.platoons = { };
    for k = 1, 10 do
        -- check whether the current platoon we're trying to find exists.
        local identifier = name .. "_Platoon" .. k;
        local exists = ScenarioUtils.FindUnitGroup(identifier, Scenario.Armies[army].Units);

        -- if not, stop. We've found all our gaurd platoons.
        if not exists then
            break;
        end

        -- spawn the units of the group.
        local units = ScenarioUtils.CreateArmyGroupAsPlatoon(army, identifier, 'AttackFormation'):GetPlatoonUnits();

        -- track the blueprint data, then throw everything out again.
        local sTable = { };
        for k, unit in units do
            table.insert(sTable, unit:GetBlueprint().BlueprintId);
            unit:Destroy();
        end

        table.insert(data.platoons, sTable);
    end

    -- then find the experimental.
    local identifier = name .. "_Experimental"; 
    local exists = ScenarioUtils.FindUnitGroup(identifier, Scenario.Armies[army].Units);

    -- if it does not exist, then we messed up!
    if not exists then
        error("given experimental eventData has no experimental: " .. name .. ' (' .. identifier .. ')');
    end

    local unit = ScenarioUtils.CreateArmyGroupAsPlatoon(army, identifier, 'AttackFormation'):GetPlatoonUnits()[1];
    data.experimental = unit:GetBlueprint().BlueprintId;
    unit:Destroy();

    return data;

end

function PatrolUnitAI(platoon, position, radius)
    ForkThread(PatrolUnitAIThread, platoon, position, radius);
end

function PatrolUnitAIThread(platoon, target, radius)

    local variation = Random();
    local attacking = false;

    while not platoon.BecomeGenericPlatoon do

        -- determine if the platoon is alive.
        if not GroundFunctionality.PlatoonAliveCheck(platoon) then
            break;
        end

        local targetPosition = target:GetPosition();

        -- if there are no nearby targets, wander in the area.
        if not attacking then

            local randomPosition = 
                GeneralFunctionality.VectorAddition(
                    targetPosition,
                    GeneralFunctionality.VectorScalarMultiply(
                        GeneralFunctionality.VectorRandom(),
                        radius
                    )
                );

            platoon:MoveToLocation(randomPosition, false);
        end

        -- determine if there are nearby targets.
        for k = 1, 4, 1 do

            -- determine the number of local targets surrounding the target.
            local platoonPosition = platoon:GetPlatoonPosition();
            
            local nearbyTargets = GroundFunctionality.FindLocalTargets(targetPosition, 2000, 3);
            local numberOfTargets = table.getn(nearbyTargets);

            -- if there are any targets, engage!
            if numberOfTargets > 0 then
                attacking = true;
                local positionToSiege = GroundFunctionality.LocalTargetsEngageVector(platoonPosition, nearbyTargets);
                platoon:Stop();
                platoon:AggressiveMoveToLocation(positionToSiege);
            else
                attacking = false;
            end

            WaitSeconds(4.0 + variation);   
        end
    end

    -- become a generic platoon.
    ScenarioInfo.Globals.PlatoonManager:AddPlatoon(platoon);
end

function PatrolPositionAI(platoon, position, radius)
    ForkThread(PatrolPositionAIThread, platoon, position, radius);
end

function PatrolPositionAIThread(platoon, position, radius)

    local variation = Random();
    local attacking = false;

    while not platoon.BecomeGenericPlatoon do

        -- determine if the platoon is alive.
        if not GroundFunctionality.PlatoonAliveCheck(platoon) then
            break;
        end

        -- if there are no nearby targets, wander in the area.
        if not attacking then

            local randomPosition = 
                GeneralFunctionality.VectorAddition(
                    position,
                    GeneralFunctionality.VectorScalarMultiply(
                        GeneralFunctionality.VectorRandom(),
                        radius
                    )
                );

            platoon:MoveToLocation(randomPosition, false);
        end

        -- determine if there are nearby targets.
        for k = 1, 4, 1 do

            -- determine the number of local targets surrounding the target.
            local platoonPosition = platoon:GetPlatoonPosition();
            

            local nearbyTargets = GroundFunctionality.FindLocalTargets(position, 2000, 3);
            local numberOfTargets = table.getn(nearbyTargets);

            -- if there are any targets, engage!
            if numberOfTargets > 0 then
                attacking = true;
                local positionToSiege = GroundFunctionality.LocalTargetsEngageVector(platoonPosition, nearbyTargets);
                platoon:Stop();
                platoon:AggressiveMoveToLocation(positionToSiege);
            else
                attacking = false;
            end

            WaitSeconds(4.0 + variation);   
        end
    end

    ScenarioInfo.Globals.PlatoonManager:AddPlatoon(platoon);
end