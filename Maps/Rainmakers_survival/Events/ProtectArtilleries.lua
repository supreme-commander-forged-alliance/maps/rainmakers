
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local Game = import(ScenarioInfo.path .. 'Functionality/EndGame.lua')

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local titleTimerObjective = 'Time to hold out';
local descriptionTimerObjective = 'The time left for the installations to assist the off-site siege.';

local titleProtectObjective = "Protect the artillery installations.";
local descriptionProtectObjective = "These installations are vital to an off-site siege. Protect them at all costs.";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local X05_DB01_030 = { {text = '<LOC X05_DB01_030_010>[{i HQ}]: The operation has ended in failure. All is lost.', vid = 'X05_HQ_DB01_04956.sfd', bank = 'Briefings', cue = 'X05_HQ_DB01_04956', faction = 'NONE'}, }

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    return event;
end

function Event:Launch(eventInfo)

    local eventIsActive = true
    local surviveTime = ScenarioInfo.Config.GeneralSettings.ObjectiveTime + ScenarioInfo.Config.GeneralSettings.StartupTime


    function Cinematics()
        local function cinematics (unit)
            -- we can't use self.TimerObjective.Active here since that 
            -- is changed _before_ this function is called
            if eventIsActive then 
                ScenarioFramework.CDRDeathNISCamera(unit, 3.0)
            end
        end
    
        -- do some fancy cinematics if they die
        for k, artillery in ScenarioInfo.Globals.ArtilleryObjectives do 
            ScenarioFramework.CreateUnitDeathTrigger(cinematics, artillery);
        end
    
    end

    function Timer()

        -- the timer actually counts down in this version!
        self.TimerObjective = Objectives.Timer(
            'primary',
            'incomplete',
            titleTimerObjective,
            descriptionTimerObjective,
            {
                Timer = surviveTime,                  -- time in seconds
                ExpireResult = 'complete',
            }
        );

        self.TimerObjective:AddResultCallback(
            function(result)
                if result then
                    eventIsActive = false
                    -- call the recall event
                    ScenarioFramework.CreateTimerTrigger(
                        function() 
                            eventInfo.OtherEvents.Recall:Launch(eventInfo);
                        end,
                        3,
                        true
                    );

                    -- prevent them from attacking.
                    IssueClearCommands(ScenarioInfo.Globals.ArtilleryObjectives)
                    for k, objective in ScenarioInfo.Globals.ArtilleryObjectives do
                        objective:SetFireState('HoldFire');
                    end

                    -- complete the protection objective.
                    self.ProtectObjective:ManualResult(true);
                else
                    -- do nothing.
                end
            end
        );
    end

    --- Prepares the protect objective
    function Protect()

        -- create the objective
        self.ProtectObjective = Objectives.Protect(
            'primary',                      
            'incomplete',                   
            titleProtectObjective,  
            descriptionProtectObjective,  
            {                               
                Units = ScenarioInfo.Globals.ArtilleryObjectives,
            }
        );

        -- add a callback
        self.ProtectObjective:AddResultCallback(
            function(result)
                if not result then
                    -- failure!
                    self.TimerObjective:ManualResult(false);

                    -- do some Hollywood. About destruction.
                    ScenarioFramework.Dialogue(X05_DB01_030, nil, false);

                    -- end the game after a few seconds.
                    ScenarioFramework.CreateTimerTrigger(
                        function() 
                            for k, player in eventInfo.StrPlayers do
                                GetArmyBrain(player):OnDefeat();
                            end

                            Game.End(false);
                        end,
                        5.0,
                        true
                    );
                end
            end
        );
    end

    Timer()
    Protect()
    Cinematics()

end