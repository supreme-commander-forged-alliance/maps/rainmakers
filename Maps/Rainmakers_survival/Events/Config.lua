
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- I know you're thinking it, why else open up this file?
-- but, I am asking you not to edit this file. Nasty desyncs.
-- ~Jip

--------------------------------------------------------------------------------------------------
-- Event customizeable settings                                                                --

do 

    if not ScenarioInfo.Config then
        ScenarioInfo.Config = { };
    end

--------------------------------------------------------------------------------------------------

    if (ScenarioInfo.Options.CommanderRequirement == nil) then
        ScenarioInfo.Options.CommanderRequirement = 1;
    end

    -- save them all in the Config.
    ScenarioInfo.Config.EventSettings = {
        CommanderRequirement = ScenarioInfo.Options.CommanderRequirement,
    }

end