
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local distance = 80;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Objective;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    -- construct the scouts
    units = ScenarioUtils.CreateArmyGroupAsPlatoon(army, 'Scouts', 'AttackFormation'):GetPlatoonUnits();

    -- determine the offsets.




    for k, unit in units do

        -- determine the center of the unit
        local center = unit:GetPosition();

        -- compute some arbitrary offset
        local distanceOffset = distance + Random() * 10 - 5;
        local offsets = {
            GeneralFunctionality.Vector(distanceOffset, 0, distanceOffset),
            GeneralFunctionality.Vector(-distanceOffset, 0, distanceOffset),
            GeneralFunctionality.Vector(-distanceOffset, 0, -distanceOffset),
            GeneralFunctionality.Vector(distanceOffset, 0, -distanceOffset),
        }

        -- determine the patrol locations
        local locations = { }
        for l, offset in offsets do 
            local i = math.mod((l + k), 4)+ 1;
            local o = offsets[i]
            table.insert(locations, GeneralFunctionality.VectorAddition(center, o));
        end

        -- move to the first location
        IssueMove({unit}, locations[1])

        -- patrol over the other locations
        for l, location in locations do 
            IssuePatrol ({unit}, location);
        end
    end
end