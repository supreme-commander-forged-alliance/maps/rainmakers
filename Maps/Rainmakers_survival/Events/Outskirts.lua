
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

local army = ScenarioInfo.Armies.Easter;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    -- spawn the units
    event.e1 = ScenarioUtils.CreateArmyGroup(army, 'EncampmentLarge', false)
    event.e2 = ScenarioUtils.CreateArmyGroup(army, 'EncampmentSmall1', false)
    event.e3 = ScenarioUtils.CreateArmyGroup(army, 'EncampmentSmall2', false)

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    function GetUnitsInRadius(location, radius)

        -- retrieve all units in there
        local x1 = location[1] - radius
        local y1 = location[2] - radius
        local z1 = location[3] - radius
        local x2 = location[1] + radius
        local y2 = location[2] + radius
        local z2 = location[3] + radius
        local rect = Rect(x1, z1, x2, z2)
        local UnitsinRec = GetUnitsInRect(rect)
        local validUnits = EntityCategoryFilterDown(categories.LAND, UnitsinRec)

        -- filter out the ones from the event
        -- filter out the ones that are too far away
        local units = { }
        for k, unit in validUnits do 
            -- do not take into account units on transporst
            if not unit:IsUnitState('Attached') then
                local army = ListArmies()[unit:GetArmy()];
                -- only take into account players
                if table.find(eventInfo.StrPlayers, army) then
                    local position = unit:GetPosition() 
                    -- only take units that are within the radius
                    -- if VDist2Sq(location[1], location[3], position[1], position[3]) < radius * radius then 
                    table.insert(units, unit)
                    -- end
                end
            end 
        end

        return units
    end

    function CheckSurroundings(gifts)

        local radius = 40

        -- compute the center of these units
        local center = { 0, 0, 0 }
        local count = table.getn(gifts)
        for k, unit in gifts do 
            local position = unit:GetPosition()
            center[1] = center[1] + position[1] * (1.0 / count)
            center[3] = center[3] + position[3] * (1.0 / count)
        end
        center[2] = GetSurfaceHeight(center[1], center[3])

        if false then
            ForkThread(
                function()
                    while true do 
                        DrawCircle(center, radius, 'ff00ff')
                        WaitSeconds(0.1)
                    end
                end
            )
        end


        -- start checking to gift
        local gifting = false 
        local identifier = 0

        while not gifting do 

            -- get all units in the radia
            local units = GetUnitsInRadius(center, radius)

            -- for every unit we want to gift
            for k, gift in gifts do 
                local pg = gift:GetPosition()
                -- for every unit nearby
                if not gifting then 
                    for l, unit in units do 
                        local pu = unit:GetPosition()
                        -- check if it is touching us
                        local distance = VDist2Sq(pg[1], pg[3], pu[1], pu[3]);
                        if distance < 12 then
                            local brain = unit:GetAIBrain()
                            identifier = brain:GetArmyIndex()
                            gifting = true
                            break;
                        end
                    end
                end
            end

            -- gift everything
            if gifting then 
                for k, gift in gifts do 
                    ScenarioFramework.GiveUnitToArmy(gift, identifier)
                end
            end

            -- wait a bit before we check again
            WaitSeconds(1.0)
        end
    end

    -- make them all tick...
    ForkThread(
        CheckSurroundings,
        event.e1
    )

    -- tock...
    ForkThread(
        CheckSurroundings,
        event.e2
    )

    -- tick...
    ForkThread(
        CheckSurroundings,
        event.e3
    )

end