
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Utilities = import(ScenarioInfo.path .. 'Utilities.lua')

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

local GroundFunctionality = import(ScenarioInfo.path .. 'GroundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local PlatoonTinkers = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua')

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

-- determines when we consider a local target a threat. Equals to the amount
-- of mass a threat is worth. If it is less, then we do not consider it 
-- a threat. To prevent us from chasing scouts :).
local threatValueThreshold = 2700;

-- determines the distance we'll search from the origin node on the
-- grid. If, say, the value is 3 then we'll keep track of a three node
-- distance in all directions from the origin node.
local threatSearchDistance = 5;

-- the objective title / description.
local objectiveTitle = "Fatboy sighted";
local objectiveDescription = "An hostile Fatboy has been sighted in your operational area. Neutralize before it can reach the installations.";

-- the spawn offset of the platoons. Not quite interesting.
local spawnOffsetDistance = 15;

-- the speed multiplier for the experimental. If it moves too fast,
-- the guarding looks weird / fails.
local speedMultiplier = 1.01;

-- the amount of vision and the duration of that vision
-- that the players gain when the experimental spawns.
local visionRadius = 80;
local visionDuration = 40;

-- the dialogues for when the experimental spawns.
XGG_GD1_380 = { {text = '<LOC XGG_GD1_380_010>[{i HQ}]: Commander, an enemy Fatboy has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04179.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04179', faction = 'NONE'}, }
X02_M02_160 = { {text = '<LOC X02_M02_160_030>[{i HQ}]: Got a barrel of fun rolling your way, Commander. HQ out.', vid = 'X02_HQ_M02_04280.sfd', bank = 'X02_VO', cue = 'X02_HQ_M02_04280', faction = 'NONE'}, }
local dialoguesOnSpawn = { XGG_GD1_380, X02_M02_160 };

-- priorities for the experimental when it is doing its thing
local experimentalPrioritiesGeneric = { }
-- guass guns
experimentalPrioritiesGeneric[1] = { categories.EXPERIMENTAL, categories.TECH3, categories.COMMAND, categories.TECH2, categories.ALLUNITS - categories.SPECIALLOWPRI }
experimentalPrioritiesGeneric[2] = { categories.EXPERIMENTAL, categories.TECH3, categories.COMMAND, categories.TECH2, categories.ALLUNITS - categories.SPECIALLOWPRI }
experimentalPrioritiesGeneric[3] = { categories.EXPERIMENTAL, categories.TECH3, categories.COMMAND, categories.TECH2, categories.ALLUNITS - categories.SPECIALLOWPRI }
experimentalPrioritiesGeneric[4] = { categories.EXPERIMENTAL, categories.TECH3, categories.COMMAND, categories.TECH2, categories.ALLUNITS - categories.SPECIALLOWPRI }

-- riot guns
experimentalPrioritiesGeneric[5] = { categories.SHIELD, categories.COMMAND, categories.TECH3, categories.ALLUNITS }
experimentalPrioritiesGeneric[6] = { categories.SHIELD, categories.COMMAND, categories.TECH3, categories.ALLUNITS }

-- priorities for the platoons
local platoonPriorities = { categories.SHIELD, categories.COMMAND, categories.EXPERIMENTAL, categories.TECH3, categories.ALLUNITS - categories.SPECIALLOWPRI}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Platoon;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    -- retrieve all event data, such as the experimental and the platoons.
    event.pRnd = pRnd;
    event.spawnRegions = spawnRegions;
    event.data = EventFunctionality.AnalyseExperimentalPlatoons("EFB", army);

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;
    local oppertunities = ScenarioInfo.GridObjective;

    -- determine the spawn locations
    local area = self.spawnRegions[self.pRnd:GetValue()];
    local count = table.getn(self.data.platoons)
    local spawns = Regions.PointsOnLongestAxis(area, count)

    -- determine time before spawning
    local holdup = {
        10, 0, 30, 30, 0, 10
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Spawn the experimental, ping it after some delay

    local location = Regions.RandomPointInRegion(area);
    local experimental = GroundFunctionality.SpawnUnit(self.data.experimental, army, area.center);
    experimental:SetSpeedMult(speedMultiplier);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Start the experimentals AI. Slow it down slightly such that the platoons can keep up.

    ScenarioFramework.CreateUnitDamagedTrigger(
        function(self, instignator)
            experimental:SetSpeedMult(1.2)
            LOG("A fatboy is in panic!")
        end, 
        experimental, 
        0.05
    );

    function ExperimentalBehaviour(experimental, oppertunities)

        local target = nil;
        local stopping = false;
        local fallingBack = false;

        while not experimental.Dead do

            -- find a target 
            local position = experimental:GetPosition();

            -- check if we need to hold up
            local range = 80;
            local threshold = 400;
            local threats, hazardousness = hazards:FindLocalHazards(position, range, threshold);
            local count = table.getn(threats);

            local stopping = false;
            if hazardousness > 3000 and not fallingBack then 
                -- move the experimental away from the targets. Keep track of the fact that we're falling back.
                IssueClearCommands({experimental});
                stopping = true;
                LOG('Fatboy found nearby threats, stopping.');
            end

            fallingBack = false;
            if hazardousness > 7000 then 
                -- compute the direction we should fall back into.
                local threat = PlatoonTinkers.ComputeEngageLocation(position, threats)

                -- don't normalize the y direction (up / down)
                local direction = GeneralFunctionality.VectorSubtraction(position, threat)
                direction[2] = 0

                -- determine some position just behind us
                local normalized = GeneralFunctionality.VectorNormalize(direction)
                local scaled = GeneralFunctionality.VectorScalarMultiply(normalized, 8.0)
                local locationToMoveTo = GeneralFunctionality.VectorAddition(position, scaled)

                -- move the experimental away from the targets. Keep track of the fact that we're falling back.
                IssueClearCommands({experimental});
                IssueMove({experimental}, locationToMoveTo);
                fallingBack = true;
                LOG('Fatboy is falling back.');
            end

            -- once we're done being a chicken
            if not fallingBack and not stopping then 
                if experimental:IsIdleState() then 

                    experimental:SetSpeedMult(speedMultiplier)

                    -- check if we got anything to go to
                    if not oppertunities:OppertunitiesAvailable() then 
                        if oppertunities == ScenarioInfo.GridObjective then 
                            LOG("Fatboy cannot find new oppertunities - switching to player oppertunities.");
                            oppertunities = ScenarioInfo.GridPlayers;
                        else
                            LOG("Fatboy cannot find new oppertunities.");
                            break;
                        end
                    end

                    target = oppertunities:GetNearestHighOppertunity(position);
                    if target then 
                        -- find a path
                        local options = { smoothen = true, force = 19000 };
                        local path = pathing:FindPath(position, target.position, options)

                        -- move in an aggressive, angry looking fashion
                        local count = table.getn(path);
                        IssueClearCommands({experimental});
                        for k = 1, count - 2, 1 do 
                            IssueMove({experimental}, path[k]);
                        end

                        -- get totally angry at everyone around it
                        IssueAggressiveMove({experimental}, target.position)
                        LOG("Fatboy is on the move.");
                    end
                else
                    -- check if we should reconsider
                    if target then 
                        local node = oppertunities:GetNode(target.position);
                        if node.oppertunity < 0.5 * target.oppertunity  then
                            -- if the target isn't what it was a few seconds ago, make the platoon stop
                            IssueClearCommands({experimental});
                            LOG("Fatboy is reconsidering.");
                        end
                    end
                end
            end

            WaitSeconds(2.0);

        end

        LOG("Fatboy died.");
    end

    ForkThread(ExperimentalBehaviour, experimental, oppertunities);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Construct the platoons

    local index = 0
    local platoons = { }
    local brain = GetArmyBrain(army);
    for k, bps in self.data.platoons do 
        -- store locally in each iteration, prevents losing k when using it in a forkthread
        index = index + 1;

        -- spawn the units
        local units = { };
        local point = spawns[index]
        for _, bp in bps do
            -- spawn the unit, then keep track of it
            local unit = CreateUnitHPR(bp, army, point[1], point[2], point[3], 0, 0, 0)
            table.insert(units, unit)
        end

        -- set their priorities
        Utilities.SetPriorities(units, platoonPriorities)

        -- turn it into a platoon
        local platoon = brain:MakePlatoon('', '')
        brain:AssignUnitsToPlatoon(platoon, units, 'Attack', 'AttackFormation')
        table.insert(platoons, platoon)

        -- make it tinker
        local lk = k;
        if lk == 3 or lk == 4 then 
            -- assault bots that guard the fatboy
            ForkThread(
                function (...) 
                    WaitSeconds(holdup[lk]) 
                    PlatoonTinkers.GuardUnit(unpack(arg))
                end,
                platoon,
                experimental,
                15
            );
        else
            -- regular platoons
            ForkThread(
                function(...)
                    WaitSeconds(holdup[lk])
                    PlatoonTinkers.MoveNearestHighOppertunity(unpack(arg))
                end,
                oppertunities,
                platoon
            );
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Give it a real campaign feeling to it.

    WaitSeconds(15.0)

    -- sound notification
    if eventInfo.MakeDialogue then
        EventFunctionality.PlayRandomDialogue(dialoguesOnSpawn);
    end

    -- objective notification
    if eventInfo.MakeObjective then
        self.ObjectiveColossus = Objectives.Kill(
            'secondary', 
            'incomplete', 
            objectiveTitle,        
            objectiveDescription, 
            {                          
                Units = { experimental },
                MarkUnits = true,
            }
        )
    end

    -- give temporarily vision
    if eventInfo.MakeVision then
        for k, player in eventInfo.StrPlayers do
            ScenarioFramework.CreateVisibleAreaAtUnit(visionRadius, experimental, visionDuration, GetArmyBrain(player));
        end
    end

    -- ping it
    ScenarioFramework.CreateTimerTrigger(
        function() 
            local army = 1 
            local position = experimental:GetPosition()
            Ping.AttackPing(position, army)
        end,
        10,
        true
    );

end

XGG_GD1_440 = { {text = '<LOC XGG_GD1_440_010>[{i HQ}]: Commander, an enemy Colossus has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04185.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04185', faction = 'NONE'},}
X01_M02_281 = { {text = '<LOC X01_M02_281_010>[{i HQ}]: Got a Galactic Colossus heading your way. Deal with it. HQ out.', vid = 'X01_HQ_M02_04889.sfd', bank = 'X01_VO', cue = 'X01_HQ_M02_04889', faction = 'NONE'},}