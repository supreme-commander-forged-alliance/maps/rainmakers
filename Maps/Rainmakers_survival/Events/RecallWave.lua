
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

local Rnd = import(ScenarioInfo.path .. 'Algorithms/PseudoRandom.lua');
local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(regions)

    -- linking
    event = { };
    setmetatable(event, Event);

    -- initialize a bunch of generators, we'll spawn units depending on the difficulty
    event.generators = {
        GenPlatoonFramework.Generator:Setup(regions, "Tech1-2:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech2-2:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech2-3:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech3-3:"),
    }

    event.regions = regions
 
    return event;
end

function Event:Launch(eventInfo)

    -- spawn some units that will move to the recall area
    local difficulty = ScenarioInfo.Config.GeneralSettings.Difficulty
    local generator = self.generators[math.min(4, difficulty + 1)]

    local platoonRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(generator.Platoons))
    );

    local regionRNG = Rnd.PseudoRandom:Initiate(
        Rnd.IndicesFromRange(1, table.getn(self.regions))
    );

    local center = {
        0.5 * ScenarioInfo.size[1], 
        GetSurfaceHeight(ScenarioInfo.size[1], ScenarioInfo.size[2]), 
        0.5 * ScenarioInfo.size[2]
    };

    -- platoon stats
    local shields = true
    local stealths = false
    local veterancy = difficulty 
    local army = ScenarioInfo.Armies.Platoon
    local formation = "AttackFormation" 
    --local formation = "GrowthFormation" 

    local multipliers = {
        range = 1.65, 
        max = 0.3,
        total = 0.4
    }

    -- spawn 3 to 6 platoons that move towards the recall zone
    for k = 1, difficulty + 2 do 

        -- determine a location to spawn at
        local ri = regionRNG:GetValue();
        local region = self.regions[ri];
        local point = {
            Random() * (region.x1 - region.x0) + region.x0, 
            0, 
            Random() * (region.y1 - region.y0) + region.y0
        };

        -- determine a platoon to spawn
        local pi = platoonRNG:GetValue();
        local platoonData = generator.Platoons[pi];
        local platoon = GenPlatoonFramework.CreatePlatoon(generator, veterancy, army, platoonData, point, shields, stealths);

        -- send it out!
        local distance = 30 + 5 * difficulty
        ForkThread(
            function() 
                TinkerPlatoonFramework.GuardLocationIndefinite(platoon, formation, center, distance, multipliers) 
            end
        )
    end

end