
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Utilities = import(ScenarioInfo.path .. 'Utilities.lua')

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'GroundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

local PlatoonTinkers = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua')

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

-- determines when we consider a local target a threat. Equals to the amount
-- of mass a threat is worth. If it is less, then we do not consider it 
-- a threat. To prevent us from chasing scouts :).
local threatValueThreshold = 1600;

-- determines the distance we'll search from the origin node on the
-- grid. If, say, the value is 3 then we'll keep track of a three node
-- distance in all directions from the origin node.
local threatSearchDistance = 3;

-- the objective title / description.
local objectiveTitle = "Monkeylord sighted";
local objectiveDescription = "An hostile Monkey Lord has been sighted in your operational area. Neutralize before it can reach the installations.";

-- the spawn offset of the platoons. Not quite interesting.
local spawnOffsetDistance = 15;

-- the speed multiplier for the experimental. If it moves too fast,
-- the guarding looks weird / fails.
local speedMultiplier = 0.9;

-- the amount of vision and the duration of that vision
-- that the players gain when the experimental spawns.
local visionRadius = 80;
local visionDuration = 40;

-- the dialogues for when the experimental spawns.
XGG_GD1_410 = { {text = '<LOC XGG_GD1_410_010>[{i HQ}]: Commander, an enemy Monkeylord has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04182.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04182', faction = 'NONE'},}
local dialoguesOnSpawn = { XGG_GD1_410 };

-- priorities for the experimental when it is doing its thing
local experimentalPrioritiesGeneric = { }
experimentalPrioritiesGeneric[1] = { categories.EXPERIMENTAL, categories.TECH3, categories.TECH2, categories.COMMAND, categories.ALLUNITS }
experimentalPrioritiesGeneric[2] = { categories.COMMAND, categories.SHIELD, categories.ALLUNITS }
experimentalPrioritiesGeneric[3] = { categories.SHIELD, categories.COMMAND, categories.ALLUNITS }

-- priorities for the experimental when it feels cornered
local experimentalPrioritiesPanic = { }
experimentalPrioritiesPanic[1] = {categories.EXPERIMENTAL, categories.COMMAND, categories.TECH3, categories.ALLUNITS - categories.SPECIALLOWPRI}
experimentalPrioritiesPanic[2] = {categories.COMMAND, categories.SHIELD, categories.ALLUNITS - categories.SPECIALLOWPRI}
experimentalPrioritiesPanic[3] = {categories.SHIELD, categories.COMMAND, categories.ALLUNITS - categories.SPECIALLOWPRI}

-- priorities for the platoons
local platoonPriorities = { categories.SHIELD, categories.DEFENSE * categories.DIRECTFIRE * categories.STRUCTURE, categories.ALLUNITS - categories.SPECIALLOWPRI}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Platoon;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    -- retrieve all event data, such as the experimental and the platoons.
    event.pRnd = pRnd;
    event.spawnRegions = spawnRegions;
    event.data = EventFunctionality.AnalyseExperimentalPlatoons("ESP", army);

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Preparing the launch.

    -- global scope into local scope
    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;
    local oppertunities = ScenarioInfo.GridObjective;

    -- determine the spawn locations
    local area = self.spawnRegions[self.pRnd:GetValue()];
    local count = table.getn(self.data.platoons)
    local spawns = Regions.PointsOnLongestAxis(area, count)

    -- determine time before spawning
    local holdup = {
        10, 0, 0, 10
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Construct the platoons

    local index = 0
    local platoons = { }
    local brain = GetArmyBrain(army);
    for k, bps in self.data.platoons do 
        -- store locally in each iteration, prevents losing k when using it in a forkthread
        index = index + 1;

        -- spawn the units
        local units = { };
        local point = spawns[index]
        for _, bp in bps do
            -- spawn the unit, then keep track of it
            local unit = CreateUnitHPR(bp, army, point[1], point[2], point[3], 0, 0, 0)
            table.insert(units, unit)
        end

        -- set their priorities
        Utilities.SetPriorities(units, platoonPriorities)

        -- turn it into a platoon
        local platoon = brain:MakePlatoon('', '')
        brain:AssignUnitsToPlatoon(platoon, units, 'Attack', 'AttackFormation')
        table.insert(platoons, platoon)

        -- make it tinker
        local lk = k;
        ForkThread(
            function(...)
                WaitSeconds(holdup[lk])
                PlatoonTinkers.MoveNearestHighOppertunity(unpack(arg))
            end,
            oppertunities,
            platoon
        );
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Construct the experimental

    WaitSeconds(15)

    -- spawn the experimental, adjust its movement speed and the weapon priorities
    local experimental = CreateUnitHPR(self.data.experimental, army, area.center[1], area.center[2], area.center[3], 0, 0, 0)
    experimental:SetSpeedMult(speedMultiplier)
    Utilities.SetPrioritiesPerWeapon({experimental}, experimentalPrioritiesGeneric)

    -- ScenarioFramework.CreateUnitDamagedTrigger(
    --     function(self, instignator)
    --         LOG("A Spiderbot is asking for help!")
    --     end,
    --     experimental,
    --     0.25
    -- )

    ScenarioFramework.CreateUnitDamagedTrigger(
        function(self, instignator)
            experimental:SetSpeedMult(1.0)
            Utilities.SetPrioritiesPerWeapon({experimental}, experimentalPrioritiesPanic)
            LOG("A Spiderbot is in panic!")
        end, 
        experimental, 
        0.85
    );

    function ExperimentalBehaviour(experimental, oppertunities)

        local target = nil;

        while not experimental.Dead do

            local position = experimental:GetPosition();

            -- check for local oppertunities
            local range = 25;
            local value = 800;
            local ops = oppertunities:FindLocalOppertunities(position, range, value);
            local count = table.getn(ops);

            if count > 0 then 
                -- move the experimental away from the targets. Keep track of the fact that we're falling back.
                IssueClearCommands({experimental});
                local locationToEngage = PlatoonTinkers.ComputeEngageLocation(position, ops);
                IssueMove({experimental}, locationToEngage);
                LOG("A Spiderbot is engaging local oppertunities.");
            end

            if experimental:IsIdleState() then 

                ------------------ ------------------ ------------------ ------------------ ------------------   
                -- check if we have anywhere to go

                if not oppertunities:OppertunitiesAvailable() then 
                    if oppertunities == ScenarioInfo.GridObjective then 
                        LOG("A Spiderbot cannot find new oppertunities - switching to player oppertunities.");
                        oppertunities = ScenarioInfo.GridPlayers;
                    else
                        LOG("A Spiderbot cannot find new oppertunities.");
                        break;
                    end
                end

                -- find a target 
                target = oppertunities:GetNearestHighOppertunity(position);

                if target then 
                    -- find a path
                    local options = { smoothen = true, force = 22000 };
                    local path = pathing:FindPath(position, target.position, options)

                    -- move in an aggressive, angry looking fashion
                    IssueClearCommands({experimental});
                    local count = table.getn(path);
                    for k = 1, count - 2, 1 do 
                        IssueMove({experimental}, path[k]);
                    end

                    -- Keep rollin' bae
                    IssueMove({experimental}, target.position)
                    LOG("A Spiderbot is on the move.");
                end
            else
                -- check if we should reconsider
                if target then 
                    local node = oppertunities:GetNode(target.position);
                    if node.oppertunity < 0.5 * target.oppertunity  then
                        -- if the target isn't what it was a few seconds ago, make the platoon stop
                        IssueClearCommands({experimental});
                        LOG("A Spiderbot is reconsidering.");
                    end
                end
            end

            WaitSeconds(4.0);
        end

        LOG("A Spiderbot died.");
    end

    ForkThread(ExperimentalBehaviour, experimental, oppertunities);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Inform the players on the situation.

    -- sound notification
    if eventInfo.MakeDialogue then
        EventFunctionality.PlayRandomDialogue(dialoguesOnSpawn);
    end

    -- objective notification
    if eventInfo.MakeObjective then
        self.ObjectiveColossus = Objectives.Kill(
            'secondary', 
            'incomplete', 
            objectiveTitle,        
            objectiveDescription, 
            {                          
                Units = { experimental },
                MarkUnits = true,
            }
        )
    end

    -- give temporarily vision.
    if eventInfo.MakeVision then
        for k, player in eventInfo.StrPlayers do
            ScenarioFramework.CreateVisibleAreaAtUnit(visionRadius, experimental, visionDuration, GetArmyBrain(player));
        end
    end

    -- ping it
    ScenarioFramework.CreateTimerTrigger(
        function() 
            local army = 1 
            local position = experimental:GetPosition()
            Ping.AttackPing(position, army)
        end,
        10,
        true
    );
end