
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local Rnd = import(ScenarioInfo.path .. 'Algorithms/PseudoRandom.lua');
local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- config is loaded _before_ this file is!
local startTime = ScenarioInfo.Config.GeneralSettings.StartupTime;
local objectiveTime = ScenarioInfo.Config.GeneralSettings.ObjectiveTime;

-- early events
local startStory = 5;
local startEaster = 5;
local startRadar = 70;
local startReclaim = 30;
local startProtectCommander = startTime + 10;
local startProtectArtilleries = 30;

-- late events
local startRogueNuke = startTime + 430 + math.floor(Random() * 30 - 15) + 10 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startReinforcements = startTime + 950 + math.floor(Random() * 60 - 30) + 13 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startSpiderbot = startTime + 860 + math.floor(Random() * 20 - 10) + 17 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startColossus = startTime + 980 + math.floor(Random() * 20 - 10) + 17 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startYthotha = startTime + 1100 + math.floor(Random() * 20 - 10) + 17 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startFatboy = startTime + 1170 + math.floor(Random() * 20 - 10) + 17 * ScenarioInfo.Config.GeneralSettings.Difficulty;
local startRecallWave = startTime + objectiveTime + startProtectArtilleries - (75 + 20 * ScenarioInfo.Config.GeneralSettings.Difficulty)

local debug = false;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- path to the event files.
local path = ScenarioInfo.path .. "Events/";

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

EventAI = { };
EventAI.__index = EventAI;

-- initialises the AI, allocating all memory and pre-computing everything that is applicable.
function EventAI:Initialise(strPlayers)

    eventAI = { };
    setmetatable(eventAI, EventAI);

    -- load in the config.
    doscript(path .. "Config.lua");

    local pRnd = Rnd.PseudoRandom:Initiate({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
    local spawnRegions = Regions.AnalyseRegions();

    eventAI.Players = strPlayers;
    eventAI.Events = { };

    if debug then
        eventAI.Events.BuildTest = import(path .. 'BuildTest.lua').Event:Setup();
    end

    eventAI.Events.ScoutsAllied = import(path .. 'ScoutsAllied.lua').Event:Setup()
    eventAI.Events.Outskirts = import(path .. 'Outskirts.lua').Event:Setup()
    eventAI.Events.Outposts = import(path .. 'Outposts.lua').Event:Setup();
    eventAI.Events.Reclaim = import(path .. 'Reclaim.lua').Event:Setup(spawnRegions, pRnd);
    eventAI.Events.Story = import(path .. 'Story.lua').Event:Setup(spawnRegions, pRnd);
    eventAI.Events.Easter = import(path .. 'Easter.lua').Event:Setup(strPlayers);
    eventAI.Events.ProtectArtilleries = import(path .. 'ProtectArtilleries.lua').Event:Setup(strPlayers);
    eventAI.Events.ProtectCommanders = import(path .. 'ProtectCommanders.lua').Event:Setup(strPlayers);
    eventAI.Events.RogueNuke = import(path .. 'Nuke.lua').Event:Setup(strPlayers);
    eventAI.Events.Recall = import(path .. 'Recall.lua').Event:Setup(spawnRegions);
    eventAI.Events.RecallWave = import(path .. 'RecallWave.lua').Event:Setup(spawnRegions);
    eventAI.Events.Radar = import(path .. 'Radar.lua').Event:Setup(strPlayers);

    pRnd:Shuffle();
    eventAI.Events.Spiderbot = import(path .. 'Spiderbot.lua').Event:Setup(spawnRegions, pRnd);
    pRnd:Shuffle();
    eventAI.Events.Colossus = import(path .. 'Colossus.lua').Event:Setup(spawnRegions, pRnd);
    pRnd:Shuffle();
    eventAI.Events.Reinforcements = import(path .. 'Reinforcements.lua').Event:Setup(spawnRegions, pRnd);
    pRnd:Shuffle();
    eventAI.Events.Fatboy = import(path .. 'Fatboy.lua').Event:Setup(spawnRegions, pRnd);
    pRnd:Shuffle();
    eventAI.Events.Ythotha = import(path .. 'Ythotha.lua').Event:Setup(spawnRegions, pRnd);

    return eventAI;
end

-- start the AI.
function EventAI:Start()

    eventInfo = { };
    eventInfo.StrPlayers = self.Players;
    eventInfo.OtherEvents = eventAI.Events;
    eventInfo.MakeObjective = true;
    eventInfo.MakeDialogue = true;
    eventInfo.MakeVision = true;



    ScenarioFramework.CreateTimerTrigger(
        function()

            if debug then
                eventAI.Events.Recall:Launch(eventInfo);
            end

        end,
        10,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Reclaim:Launch(eventInfo);
        end,
        startReclaim,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Story:Launch(eventInfo);
        end,
        startStory,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.ScoutsAllied:Launch(eventInfo);
            eventAI.Events.Outskirts:Launch(eventInfo);
            eventAI.Events.Easter:Launch(eventInfo);
        end,
        startEaster,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Radar:Launch(eventInfo);
        end,
        startRadar,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.ProtectCommanders:Launch(eventInfo);
        end,
        startProtectCommander,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.ProtectArtilleries:Launch(eventInfo);
        end,
        startProtectArtilleries,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.RogueNuke:Launch(eventInfo);
        end,
        startRogueNuke,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Reinforcements:Launch(eventInfo);
        end,
        startReinforcements,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Spiderbot:Launch(eventInfo);
        end,
        startSpiderbot,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Colossus:Launch(eventInfo);
        end,
        startColossus,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Ythotha:Launch(eventInfo);
        end,
        startYthotha,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function()
            eventAI.Events.Fatboy:Launch(eventInfo);
        end,
        startFatboy,
        true
    );

    ScenarioFramework.CreateTimerTrigger(
        function() 
            eventAI.Events.RecallWave:Launch(eventInfo);
        end,
        startRecallWave,
        true
    );

end