
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local Game = import(ScenarioInfo.path .. 'Functionality/EndGame.lua')

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local titleProtectObjective = 'Protect the commanders';
local descriptionProtectObjectiveAll = 'All commanders must survive - we leave no man behind.';
local descriptionProtectObjectiveOne = 'Any commander must survive - the mission is more important.';

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- sounds when one commander dies while all should live.
local X06_DB01_030 = { {text = '<LOC X06_DB01_030_010>[{i HQ}]: Commander? You there? Dammit.', vid = 'X06_HQ_DB01_04963.sfd', bank = 'Briefings', cue = 'X06_HQ_DB01_04963', faction = 'NONE'},}
local X06_M01_140 = { {text = '<LOC X06_M01_140_010>[{i HQ}]: Commander, you read me? Commander? Ah hell...', vid = 'X06_HQ_M01_04961.sfd', bank = 'X06_VO', cue = 'X06_HQ_M01_04961', faction = 'NONE'},}
local X05_DB01_040 = { {text = '<LOC X05_DB01_040_010>[{i HQ}]: Commander? Can you read me? Ah hell, I think the Commander is dea --', vid = 'X05_HQ_DB01_04957.sfd', bank = 'Briefings', cue = 'X05_HQ_DB01_04957', faction = 'NONE'},}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    return event;
end

function Event:Launch(eventInfo)

    -- option flag
    local onlyOne = ScenarioInfo.Config.GeneralSettings.Commanders == 'one';

    -- find all the commanders.
    local commanders = { };
    for k, player in eventInfo.StrPlayers do
        local brain = GetArmyBrain(player);
        local commander = brain:GetListOfUnits(categories.COMMAND, false);
        table.insert(commanders, commander[1]);
    end

    -- General objective information
    local count = table.getn(commanders)
    local description = descriptionProtectObjectiveAll
    if onlyOne then 
        count = 1
        description = descriptionProtectObjectiveOne
    end

    local function aliveCheck (self)

        local function DoCinematics(unit)
            ScenarioFramework.CDRDeathNISCamera(self, 3.0)
        end

        -- we should've all survived - play immediately
        if not onlyOne then 
            DoCinematics(self)
        -- only one of us should survive - play if we're the last one
        else
            local count = 0
            for k, commander in commanders do 
                if not commander.Dead then 
                    count = count + 1 
                end
            end

            if not count > 0 then 
                DoCinematics(self)
            end
        end
    end

    -- keep checking if they are alive
    for k, commander in commanders do 
        ScenarioFramework.CreateUnitDeathTrigger(aliveCheck, commander);
    end

    -- setup the protect objective.
    self.ObjectiveCommander = Objectives.Protect(
        'primary',
        'incomplete',
        titleProtectObjective,
        description,
        {
            Units = commanders,
            NumRequired = count;
        }
    )

    -- determine the consequences.
    self.ObjectiveCommander:AddResultCallback(
        function(result)
            if result then
                -- succes!
            else
                -- play a random 'oh no, commander died' video.
                local dialogues = { X06_DB01_030, X06_M01_140, X05_DB01_040}
                EventFunctionality.PlayRandomDialogue(dialogues);
    
                -- we're done for :(.)
                ScenarioFramework.CreateTimerTrigger(
                    function()
                        for k, player in eventInfo.StrPlayers do
                            GetArmyBrain(player):OnDefeat();
                        end

                        Game.End(false);
                    end,
                    6,
                    true
                );
                
            end
        end
    );
end