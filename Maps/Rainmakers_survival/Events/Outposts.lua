
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local distance = 80;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Objective;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    -- determine what outposts we should spawn, then spawn them
    local n = ScenarioInfo.Config.GeneralSettings.InitialStaticDefenses;
    for k = 1, n, 1 do 
        local units = ScenarioUtils.CreateArmyGroup(ScenarioInfo.Armies.Objective, 'Outpost' .. k, false);

        -- make all the turrets stronger
        for k, unit in units do
            if EntityCategoryContains(categories.DIRECTFIRE, unit) then
                unit:SetVeterancy(n - 1);
            end
        end
    end

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    -- nothing to do

end