
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- spawns a bunch of engineers, along with a couple of gaurding platoons.
-- on higher difficulty, multiple build sites will spawn (over time).
-- builds a spiderbot. Engineers are marked.

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Platoon;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    -- retrieve all event data, such as the experimental and the platoons.
    event.data = self:Analyse("ESP");

    return event;
end

-- starts the event.
function Event:Start(strPlayers)

end

function Event:Analyse(name)

    local eventData = { };

    -- first, find all platoons that accompany this experimental.
    eventData.platoons = { };
    for k = 1, 10 do
        -- check whether the current platoon we're trying to find exists.
        local identifier = name .. "_Platoon" .. k;
        local exists = ScenarioUtils.FindUnitGroup(identifier, Scenario.Armies[army].Units);

        -- if not, stop. We've found all our gaurd platoons.
        if not exists then
            break;
        end

        -- spawn the units of the group.
        local units = ScenarioUtils.CreateArmyGroupAsPlatoon(army, identifier, 'AttackFormation'):GetPlatoonUnits();

        -- track the blueprint data, then throw everything out again.
        local sTable = { };
        for k, unit in units do
            table.insert(sTable, unit:GetBlueprint().BlueprintId);
            unit:Destroy();
        end

        eventData.platoons[k] = sTable;
    end

    -- then find the experimental.
    local identifier = name .. "_Experimental"; 
    local exists = ScenarioUtils.FindUnitGroup(identifier, Scenario.Armies[army].Units);

    -- if it does not exist, then we messed up!
    if not exists then
        error("given experimental eventData has no experimental: " .. name .. ' (' .. identifier .. ')');
    end

    local unit = ScenarioUtils.CreateArmyGroupAsPlatoon(army, identifier, 'AttackFormation'):GetPlatoonUnits()[1];
    eventData.experimental = unit:GetBlueprint().BlueprintId;

    return eventData;
end