
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- a shell barrage from an external source.
-- Shoots for 2 - 3 minutes on the biggest global targets.
-- area under fire is being indicated with a red circle, which moves slowly.

-- spawns a spider bot along with a couple gaurding platoons.
-- choose path to weakest point in defense.

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

local stringNukeCaptureTitle = "On-site UEF Forward Operation Base";
local stringNukeCaptureDescription = "A nearby secret UEF forward operation base has been discovered. One of its major assets is being captured. Prevent the capture or destroy the asset before they take control of it."

local stringNukeLaunchTitle = "On-site UEF Forward Operation Base";
local stringNukeLaunchDescription = "The asset has been captured. Destroy it before the enemies can utilise it again."

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local UEFOperativeArmy = ScenarioInfo.Armies.UEFOperative;
local OperativeHostileTakeOverArmy = ScenarioInfo.Armies.Platoon;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };    
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    -- setup diplomacy.
    for i, Army in ListArmies() do
        SetAlliance(Army, UEFOperativeArmy, "Neutral");
    end

    -- if set, the platoons that are still alive will become regular platoons.
    event.GoHostile = false;

    -- if set, then the launcher has been captured.
    event.Captured = false;

    -- determine through difficulty what we should and should not spawn.
    local index = ScenarioInfo.Config.GeneralSettings.Difficulty;
    local platoonNumber = {2, 3, 4, 6};

    -- create the objective.
    event.Objective = ScenarioUtils.CreateArmyGroup(UEFOperativeArmy, "EN_Objective")[1];

    -- create the platoons. Number of platoons depend on difficulty.
    event.Platoons = { };
    for k = 1, platoonNumber[index] do
        local identifier = "EN_Platoon" .. k;
        table.insert(event.Platoons, ScenarioUtils.CreateArmyGroupAsPlatoon(OperativeHostileTakeOverArmy, identifier, 'AttackFormation'));
    end    
    
    function AliveTick(platoons)

        while table.getn(platoons) > 0 and not event.GoHostile do

            -- keep track of all the platoons that are alive.
            local currentPlatoons = { };
            for k, platoon in platoons do
                if GroundFunctionality.PlatoonAliveCheck(platoon) then
                    table.insert(currentPlatoons, platoon);
                end
            end

            platoons = currentPlatoons;

            -- find targets in the area to engage.
            local targets = GroundFunctionality.FindLocalTargets(event.Objective:GetPosition(), 1, 6, 1);
            
            -- depending on how many platoons we have and the situation, determine what to do.
            if table.getn(targets) > 0 then

                for k, platoon in platoons do

                    platoon.WanderCount = 5;

                    -- make this platoon go to the objective.
                    if k == 1 then
                        platoon:Stop();
                        platoon:AggressiveMoveToLocation(event.Objective:GetPosition());
                    end

                    -- make these platoons engage the threats.
                    if k > 1 then
                        local position = GroundFunctionality.LocalTargetsEngageVector(platoon:GetPlatoonPosition(), targets);
                        GroundFunctionality.PlatoonDiveTarget(platoon, position);
                    end
                end
            else
                for k, platoon in platoons do
                    -- we don't want to pick a new wander target every iteration - this causes them to never stop and be where they are.
                    if not platoon.WanderCount then
                        platoon.WanderCount = 5;
                    end

                    if platoon.WanderCount > 2 then
                        -- find a random position around the target.
                        local location = GeneralFunctionality.VectorAddition(
                            event.Objective:GetPosition(),
                            GeneralFunctionality.VectorScalarMultiply(
                                GeneralFunctionality.VectorNormalize(
                                    GeneralFunctionality.Vector(Random() - 0.5, 0.1, Random() - 0.5)
                                ),
                                60
                            )
                        );
                        
                        -- move towards this target.
                        platoon:Stop();
                        platoon:MoveToLocation(location, false);
                        platoon.WanderCount = 0;
                    else
                        platoon.WanderCount = platoon.WanderCount + 1;
                    end
                end
            end

            WaitSeconds(17.0);
        end

        -- either all the platoons are dead, or the event stopped somehow. Either way, turn all remaining platoons to the groundplatoon AI.
        for k, platoon in platoons do
            -- make the platoon part of the regular platoons.
            ScenarioInfo.Globals.PlatoonManager:AddPlatoon(platoon);
        end
    end

    -- if the nuke is destroyed, then the platoons can go hostile.
    function NukeDestroyed(self)
        event.GoHostile = true
    end

    -- add the callback when the launcher is destroyed.
    ScenarioFramework.CreateUnitDeathTrigger(NukeDestroyed, event.Objective);

    -- create the wreckages.
    ScenarioUtils.CreateArmyGroup(UEFOperativeArmy, "WRECKAGES", true);

    -- turn on the platoon wandering!
    ForkThread(AliveTick, event.Platoons);

    return event;
end

-- starts the event.
function Event:Launch(eventInfo)

    -- function-wide reference to the Event table.
    local event = self;

    --------------------------------------------------------------------------------------
    -- MOVE ENGINEERS NEAR TO CAPTURE --
    --------------------------------------------------------------------------------------
    
    -- create the engineers.
    event.Engineers = ScenarioUtils.CreateArmyGroupAsPlatoon(OperativeHostileTakeOverArmy, "EN_CaptureEngineers", 'AttackFormation'):GetPlatoonUnits();
    for k, engineer in event.Engineers do

        -- make them a wee bit stronger.
        engineer:SetMaxHealth(1500);
        engineer:SetHealth(engineer, 1500);

        -- issue them to move somewhere randomly around the target, this is to try to make sure they do not capture next to one another.
        local location = GeneralFunctionality.VectorAddition(
            event.Objective:GetPosition(),
            GeneralFunctionality.VectorScalarMultiply(
                GeneralFunctionality.VectorNormalize(
                    GeneralFunctionality.Vector(Random() - 0.5, 0.1, Random() - 0.5)
                ),
                5
            )
        );

        IssueMove({engineer}, location);
    end

    -- cappttuuree!! raaarr!
    IssueCapture(event.Engineers, event.Objective);

    -- show where it is happening
    for k, player in eventInfo.StrPlayers do
        ScenarioFramework.CreateVisibleAreaAtUnit(15, event.Objective, 15, GetArmyBrain(player));
    end

    --------------------------------------------------------------------------------------
    -- CREATE OBJECTIVE TO SOLVE SITUATION --
    --------------------------------------------------------------------------------------

    -- create the actual objective.
    local objectiveUnits = { event.Objective };
    for k, engineer in event.Engineers do
        table.insert(objectiveUnits, engineer);
    end

    event.ObjectivePreventCapture = Objectives.Basic(
        'secondary',                        # type
        'incomplete',                       # status
        stringNukeCaptureTitle,             # title
        stringNukeCaptureDescription,       # description
        Objectives.GetActionIcon('kill'),
        {
            Units = objectiveUnits,
        }
    )

    -- create the triggers to complete it.
    function AssetDestroyed(unit)
        if not event.Captured then
            event.ObjectivePreventCapture:ManualResult(true);
        end
    end

    function EngineersToCaptureDestroyed(group)
        if not event.Captured then
            event.ObjectivePreventCapture:ManualResult(true);
        end
    end

    function AssetCaptured(unit)
        event.ObjectivePreventCapture:ManualResult(false);
    end

    -- add the triggers.
    ScenarioFramework.CreateUnitDeathTrigger(AssetDestroyed, event.Objective);
    ScenarioFramework.CreateUnitReclaimedTrigger(AssetDestroyed, event.Objective);
    ScenarioFramework.CreateGroupDeathTrigger(EngineersToCaptureDestroyed, event.Engineers)
    ScenarioFramework.CreateUnitCapturedTrigger(nil, AssetCaptured, event.Objective)

    --------------------------------------------------------------------------------------
    -- WHAT HAPPENS WHEN CAPTURED --
    --------------------------------------------------------------------------------------

    -- the callback when the capture was succesful. Time to start launching nukes!
    function NukeCaptured(unit)

        -- set the new objective!
        event.Objective = unit;
        event.Captured = true;

        -- add the new callback.
        ScenarioFramework.CreateUnitDeathTrigger(NukeDestroyed, event.Objective);

        -- move back the engineers, their job is done.
        IssueMove(event.Engineers, {255, 0, 0});

        -- set the new objective!
        function WaitThread()

            -- wait a bit before we actually launch.
            WaitSeconds(4.0 + Random() * 5);

            event.ObjectiveNextLaunch = Objectives.Kill(
                'secondary',                        # type
                'incomplete',                       # status
                stringNukeCaptureTitle,             # title
                stringNukeCaptureDescription,       # description
                {
                    Units = { event.Objective },
                }
            )

            -- the objective until the next nuke is to be expected along with the timer.
            local waitTime = 170 + Random() * 20;

            while not event.Objective.Dead do

                -- give it a nuke.
                event.Objective:GiveNukeSiloAmmo(1);

                -- find a target.
                local index = math.floor(Random() * math.max(table.getn(ScenarioInfo.Globals.GridManager.globalTargets), 2)) + 1;
                local target = ScenarioInfo.Globals.GridManager.globalTargets[index].coords;

                -- launch it.
                IssueNuke({event.Objective}, target);

                WaitSeconds(waitTime);
            end
        end

        ForkThread(WaitThread);
    end

    -- if the engineers are destroyed while the objective is still alive, then the platoons can go hostile.
    function EngineersDestroyed(group)
        if not event.Captured then
            event.GoHostile = true
        end
    end

    function AssetDestroyed(unit)
        -- means it is destroyed - succes!
        if event.ObjectiveNextLaunch then
            event.ObjectiveNextLaunch:ManualResult(true);
            event.GoHostile = true
        end
    end

    -- add the triggers.
    ScenarioFramework.CreateUnitDeathTrigger(AssetDestroyed, event.Objective);
    ScenarioFramework.CreateUnitReclaimedTrigger(AssetDestroyed, event.Objective);

    -- when the capture succeeeds.
    ScenarioFramework.CreateUnitCapturedTrigger(nil, NukeCaptured, event.Objective)

    -- when all engineers are destroyed.
    ScenarioFramework.CreateGroupDeathTrigger(EngineersDestroyed, event.Engineers)
end