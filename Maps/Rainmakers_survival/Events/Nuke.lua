
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;

local PlatoonTinkers = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua')
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

-- the amount of vision and the duration of that vision
-- that the players gain when the objective is activated.
local visionRadius = 80;
local visionDuration = 40;

local timeTillObjective = 300;

local stringNukeCaptureTitle = "On-site UEF Forward Operation Base";
local stringNukeCaptureDescription = "A nearby secret UEF forward operation base has been discovered. One of its major assets is being captured. Prevent the capture or destroy the asset before they take control of it."

local stringNukeLaunchTitle = "On-site UEF Forward Operation Base";
local stringNukeLaunchDescription = "The asset has been captured. Destroy it before the enemies can utilise it again."

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Objective;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    local n = 2 + ScenarioInfo.Config.GeneralSettings.Difficulty;

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the wreckage, event.Damaged event.BuildingData / units and the objectives

    event.Wreckages = ScenarioUtils.CreateArmyGroup("Neutrals", "EventNukeWreckages", false);
    event.Damaged = ScenarioUtils.CreateArmyGroup("Neutrals", "EventNukeDamaged", false);
    event.Launcher = ScenarioUtils.CreateArmyGroup("HOSTILEPLATOONS", "EventNukeLauncher", false)[1];

    for k, unit in event.Wreckages do 
        unit:CreateWreckageProp(0);
        unit:Destroy();
    end

    for k, unit in event.Damaged do 
        local health = unit:GetMaxHealth();
        local rnd = 0.25 + Random() * 0.5;
        unit:SetHealth(unit, health * rnd);
    end

    event.Launcher:SetHealth(event.Launcher, 1244);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- construct the engineer table

    event.Engineers = { };
    for k = 1, n do 
        local name = "EventNukeEngineers" .. k
        local engies = ScenarioUtils.CreateArmyGroup("HOSTILEPLATOONS", name, false);
        table.insert(event.Engineers, engies);
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- prepare the building locations and blueprints

    event.BuildingData = { };
    for k = 1, n - 1     do 
        local bps = { }
        local name = "EventNukeBuildings" .. k;
        local units = ScenarioUtils.CreateArmyGroup("HOSTILEPLATOONS", name, false);
        for k, unit in units do
            table.insert(bps, { bp = unit:GetBlueprint().BlueprintId, loc = unit:GetPosition() });
            unit:Destroy();
        end

        table.insert (event.BuildingData, bps);
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialize the engineer that 'captures' the nuke

    function ForkEngineerNuke(engineer, launcher)

        WaitSeconds(1.0);

        while not engineer.Dead and not launcher.Dead do

            -- issue them to move somewhere randomly around the target, this is to try to make sure they do not capture next to one another.
            local location = GeneralFunctionality.VectorAddition(
                launcher:GetPosition(),
                GeneralFunctionality.VectorScalarMultiply(
                    GeneralFunctionality.VectorNormalize(
                        GeneralFunctionality.Vector(Random() - 0.5, 0.1, Random() - 0.5)
                    ),
                    5
                )
            );

            IssueClearCommands({engineer});

            -- mooovviiinngg, sir!
            IssueMove({engineer}, location);

            -- repaaiirrr!! raaarr!
            IssueRepair({engineer}, launcher);

            WaitSeconds(40);
        end
    end

    -- make it a wee bit stronger
    event.CaptureEngineer = event.Engineers[1][1];
    event.CaptureEngineer:SetMaxHealth(1640);
    event.CaptureEngineer:SetHealth(event.CaptureEngineer, 1640);

    ForkThread(ForkEngineerNuke, event.CaptureEngineer, event.Launcher);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- initialize the event.Engineers that build the event.BuildingData

    function ForkCheckIdle(engineer)
        WaitSeconds(1.0);
        while not engineer.Dead do
            if engineer:IsIdleState() then
                engineer:Kill();
            end
            WaitSeconds(1.0);
        end
    end

    for k = 2, n do 
        -- retrieve the tables
        local engineerTable = event.Engineers[k];
        local buildingTable = event.BuildingData[k - 1];

        -- issue the build commands
        -- todo: move towards the event.BuildingData first?
        for k, building in buildingTable do
            IssueBuildMobile(engineerTable, building.loc, building.bp, { });
        end

        -- if engineer becomes idle, kill it
        for ke, engineer in engineerTable do 
            ForkThread(ForkCheckIdle, engineer);
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the platoons that help guard the launcher

    event.Platoons = { }
    for k = 1, n do 
        local name = "EventNukePlatoon" .. k 
        local platoon = ScenarioUtils.CreateArmyGroupAsPlatoon("HOSTILEPLATOONS", name, 'AttackFormation');
        table.insert(event.Platoons, platoon);

        ForkThread(
            PlatoonTinkers.GuardLocation,   -- the AI function
            platoon,                        -- the platoon
            event.Launcher:GetPosition(),   -- the ward
            function()                      -- the function that determines if the platoon should
                                            -- keep on guarding
                if event.ObjectiveNeutralize then
                    if not event.ObjectiveNeutralize.Active then 
                        return false;
                    end
                end

                return true;
            end,
            (k + 4) * 10                   -- the guard distance (for wandering behavior)        
        );

    end

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an event.Launcher is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    local oppertunities = ScenarioInfo.GridPlayers;
    local objectiveNeutralize = { self.Launcher, self.CaptureEngineer }
    local captured = false;

    -- give temporarily vision
    if eventInfo.MakeVision then
        for k, player in eventInfo.StrPlayers do
            ScenarioFramework.CreateVisibleAreaAtUnit(visionRadius, self.Launcher, visionDuration, GetArmyBrain(player));
        end
    end

    -- ping it
    ScenarioFramework.CreateTimerTrigger(
        function() 
            local army = 1 
            local position = self.Launcher:GetPosition()
            Ping.AttackPing(position, army)
        end,
        5,
        true
    );


    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Start the objectives

    self.ObjectiveNeutralize = Objectives.Basic(
        'secondary',                        # type
        'incomplete',                       # status
        "UEF Operative Base",               # title
        "A nearby UEF operative base is being captured. One of its assets is a Strategic Missile Launcher (SML). The asset has two armed warheads. Neutralize the engineer or the asset before it is captured.",       # description
        Objectives.GetActionIcon('kill'),
        {
            Units = objectiveNeutralize,
            MarkUnits = true,
            FlashVisible = true,
        }
    );

    self.ObjectiveNeutralizeTimer = Objectives.Timer(
        'secondary',
        'incomplete',
        "UEF Operative Base",
        "The remaining time before the Strategic Missile Launcher (SML) with two armed warheads is captured.",
        {
            Timer = 623,
            ExpireResult = 'failed',
        }
    );  

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Add the callbacks / add the logic behind the objectives (or units)

    function EngineerNeutralized()
        if not captured then 
            self.ObjectiveNeutralize:ManualResult(true);
            self.ObjectiveNeutralizeTimer:ManualResult(true);
        end
    end

    -- engineer: on death
    ScenarioFramework.CreateUnitDeathTrigger(
        EngineerNeutralized, self.CaptureEngineer
    );

    function LauncherNeutralized()
        self.ObjectiveNeutralize:ManualResult(true);

        if not captured then
            self.ObjectiveNeutralizeTimer:ManualResult(true);
        end

        if not self.CaptureEngineer.Dead then
            self.CaptureEngineer:Kill();
        end
    end

    -- launcher: on death
    ScenarioFramework.CreateUnitDeathTrigger(
        LauncherNeutralized, self.Launcher
    );

    -- a warhead will be launched. If called. Sad life. Don't call *:(.
    function LaunchWarhead(launcher)
        if not launcher.Dead then
            launcher:GiveNukeSiloAmmo(1);
            local target = oppertunities:GetHighOppertunity().position;
            IssueNuke({launcher}, target);
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- timer before it launches

    self.ObjectiveNeutralizeTimer:AddResultCallback(
        function (result)
            if result then
                -- hooray, the stuffs was prevented in time
                return;
            end

            -- hooray x2, we can blow things up now
            captured = true;

            -- get rid of the engineer, his work is done
            self.CaptureEngineer:Kill();

            -- update the objective
            Objectives.UpdateBasicObjective(self.ObjectiveNeutralize, 'description', 
                "A nearby UEF operative base is being captured. One of its assets is a Strategic Missile Launcher (SML). The asset contains two armed warheads. Neutralize the asset."
            );

            ScenarioFramework.CreateTimerTrigger(
                function() 
    
                    -- launch a warhead: we're about to get f'ed up
                    LaunchWarhead(self.Launcher);
    
                    WaitSeconds(7.0);
    
                    local objectiveSecondNuke = Objectives.Timer(
                        'secondary',
                        'incomplete',
                        "UEF Operative Base",
                        "Neutralize the asset before the second warhead is ready to launch.",
                        {
                            Timer = 123,
                            ExpireResult = 'failed',
                        }
                    );
    
                    objectiveSecondNuke:AddResultCallback(
                        function (result)
                            if result then
                                -- hooray, we don't get bombed twice
                                return;
                            end
                                -- yeehh!! Launch a warhead and we failed to neutralize the asset
                                LaunchWarhead(self.Launcher);
                                self.ObjectiveNeutralize:ManualResult(false);
                        end
                    );

                    
                    -- set a timer when the next warhead launches
                    self.ObjectiveSecondNuke = objectiveSecondNuke;
                end,
                7,
                true
            );
        end
    );
end