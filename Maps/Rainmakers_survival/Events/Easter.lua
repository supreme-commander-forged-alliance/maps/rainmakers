
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua');
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local Objectives = import('/lua/ScenarioFramework.lua').Objectives;

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

local explosion = import('/lua/defaultexplosions.lua')

local army = ScenarioInfo.Armies.Easter;

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup()

    -- linking
    event = { };
    setmetatable(event, Event);

    local units = ScenarioUtils.CreateArmyGroupAsPlatoon(army, 'Egg', 'AttackFormation'):GetPlatoonUnits();
    event.Jip = EntityCategoryFilterDown(categories.UEF, units)[1];
    event.Kyle = EntityCategoryFilterDown(categories.AEON, units)[1];
    event.Fenix = EntityCategoryFilterDown(categories.SERAPHIM, units)[1];

    event.Jip:SetCustomName("Jip");
    event.Kyle:SetCustomName("Kyle");
    event.Fenix:SetCustomName("Alex");

    -- our little damaged mass extractor
    event.Target = EntityCategoryFilterDown(categories.TECH3, units)[1];
    event.Target:SetProductionPerSecondMass(1);
    event.Target:SetHealth(event.Jip, (Random() * 0.1 + 0.1) * event.Target:GetMaxHealth());
    event.Target:SetRegenRate(-4);

    -- make sure they do not fall prey to silly people.
    for k, unit in units do
        unit:SetReclaimable(false);
        unit:SetCapturable(false);
        unit:SetDoNotTarget(true);
        unit:SetUnSelectable(true);
        unit:SetCanBeKilled(false);
    end

    -- the engineer can be killed
    event.Jip:SetReclaimable(true);
    event.Jip:SetCanBeKilled(true);

    -- and when it is, all hell breaks lose!
    ScenarioFramework.CreateUnitDestroyedTrigger(
        function(self)

            for k, unit in units do 
                if not unit.Dead then 
                    explosion.CreateDefaultHitExplosion(unit, k)
                    unit:Destroy()
                end
            end 

            effects = {
                '/effects/emitters/seraphim_othuy_spawn_01_emit.bp',
                '/effects/emitters/seraphim_othuy_spawn_02_emit.bp',
                '/effects/emitters/seraphim_othuy_spawn_03_emit.bp',
                '/effects/emitters/seraphim_othuy_spawn_04_emit.bp',
            }
        
            local position = self:GetPosition()
            
            -- Spawn the Energy Being
            local army = "HOSTILEPLATOONS"
            local brain = GetArmyBrain(army)
            local index = brain:GetArmyIndex()
            local spiritUnit = CreateUnitHPR('XSL0402', index, position[1], position[2], position[3], 0, 0, 0)

            explosion.CreateDefaultHitExplosion(spiritUnit, 1.0)

            -- Create effects for spawning of energy being
            for k, v in effects do
                CreateAttachedEmitter(spiritUnit, -1, index, v)

            end
        end,
        event.Jip
    )

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    -- create vision on the little scene.
    local marker = ScenarioUtils.GetMarker("Easter");
    for k, player in eventInfo.StrPlayers do
        ScenarioFramework.CreateVisibleAreaLocation(16, marker.position, 3000, GetArmyBrain(player));
    end

    -- let the engineer 'repair' the mass extractor
    IssueRepair({self.Jip}, self.Target);

    -- forkthread the units to wander a bit.
    function Wander(unit, origin, radius, waitTime)

        while true do

            local vector = GeneralFunctionality.VectorScalarMultiply(GeneralFunctionality.VectorRandom(), radius);
            local target = GeneralFunctionality.VectorAddition(origin, vector);
            IssueMove({unit}, target);

            WaitSeconds(waitTime);

        end
    end

    -- ForkThread(Build, self.Jip);
    ForkThread(Wander, self.Kyle, marker.position, 8, 8);
    ForkThread(Wander, self.Fenix, marker.position, 10, 11);

end

--aiBrain:BuildStructure(builder, structureCategory, location, false)