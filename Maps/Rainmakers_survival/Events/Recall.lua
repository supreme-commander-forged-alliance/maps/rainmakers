
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local Game = import(ScenarioInfo.path .. 'Functionality/EndGame.lua')

local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local TinkerUnitFramework = import(ScenarioInfo.path .. 'Tinkers/Unit.lua');
local TinkerPlatoonFramework = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua');

local GenUnitFramework = import(ScenarioInfo.path .. 'Generators/Unit.lua');
local GenPlatoonFramework = import(ScenarioInfo.path .. 'Generators/Platoon.lua');
local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

local Rnd = import(ScenarioInfo.path .. 'Algorithms/PseudoRandom.lua');
local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local titleRecallObjective = 'Recall zone';
local descriptionRecallObjetive = 'Move the commanders into the recall zone for immediate recall.';

local titleTimerObjective = 'Recall time';
local descriptionTimerObjective = 'The enemy is jamming our recall. HQ needs time to circumvent the jamming. Survive until recall is available.';

local startRecall = 111 + math.floor(Random() * 30 - 15) + ScenarioInfo.Config.GeneralSettings.Difficulty * 11;
local holdoutReminder = 94;
local recallReminder = 93;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local recallAreaString = "EVENTRECALL" .. (math.floor(0.98 * Random() * 4) + 1);
local recallArea = Scenario.Areas[recallAreaString].rectangle
local recallRect = Rect(recallArea[1],recallArea[2],recallArea[3],recallArea[4]);
local recallCenter = VECTOR3((recallRect.x0 + recallRect.x1) * 0.5, 0, (recallRect.y0 + recallRect.y1) * 0.5)
recallCenter[2] = GetSurfaceHeight(recallCenter[1], recallCenter[3])

X06_M03_240 = { {text = '<LOC X06_M03_240_010>[{i HQ}]: Scratch one alien. HQ out.', vid = 'X06_HQ_M03_04002.sfd', bank = 'X06_VO', cue = 'X06_HQ_M03_04002', faction = 'NONE'},}
X04_M03_020 = { {text = '<LOC X04_M03_020_010>[{i HQ}]: Commander, you still have enemy forces moving toward your position. Hold the line until we can extract you. HQ out.', vid = 'X04_HQ_M03_03757.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03757', faction = 'NONE'},}
X04_M03_070 = { {text = '<LOC X04_M03_070_010>[{i HQ}]: Going to be a bit longer, Commander. Hang in there.', vid = 'X04_HQ_M03_03765.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03765', faction = 'NONE'},}
X03_M03_120 = { {text = '<LOC X03_M03_120_010>[{i HQ}]: They ain\'t going to forget that butt-whipping. Good job, Commander. HQ out.', vid = 'X03_HQ_M03_03345.sfd', bank = 'X03_VO', cue = 'X03_HQ_M03_03345', faction = 'NONE'},}
X04_M03_120 = { {text = '<LOC X04_M03_120_010>[{i HQ}]: Grab your barf bag, Commander, this is going to be rough. Recalling now!', vid = 'X04_HQ_M03_03770.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_03770', faction = 'NONE'},}
X04_M03_210 = { {text = '<LOC X04_M03_210_010>[{i HQ}]: Damn, you certainly took care of business. We\'re recalling you. HQ out.', vid = 'X04_HQ_M03_04920.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04920', faction = 'NONE'},}
X04_M03_220 = { {text = '<LOC X04_M03_220_010>[{i HQ}]: Commander, stay within the area of your base. If you stray too far, we won\'t be able to get a recall lock. HQ out.', vid = 'X04_HQ_M03_04935.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04935', faction = 'NONE'},}
X04_M03_230 = { {text = '<LOC X04_M03_230_010>[{i HQ}]: Get back to your base, Commander. You\'re too far out and we can\'t get a lock on you. HQ out.', vid = 'X04_HQ_M03_04936.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04936', faction = 'NONE'},}
X04_M03_240 = { {text = '<LOC X04_M03_240_010>[{i HQ}]: Stay within your base\'s perimeter, Commander. We need to lock down your position so we can get you the hell out of there. HQ out.', vid = 'X04_HQ_M03_04937.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04937', faction = 'NONE'},}
X04_M03_250 = { {text = '<LOC X04_M03_250_010>[{i HQ}]: We\'ve got a lock on your ACU. Almost there.', vid = 'X04_HQ_M03_04938.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_04938', faction = 'NONE'},}
X04_M03_270 = { {text = '<LOC X04_M03_270_010>[{i HQ}]: That was damn close, Commander. Glad we got you out of there in one piece.', vid = 'X04_HQ_M03_05107.sfd', bank = 'X04_VO', cue = 'X04_HQ_M03_05107', faction = 'NONE'},}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(regions)

    LOG(repr(recallCenter))

    -- linking
    event = { };
    setmetatable(event, Event);

    -- initialize a bunch of generators, we'll spawn units depending on the difficulty
    event.generators = {
        GenPlatoonFramework.Generator:Setup(regions, "Tech1-2:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech2-2:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech2-3:"),
        GenPlatoonFramework.Generator:Setup(regions, "Tech3-3:"),
    }

    event.regions = regions
 
    return event;
end

function Event:Launch(eventInfo)

    -- find the remaining commanders
    local commanders = { };
    for k, player in eventInfo.StrPlayers do
        local brain = GetArmyBrain(player);
        local commander = brain:GetListOfUnits(categories.COMMAND, false);
        table.insert(commanders, commander[1]);
    end

    -- add in some dialogues that you're going to have to wait. HQ needs some coffee.
    ScenarioFramework.Dialogue(X04_M03_020, nil, false);
    ScenarioFramework.CreateTimerTrigger(
        function() 
            ScenarioFramework.Dialogue(X04_M03_070, nil, false); 
        end,
        holdoutReminder,
        true
    );

    -- a timer to make this clear: HQ really needs coffee.
    self.ObjectiveTimer = Objectives.Timer(
        'primary',
        'incomplete',
        titleTimerObjective,
        descriptionTimerObjective,
        {
            Timer = startRecall,
            ExpireResult = 'complete',
        }
    );

    -- when the timer finishes, we can start the recall process.
    self.ObjectiveTimer:AddResultCallback(
        function (result)
            if result then
                PhaseRecall();
            end
        end
    );

    function PhaseRecall()

        -- a litl' bit of cinematics. Add in the reminder too.
        ScenarioFramework.Dialogue(X04_M03_230, nil, false);
        ScenarioFramework.CreateTimerTrigger(
            function() 
                if self.ObjectiveRecall.Active then
                    ScenarioFramework.Dialogue(X04_M03_240, nil, false); 

                    -- add another ping!
                    WaitSeconds(15.0)
                    Ping.MovePing(recallCenter, 1)
                end
            end,
            recallReminder,
            true
        );

        -- add a ping at the center
        ForkThread(
            function()
                WaitSeconds(15.0)
                Ping.MovePing(recallCenter, 1)
            end
        )

        -- construct the area objective
        self.ObjectiveRecall = Objectives.SpecificUnitsInArea(
            'primary',
            'incomplete',
            titleRecallObjective, 
            descriptionRecallObjetive,
            {
                Units = commanders,
                Area = recallAreaString,
                MarkArea = true,
            }
        )

        -- check if we passed the objective
        ForkThread(
            function()

                local onlyOne = ScenarioInfo.Config.GeneralSettings.Commanders == 'one'

                -- if only one commander has to survive then it can happen that another commander dies
                -- during the end sequence - this ensures that it still ends
                if onlyOne then 
                    while self.ObjectiveRecall.Active do 
                        WaitSeconds(1.0)
                        local allDead = true
                        local allInside = true
                        for k, commander in commanders do 
                            if not commander.Dead then 
                                allDead = false
                                local position = commander:GetPosition()
                                allInside = ScenarioUtils.InRect(position, recallAreaString) and allInside
                            end
                        end

                        if allInside and (not allDead) then 
                            self.ObjectiveRecall:OnResult(true)
                            break
                        end
                    end
                end
            end
        )

        -- construct the callback
        self.ObjectiveRecall:AddResultCallback(
            function(result)
                if result then
                    -- hooray!

                    -- add a bit more drama :).
                    ScenarioFramework.Dialogue(X04_M03_250, nil, false);

                    -- move the camera
                    for k, commander in commanders do 
                        if not commander.Dead then 
                            ScenarioFramework.EndOperationCamera(commander, true)
                            break
                        end
                    end

                    -- the end sequence
                    ScenarioFramework.CreateTimerTrigger(
                        function()

                            -- make the commanders invurnable, etc.
                            IssueClearCommands(commanders);
                            IssueStop(commanders);
                            for k, unit in commanders do
                                unit:SetUnSelectable(true);
                                unit:SetCanTakeDamage(false);
                            end

                            -- addin 'dem Bollywood styles.
                            local dialogues = { X03_M03_120, X04_M03_120, X04_M03_210, X04_M03_270 }
                            EventFunctionality.PlayRandomDialogue(dialogues);

                            -- start the warping effect on the commanders.
                            IssueClearCommands(commanders);
                            IssueStop(commanders);
                            for k, unit in commanders do
                                unit:PlayCommanderWarpInEffect();
                            end

                            -- ScenarioFramework.FakeTeleportUnit(...)?
                            -- start the final trigger - our winning trigger.
                            ScenarioFramework.CreateTimerTrigger(
                                function() 
                                    -- complete the protect objective.
                                    eventInfo.OtherEvents.ProtectCommanders.ObjectiveCommander:ManualResult(true);

                                    -- :(
                                    for k, unit in commanders do
                                        unit:Destroy();
                                    end

                                    -- all players won!!
                                    for k, player in eventInfo.StrPlayers do
                                        GetArmyBrain(player):OnVictory();
                                    end
                                end,
                                2.5,
                                true
                            );

                            -- this is the actual final trigger. Fooled ya!
                            ScenarioFramework.CreateTimerTrigger(
                                function()
                                    Game.End(true);
                                    UnlockInput()
                                end,
                                7.0,
                                false
                            );

                        end,
                        2.0,
                        false
                    );

                else
                    -- I don't think you can fail this objective. But here we
                    -- go either way!
                    for k, player in eventInfo.StrPlayers do
                        GetArmyBrain(player):OnDefeat();
                    end
                end
            end
        );
    end

end