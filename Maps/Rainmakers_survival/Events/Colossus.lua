
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

------------------ ------------------ ------------------ ------------------ ------------------                              
-- IMPORTS --
------------------ ------------------ ------------------ ------------------ ------------------ 

local Utilities = import(ScenarioInfo.path .. 'Utilities.lua')

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Ping = import(ScenarioInfo.path .. 'Functionality/Ping.lua');

local GroundFunctionality = import(ScenarioInfo.path .. 'GroundAI/GroundFunctionality.lua')
local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local EventFunctionality = import(ScenarioInfo.path .. 'Events/EventFunctionality.lua');

local PlatoonTinkers = import(ScenarioInfo.path .. 'Tinkers/Platoon.lua')

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

local Regions = import(ScenarioInfo.path .. "GroundAI/GroundSpawnRegions.lua");

------------------ ------------------ ------------------ ------------------ ------------------                              
-- TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local debug = false;

-- determines when we consider a local target a threat. Equals to the amount
-- of mass a threat is worth. If it is less, then we do not consider it 
-- a threat. To prevent us from chasing scouts :).
local threatValueThreshold = 1000;

-- determines the distance we'll search from the origin node on the
-- grid. If, say, the value is 3 then we'll keep track of a three node
-- distance in all directions from the origin node.
local threatSearchDistance = 4;

-- the objective title / description.
local objectiveTitle = "Colossus sighted";
local objectiveDescription = "An hostile Colossus has been sighted in your operational area. Neutralize it before it can reach the installations.";

-- the spawn offset of the platoons. Not quite interesting.
local spawnOffsetDistance = 15;

-- the speed multiplier for the experimental. If it moves too fast,
-- the guarding looks weird / fails.
local speedMultiplier = 0.82;

-- the amount of vision and the duration of that vision
-- that the players gain when the experimental spawns.
local visionRadius = 80;
local visionDuration = 40;

-- the dialogues for when the experimental spawns.
XGG_GD1_440 = { {text = '<LOC XGG_GD1_440_010>[{i HQ}]: Commander, an enemy Colossus has been sighted. HQ out.', vid = 'XGG_HQ_GD1_04185.sfd', bank = 'XGG', cue = 'XGG_HQ_GD1_04185', faction = 'NONE'},}
X01_M02_281 = { {text = '<LOC X01_M02_281_010>[{i HQ}]: Got a Galactic Colossus heading your way. Deal with it. HQ out.', vid = 'X01_HQ_M02_04889.sfd', bank = 'X01_VO', cue = 'X01_HQ_M02_04889', faction = 'NONE'},}
local dialoguesOnSpawn = { XGG_GD1_440, X01_M02_281 };

-- priorities for the experimental when it is doing its thing
local experimentalPrioritiesGeneric = { }
experimentalPrioritiesGeneric[1] = { categories.EXPERIMENTAL, categories.TECH3, categories.TECH2, categories.COMMAND, categories.ALLUNITS }
experimentalPrioritiesGeneric[2] = { categories.COMMAND, categories.SHIELD, categories.ALLUNITS }
experimentalPrioritiesGeneric[3] = { categories.SHIELD, categories.COMMAND, categories.ALLUNITS }

-- priorities for the experimental when it feels cornered
local experimentalPrioritiesPanic = { }
experimentalPrioritiesPanic[1] = {categories.EXPERIMENTAL, categories.COMMAND, categories.TECH3, categories.ALLUNITS - categories.SPECIALLOWPRI}
experimentalPrioritiesPanic[2] = {categories.COMMAND, categories.SHIELD, categories.ALLUNITS - categories.SPECIALLOWPRI}
experimentalPrioritiesPanic[3] = {categories.SHIELD, categories.COMMAND, categories.ALLUNITS - categories.SPECIALLOWPRI}

-- priorities for the platoons
local platoonPriorities = { categories.SHIELD, categories.DEFENSE * categories.DIRECTFIRE * categories.STRUCTURE, categories.ALLUNITS - categories.SPECIALLOWPRI}

------------------ ------------------ ------------------ ------------------ ------------------                              
-- NON-TWEAKABLE VALUES --
------------------ ------------------ ------------------ ------------------ ------------------ 

local army = ScenarioInfo.Armies.Platoon;

------------------ ------------------ ------------------ ------------------ ------------------                              
-- FUNCTIONALITY --
------------------ ------------------ ------------------ ------------------ ------------------ 

Event = { };
Event.__index = Event;

-- initialises the event.
function Event:Setup(spawnRegions, pRnd)

    -- linking
    event = { };
    setmetatable(event, Event);

    -- retrieve all event data, such as the experimental and the platoons.
    event.pRnd = pRnd;
    event.spawnRegions = spawnRegions;
    event.data = EventFunctionality.AnalyseExperimentalPlatoons("EC", army);

    return event;
end

-- eventInfo can contain:
-- eventInfo.OtherEvents        contains all the other events available in EventSetup
-- eventInfo.strPlayers         contains all the army strings of the players.
-- eventInfo.MakeObjective      determines whether an objective is created
-- eventInfo.MakeDialogue       determines whether an dialogue is played
-- eventInfo.MakeVision         determines whether vision is generated
function Event:Launch(eventInfo)

    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;
    local oppertunities = ScenarioInfo.GridObjective;

    -- determine the spawn locations
    local area = self.spawnRegions[self.pRnd:GetValue()];
    local count = table.getn(self.data.platoons)
    local spawns = Regions.PointsOnLongestAxis(area, count)

    -- determine time before spawning
    local holdup = {
        10, 0, 0, 10
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Construct the platoons

    local index = 0
    local platoons = { }
    local brain = GetArmyBrain(army);
    for k, bps in self.data.platoons do 
        -- store locally in each iteration, prevents losing k when using it in a forkthread
        index = index + 1;

        -- spawn the units
        local units = { };
        local point = spawns[index]
        for _, bp in bps do
            -- spawn the unit, then keep track of it
            local unit = CreateUnitHPR(bp, army, point[1], point[2], point[3], 0, 0, 0)
            table.insert(units, unit)
        end

        -- set their priorities
        Utilities.SetPriorities(units, platoonPriorities)

        -- turn it into a platoon
        local platoon = brain:MakePlatoon('', '')
        brain:AssignUnitsToPlatoon(platoon, units, 'Attack', 'AttackFormation')
        table.insert(platoons, platoon)

        -- make it tinker
        local lk = k;
        ForkThread(
            function(...)
                WaitSeconds(holdup[lk])
                PlatoonTinkers.MoveNearestHighOppertunity(unpack(arg))
            end,
            oppertunities,
            platoon
        );
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Construct the experimental

    WaitSeconds(15)

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Spawn the experimental and start its AI. Slow it down slightly such that the platoons can keep up.

    local experimental = GroundFunctionality.SpawnUnit(self.data.experimental, army, area.center);
    experimental:SetSpeedMult(speedMultiplier);

    function ExperimentalBehaviour(experimental, oppertunities)

        local target = nil;
        while not experimental.Dead do

            local position = experimental:GetPosition();

            -- check for local hazards
            local range = 30;
            local threshold = 100;
            local threats, hazardousness = hazards:FindLocalHazards(position, range, threshold);
            local count = table.getn(threats);

            if hazardousness > 2000 then 
                -- move the experimental away from the targets. Keep track of the fact that we're falling back.
                IssueClearCommands({experimental});
                local locationToEngage = PlatoonTinkers.ComputeEngageLocation(position, threats);
                IssueMove({experimental}, locationToEngage);
                LOG("Colossus is engaging local hazards.");
            end

            if experimental:IsIdleState() then 

                -- check if we got anything to go to
                if not oppertunities:OppertunitiesAvailable() then 
                    if oppertunities == ScenarioInfo.GridObjective then 
                        LOG("Colossus cannot find new oppertunities - switching to player oppertunities.");
                        oppertunities = ScenarioInfo.GridPlayers;

                    else
                        LOG("Colossus cannot find new oppertunities.");
                        break;
                    end
                end

                -- find a target 
                target = oppertunities:GetNearestHighOppertunity(position);

                if target then 
                    -- find a path
                    local options = { smoothen = true, force = 18000 };
                    local path = pathing:FindPath(position, target.position, options)

                    -- move in an aggressive, angry looking fashion
                    local count = table.getn(path);
                    IssueClearCommands({experimental});
                    for k = 1, count - 2, 1 do 
                        IssueMove({experimental}, path[k]);
                    end

                    -- get totally angry at everyone around it
                    IssueAggressiveMove({experimental}, target.position)
                    LOG("Colossus is on the move.");
                end
            else
                -- check if we should reconsider
                if target then 
                    local node = oppertunities:GetNode(target.position);
                    if node.oppertunity < 0.5 * target.oppertunity  then
                        -- if the target isn't what it was a few seconds ago, make the platoon stop
                        IssueClearCommands({experimental});
                        LOG("Colossus is reconsidering.");
                    end
                end
            end

            WaitSeconds(4.0);
        end

        LOG("Colossus died.");
    end

    ForkThread(ExperimentalBehaviour, experimental, oppertunities);

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- Give it a real campaign feeling to it.

    -- sound notification
    if eventInfo.MakeDialogue then
        EventFunctionality.PlayRandomDialogue(dialoguesOnSpawn);
    end

    -- objective notification
    if eventInfo.MakeObjective then
        self.ObjectiveColossus = Objectives.Kill(
            'secondary', 
            'incomplete', 
            objectiveTitle,        
            objectiveDescription, 
            {                          
                Units = { experimental },
                MarkUnits = true,
            }
        )
    end

    -- give temporarily vision.
    if eventInfo.MakeVision then
        for k, player in eventInfo.StrPlayers do
            ScenarioFramework.CreateVisibleAreaAtUnit(visionRadius, experimental, visionDuration, GetArmyBrain(player));
        end
    end

    -- ping it
    ScenarioFramework.CreateTimerTrigger(
        function() 
            local army = 1 
            local position = experimental:GetPosition()
            Ping.AttackPing(position, army)
        end,
        10,
        true
    );
end