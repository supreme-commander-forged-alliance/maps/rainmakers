
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

presets = 
{
	default = 2,
	sets = {
		{
			key = 1,
			label = 'up to 900',
			help = 'For inexperienced players.',
			options = {
				RainmakersObjectives = 2,
				RainmakersStartupTime = 1,
				RainmakersInitialStaticDefenses = 3,
				RainmakersStreamArtillery = 1,
				RainmakersStreamDirectFire = 2,
				RainmakersPlatoons = 1, 
				RainmakersMobileStealthFieldGens = 2,
				RainmakersMobileShieldGens = 2,
			}
		},
		{
			key = 2,
			label = '900 - 1300',
			help = 'For the average player in the community.',
			options = {
				RainmakersObjectives = 2,
				RainmakersStartupTime = 2,
				RainmakersInitialStaticDefenses = 3,
				RainmakersStreamArtillery = 2,
				RainmakersStreamDirectFire = 2,
				RainmakersPlatoons = 2, 
				RainmakersMobileStealthFieldGens = 2,
				RainmakersMobileShieldGens = 2,
			}
		},
		{
			key = 3,
			label = '1300 - 1600',
			options = {
				RainmakersObjectives = 3,
				RainmakersStartupTime = 2,
				RainmakersInitialStaticDefenses = 3,
				RainmakersStreamArtillery = 2,
				RainmakersStreamDirectFire = 3,
				RainmakersPlatoons = 2, 
				RainmakersMobileStealthFieldGens = 2,
				RainmakersMobileShieldGens = 1,
			}
		},
		{
			key = 4,
			label = '1600 and above',
			options = {
				RainmakersObjectives = 3,
				RainmakersStartupTime = 3,
				RainmakersInitialStaticDefenses = 3,
				RainmakersStreamArtillery = 2,
				RainmakersStreamDirectFire = 3,
				RainmakersPlatoons = 2, 
				RainmakersMobileStealthFieldGens = 2,
				RainmakersMobileShieldGens = 1,
			}
		}
	}
};

options =
{
	{
		default 	= 2,
		label 		= "Objectives",
		help 		= "Defines the overall difficulties for the primary and secondary objectives.",
		key 		= 'RainmakersObjectives',
		pref	 	= 'RainmakersObjectives',
		values 		= {
			{ text 	= "Easy", help = "All things considered, this is almost an holiday.", key = 1, },		
			{ text 	= "Normal", help = "The mission is as expected - there are no major hickups.", key = 2, },	
			{ text 	= "Heroic", help = "It's another day in the storm. Things are tough.", key = 3, },	
			{ text 	= "Legendary", help = "We haven't found a way to make it viable yet.", key = 4, },
		},	
	},

	{
		default 	= 1,
		label 		= "Commanders",
		help 		= "Defines how many commanders must survive the onslaught.",
		key 		= 'RainmakersCommanders',
		pref	 	= 'RainmakersCommanders',
		values 		= {
			{ text 	= "All must survive", help = "We leave no man behind.", key = 1, },		
			{ text 	= "One must survive", help = "The mission is all what matters, at any cost.", key = 2, },	
		},	
	},

	{
		default 	= 2,
		label 		= "Startup time",
		help 		= "Defines the the start up time players will be able to enjoy before the storm hits.",
		key 		= 'RainmakersStartupTime',
		pref 		= 'RainmakersStartupTime',
		values 		= {
			{ text 	= "Easy", help = "We'll have sufficient time to start building up our defense strategy.", key = 1, },		
			{ text 	= "Normal", help = "There's sufficient time - but we'll need to hurry up.", key = 2, },	
			{ text 	= "Heroic", help = "There's little time - no space for errors.", key = 3, },	
			{ text 	= "Legendary", help = "We're being dropped in the middle of it.", key = 4, },
		},	
	},

	{
		default 	= 3,
		label 		= "Initial defenses",
		help 		= "Determines how many defenses are in place when the commanders warp in.",
		key 		= 'RainmakersInitialStaticDefenses',
		pref 		= 'RainmakersInitialStaticDefenses',
		values 		= {
			{ text 	= "Few", help = "The core is protected.", key = 1, },	
			{ text 	= "Moderate", help = "The core and key positions are protected.", key = 2, },	
			{ text 	= "Many", help = "The core, key positions and expansion locations are protected.", key = 3, },
		},	
	},

	{
		default 	= 2,
		label 		= "Stream: Artillery",
		help 		= "Defines how many artillery units such artillery or missile launchers spawn in the stream.",
		key 		= 'RainmakersStreamArtillery',
		pref 		= 'RainmakersStreamArtillery',
		values 		= {
			{ text 	= "Few", 		help = "Every 5.70 seconds a unit will spawn.", key = 1, },
			{ text 	= "Moderate", 	help = "Every 4.70 seconds a unit will spawn.", key = 2, },
			{ text 	= "Many", 		help = "Every 3.70 seconds a unit will spawn.", key = 3, },
		},
	},
	{
		default 	= 2,
		label 		= "Stream: Direct fire",
		help 		= "Defines how many direct fire units such as tanks or bots spawn in the stream.",
		key 		= 'RainmakersStreamDirectFire',
		pref 		= 'RainmakersStreamDirectFire',
		values 		= {
			{ text 	= "Few", 		help = "Every 2.90 seconds a unit will spawn.", key = 1, },
			{ text 	= "Moderate", 	help = "Every 2.30 seconds a unit will spawn.", key = 2, },
			{ text 	= "Many", 		help = "Every 1.80 seconds a unit will spawn.", key = 3, },
		},
	},
	{
		default 	= 2,
		label 		= "Platoons",
		help 		= "Defines how many platoons will appear.",
		key 		= 'RainmakersPlatoons',
		pref 		= 'RainmakersPlatoons',
		values 		= {
			{ text 	= "Few", help = "They will be sparse.", key = 1, },
			{ text 	= "Moderate", help = "They are what to be anticipated.", key = 2, },
			{ text 	= "Many", help = "They will be numerous, but not unstoppable.", key = 3, },
		},
	},

	{
		default 	= 2,
		label 		= "Mobile stealth field generators",
		help 		= "Defines whether or not there will be mobile stealth field generators among some of the platoons.",
		key 		= 'RainmakersMobileStealthFieldGens',
		pref 		= 'RainmakersMobileStealthFieldGens',
		values 		= {
			{ text 	= "On", help = "Platoons may have additional stealth field generators - hiding them from radar.", key = 1, },
			{ text 	= "Off", help = "Platoons will not have additional stealth field generators.", key = 2, },
		},	
	},

	{
		default 	= 2,
		label 		= "Mobile shield generators",
		help 		= "Defines whether or not there will be mobile shield field generators among some of the platoons.",
		key 		= 'RainmakersMobileShieldGens',
		pref 		= 'RainmakersMobileShieldGens',
		values 		= {
			{ text 	= "On", help = "Platoons may have additional shield generators - reinforcing their numbers.", key = 1, },
			{ text 	= "Off", help = "Platoons will not have additional shield generators.", key = 2, },
		},	
	},
}
