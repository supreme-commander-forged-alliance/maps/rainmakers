
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- a simple binary search implementation that assumes numeric values within the table.
-- finds the closest index to it, rounded down.
function Search(t, v)

    function BinarySearch(t, v, l, u)

        -- end condition!
        if u - l == 1 then
            if v > t[u] then
                return u;
            else
                return l;
            end
        end
        
        -- take the middle of l, u.
        local m = math.floor((l + u) * 0.5);

        -- if v is bigger than the current middle.
        if t[m] > v then
            return BinarySearch(t, v, l, m);    -- search in lower half.
        else
            return BinarySearch(t, v, m, u);    -- otherwise, search in upper half.
        end
    end

    return BinarySearch(t, v, 1, table.getn(t));
end