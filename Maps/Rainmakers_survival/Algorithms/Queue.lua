
Queue = { };
Queue.__index = Queue;

-- initialises the queue.
function Queue:Initiate(func)

    local queue = { };
    setmetatable(queue, Queue);

    queue.Table = { };
    queue.Left = 1;
    queue.Right = 1;

    return queue;
end

-- adds the given element to the queue.
function Queue:Enqueue(element)
    self.Table[self.Right] = element;
    self.Right = self.Right + 1;
end

-- dequeues if the queue is not empty, returns nil otherwise.
function Queue:Dequeue()

    if self:IsEmpty() then
        error("Queue is empty.");
    end

    local element = self.Table[self.Left];
    self.Table[self.Left] = nil;
    self.Left = self.Left + 1;
    return element;
end

-- clears out the queue
function Queue:Clear()
    for k = self.Left, self.Right, 1 do 
        self.Table[k] = nil;
    end

    self.Left = 1;
    self.Right = 1;
end

-- checks whether the queue is empty
function Queue:IsEmpty()
    return self.Left == self.Right;
end

-- turns the queue into a table that starts at index 1
function Queue:ToTable()

    local table = { };
    for k = self.Left, self.Right do 
        table[k - self.Left + 1] = queue.Table[k];
    end

    return table;
end