
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- inspiration:
-- https://www.youtube.com/watch?v=c1TpLRyQJ4w (english)
-- https://www.youtube.com/watch?v=ijfPvX2qYOQ (english)
-- http://www.cs.uu.nl/docs/vakken/ds/HoorC/Heap.pdf (dutch)

MinHeap = { };
MinHeap.__index = MinHeap;

-- this heap can have any kind of object - we therefore need a compareto function that allows us to extract a number from the object.
-- we can then use this number to compare it to another object's number. This way we know which object is more 'minimal'.
function MinHeap:Initiate(func)

    local heap = { };
    setmetatable(heap, MinHeap);

    -- holds the function we can use to compare the objects.
    heap.compare = func;

    -- holds the actual heap.
    heap.heap = { };

    heap.size = 0;

    return heap;
end

function MinHeap:Empty()
    return self.size == 0;
end

function MinHeap:ExtractMin()

    -- if the heap is empty, we got nothing to return!
    if self.size == 0 then
        return nil;
    end

    -- keep a reference to the top value.
    local value = self.heap[1];

    -- put our highest value at the top.
    self.heap[1] = self.heap[self.size];
    self.heap[self.size] = nil;
    self.size = self.size - 1;

    -- fix its position.
    self:Heapify(1);

    return value;
end

function MinHeap:Heapify(index)

    -- find the left / right child and the parent.
    local parent = index;
    local left = self:ToLeftChild(index);
    local right = self:ToRightChild(index);

    -- find the best of the two, we assume the left child is.
    local min = left;

    --local int = (self.compare(self.heap[right]));
    --if int then
        --LOG("Value: " .. int);
    --end

    -- if there is a right child, then there always has to be a left child. Hence, we can now assume there's both a right and a left child.
    -- compare the two: if right is smaller, then assign min = right. Else, keep min on left.
    if self.heap[right] then
        if (self.compare(self.heap[right])) < (self.compare(self.heap[left])) then
            min = right;
        end
    end

    -- if there is a child, compare the lowest child (is given due to code above) with our parent. 
    -- If it's smaller, switch the parent and the lowest child.
    if self.heap[min] then
        if (self.compare(self.heap[min])) < (self.compare(self.heap[parent])) then
            -- swap the two values.
            self:Swap(parent, min);

            -- check if the (parent) value that is now at the child position is at the correct position within the heap.
            self:Heapify(min);
        end
    end
end

-- swaps the two given indices.
function MinHeap:Swap(a, b)
    local l = self.heap[a];
    self.heap[a] = self.heap[b];
    self.heap[b] = l;
end

-- this function is not useful in our cases. Do not use it!
function MinHeap:BuildHeap(listOfObjects)
    self.size = table.length(listOfObjects);
    self.heap = listOfObjects;
    -- n >> 1 = n / 2
    for i = 1, self.size >> 1 do 
        self:Heapify(i);
    end
end

function MinHeap:Insert(object)
    if object then
        self.size = self.size + 1;
        self.heap[self.size] = object;
        self:Rootify(self.size);
    else
        WARN("given object to heap was nil!");
    end
end

function MinHeap:Rootify(index)

    local parent = self:ToParent(index);
    local current = index;

    if (current == 1) or self.compare(self.heap[parent]) < self.compare(self.heap[current]) then
        return;
    end

    self:Swap(parent, current);
    self:Rootify(parent);
end

-- given an index, this will take us to the parent index.
function MinHeap:ToParent(n)
    -- n / 2
    return n >> 1;
end

-- given an index, this will take us to the right child index.
function MinHeap:ToRightChild(n)
    -- 2 * n + 1
    return (n << 1) | 1;
end

-- given an index, this will take us to the left child index.
function MinHeap:ToLeftChild(n)
    -- 2 * n
    return n << 1;
end