
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

-- we assume grid vectors.
function Path (o, d)

    local path = { };
    
    -- compute the perpendicular vector from o to d.
    local direction = GeneralFunctionality.VectorSubtract(d, o);
    local normalized = GeneralFunctionality.VectorNormalize(direction);
    local perpendicular = GeneralFunctionality.Vector2DCross(normalized);

    -- compute the length from o to d.
    local length = GeneralFunctionality.VectorLength(direction);

    -- take a random point between o and d, called wp with an offset depending on the value r and the perpendicular vector.
    local r = Random();

    local pr;
    if r > 0.5 then
        pr = 0.5 - r;
    else
        pr = r;
    end

    local wpd =  GeneralFunctionality.VectorScalarMultiply(normalized, r * length * 0.5);
    local wpp = GeneralFunctionality.VectorScalarMultiply(perpendicular, pr * length * 0.5);
    local wp = GeneralFunctionality.VectorAddition(o, GeneralFunctionality.VectorAddition(wpd, wpp));

    return { wp, d };
end