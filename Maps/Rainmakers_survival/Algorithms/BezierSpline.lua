
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/rainmakers
-- 
-- Please do not remove this message when modifying the map.
--------------------------------------------------------------------------------

-- https://pomax.github.io/bezierinfo/
-- cs.uu.nl/ddm

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');

-- generate an approximation of the bezier curve.
function SmoothPath(path)

    local length = table.getn(path);
    if length == 1 then
        return path;
    end

    local adjustedLength = length;
    local smoothPath = { };
    for k = 1, adjustedLength do
        local factor = k / adjustedLength;
        local smoothPoint = SmoothPathPoint(path, factor);
        table.insert(smoothPath, smoothPoint);
    end

    return smoothPath;
end

-- find the point at t on the bezier curve.
function SmoothPathPoint(path, t)

    local length = table.getn(path);
    if length == 1 then
        return path[1];
    end

    local smoothPath = { };
    for k = 1, length - 1  do
        local cp = path[k];
        local np = path[k + 1];
        table.insert(smoothPath, GeneralFunctionality.Vector((1-t) * cp[1] + t * np[1], 0, (1-t) * cp[3] + t * np[3]))
    end

    return SmoothPathPoint(smoothPath, t);
end