

local GridFramework = import(ScenarioInfo.path .. 'GroundAI/GroundGrid.lua');

local GeneralFunctionality = import(ScenarioInfo.path .. 'GeneralFunctionality.lua');
local GroundFunctionality = import(ScenarioInfo.path .. 'groundAI/GroundFunctionality.lua')
local Utilities = import(ScenarioInfo.path .. 'Utilities.lua')

local ScenarioFramework = import('/lua/ScenarioFramework.lua')

-- categories for various types of units
local catArts = categories.ARTILLERY + 
(categories.MOBILE * categories.SILO * categories.INDIRECTFIRE) + 
categories.xal0305 + categories.xsl0305;
local catShields = categories.SHIELD;
local catStealths = categories.url0306;
local catGenerics = categories.LAND - (catArts);

local showRange = false;

-- checks whether or not the given platoon is still fully alive.
function PlatoonIsAlive(platoon)

    -- go over every unit: if one is alive then so is the platoon.
    units = platoon:GetPlatoonUnits();
    for k, unit in units do
        if not unit.Dead then
            return true
        end
    end

    return false
end

-- checks whether or not the given platoon is completely idle.
function PlatoonIsIdle(platoon)

    local idle = false;

    -- check if there are idle units.
    local units = platoon:GetPlatoonUnits();
    for k, unit in units do
        if not unit.Dead and unit:IsIdleState() then
            idle = true;
            break;
        end
    end

    return idle;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Computes the value of the platoon, mass wise. Does not take into account health.

function PlatoonValue(platoon)
    local max = 0
    local total = 0

    local units = platoon:GetPlatoonUnits()
    for k, unit in units do 
        if not unit.Dead then
            local mass = unit:GetBlueprint().Economy.BuildCostMass
            max = math.max(max, mass)
            total = total + mass
        end
    end

    return total, max
end

--- Returns the largest mass value in the platoon
function PlatoonLargestIndividualValue(platoon)
    local mass = 0

    local units = platoon:GetPlatoonUnits();
    for k, unit in units do 
        if not unit.Dead then
            local value = unit:GetBlueprint().Economy.BuildCostMass
            if value > mass then 
                mass = value
            end
        end
    end

    return mass;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- finds the weapon on a unit with the most range. If no weapons, range will be 0.

local validWeaponCategories = {
    'Direct Fire',
    'Artillery',
    'Missile'
}

local rangeMultipliers = { };
rangeMultipliers['Direct Fire'] = 1.25;
rangeMultipliers['Artillery'] = 0.95;
rangeMultipliers['Missile'] = 0.8;

function FindWeaponWithMostRange(unit)

    local range = 0;

    local weapons = unit:GetBlueprint().Weapon;
    if weapons then 
        for k, weapon in weapons do 
            -- check if it is the right category, e.g., don't take 'Anti Air' into account
            local category = weapon.WeaponCategory;
            if table.find(validWeaponCategories, category) then 
                -- multiply it to tweak the range slightly
                local distance = weapon.MaxRadius * rangeMultipliers[category];
                if distance > range then 
                    range = distance;
                end
            end
        end
    end

    return range;
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Computes the weapon range of the platoon, apply a correction for larger platoons to take
-- into account the size of the platoon

function PlatoonRange(platoon)

    local range = 0;
    local units = platoon:GetPlatoonUnits();
    local count = table.getn(units);

    for k, unit in units do 
        range = math.max(range, FindWeaponWithMostRange(unit));
    end

    return range + count * 0.20;

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Adds a trigger to every unit within the platoon in case the platoon is damaged somehow

function PlatoonUnitDamagedTrigger(platoon, trigger, amount)
    local units = platoon:GetPlatoonUnits();
    for k, unit in units do 
        ScenarioFramework.CreateUnitDamagedTrigger(trigger, unit, amount);
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- Adds a trigger to every unit within the platoon in case a unit in the platoon dies somehow

function PlatoonUnitDeathTrigger(platoon, trigger)
    local units = platoon:GetPlatoonUnits();
    for k, unit in units do 
        ScenarioFramework.CreateUnitDeathTrigger(trigger, unit);
    end
end

-- used for shields to find the nearest unit
function NearestUnit(origin, units)

    -- with no units there is no nearest unit
    local numberOfUnits = table.getn(units);
    if numberOfUnits == 0 then
        error ("No units provided.");
    end

    function ComputeSquaredDistance(p1, p2)
        local dx = p1[1] - p2[1];
        local dz = p1[3] - p2[3];

        return dx * dx + dz * dz;
    end

    local nearest = 1;
    local distance = ComputeSquaredDistance(origin:GetPosition(), units[1]:GetPosition());

    -- and then find the actual nearest unit
    for k = 2, numberOfUnits, 1 do 

        -- compute distance to unit
        local unit = units[k];
        local unitDistance = ComputeSquaredDistance(origin:GetPosition(), unit:GetPosition());

        -- determine if it is closer
        if distance > unitDistance then 
            nearest = k;
            distance = unitDistance;
        end
    end

    -- return the nearest unit
    return units[nearest];

end

function RandomPointAroundTargetAtEdge(target, distance)

    local ox = Random() - 0.5;
    local oz = Random() - 0.5;

    local d = math.sqrt (ox * ox + oz * oz);
    local dinv = 1.0 / d;

    local nx = dinv * ox;
    local nz = dinv * oz;
    local r = Random() * distance;

    local point = target.position;
    return {
        point[1] + nx * r, 
        point[2], 
        point[3] + nz * r
    };
    
end

-- TODO: this function only used target.hazard values, quick fix was to add target.oppertunity values. Function is used both by
-- checking for hazards and oppertunities
function ComputeEngageLocation(position, targets)

    -- take into account distance?

    -- check if we have any targets at all
    if table.getn(targets) == 0 then
        WARN("ComputeEngageVector: Computing the engage vector over no targets.");
        return position;
    end

    -- compute total hazard value
    local totalValue = 0;
    local values = { };
    for k, target in targets do 
        local value = target.hazard + target.oppertunity;
        table.insert(values, value)

        totalValue = totalValue + value;
    end

    -- compute the largest distance (platoon -> target)
    local largestDistance = 0;
    local distances = { };
    for k, target in targets do 
        local d = VDist2(position[1], position[3], target.position[1], target.position[3]);
        table.insert(distances, d);

        if d > largestDistance then
            largestDistance = d;
        end
    end

    -- add up all directions.
    local direction = { 0, 0, 0 };
    for k, target in targets do   
        -- compute the direction
        local d = GeneralFunctionality.VectorSubtract(target.position, position);

        -- compute how much we should take this target into account
        local hoMultiplier = values[k] / totalValue;
        local dMultiplier = 1.0 - (0.4 * distances[k] / largestDistance)

        -- scale and add it
        local s = GeneralFunctionality.VectorScalarMultiply(d,  2.0 * hoMultiplier * dMultiplier);
        direction = GeneralFunctionality.VectorAddition(direction, s);
    end


    -- return the final location to move to
    return GeneralFunctionality.VectorAddition(position, direction);
end

function EngageBehaviorA(platoon, target)

    -- find the various types of units within the platoon
    local units = platoon:GetPlatoonUnits();
    local art = EntityCategoryFilterDown(catArts, units);
    local generic = EntityCategoryFilterDown(catGenerics, units);
    local numberOfGenerics = table.getn(generic);

    -- clear all previous orders
    IssueClearCommands(units);

    -- move all the generic units randomly (e.g., spread out)
    if numberOfGenerics > 5 then
        for k, unit in generic do 
            local point = GeneralFunctionality.RandomPointAroundTarget(target, numberOfGenerics);
            IssueMove({unit}, point);
        end
    end

    -- huurrr, we're so aggressive!
    IssueAggressiveMove(art, target);
    IssueAggressiveMove(generic, target);

end

-- the platoon stops moving to allow artillery to fire
-- all other land units guard
function PlatoonKeepDistance(platoon)

end

-- all artillery are on aggressive move
-- all other land units move in
function PlatoonAggressive(platoon)

end

-- all units move in, losing formation
-- once in position, they target high priority targets
function PlatoonSnipe(platoon)

end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to the nearest high oppertunity target.

function MoveNearestHighOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetNearestHighOppertunity(position);
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to the nearest mediocre oppertunity target.

function MoveNearestMediocreOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetNearestMediocreOppertunity(position);
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to the nearest low oppertunity target.

function MoveNearestLowOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetNearestLowOppertunity(position);
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to a random high oppertunity target.

function MoveHighOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetHighOppertunity();
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to a random mediocre oppertunity target.

function MoveMediocreOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetMediocreOppertunity();
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Moves the platoon to a random low oppertunity target.

function MoveLowOppertunity(oppertunities, platoon)
    function funcTarget (oppertunities, position) 
        return oppertunities:GetLowOppertunity();
    end

    Move(funcTarget, oppertunities, platoon);
end

------------------ ------------------ ------------------ ------------------ ------------------   
-- Keeps formation while on the move. Loses formation when the platoon is stuck or is engaging.

function Move(funcTarget, oppertunities, platoon)

    -- as defined in GroundSetup.lua
    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;
    local oppertunities = oppertunities;

    -- used to determine when the value and range of the platoon
    -- should be computed again.
    local update = 4;
    local updateAt = 4;

    local range = 0;
    local value = 0;
    local target = nil;
    local busyTicks = 0;

    -- when the platoon is stuck, behavior is changed
    local index = 1;
    local stuck = false;
    local previousPosition = nil;

    -- debugging
    if showRange then 
        ForkThread(
            function() 
                while PlatoonIsAlive(platoon) do 
                    WaitSeconds(0.1);
                    if previousPosition then
                        DrawCircle(previousPosition, range, "ffffff");
                    end
                end
            end
        );
    end

    -- add in a damage trigger
    PlatoonUnitDamagedTrigger(
        platoon, 
        function(self, instignator)
            -- are we as a platoon already busyTicks with something?
            if busyTicks <= 0 then 
                -- engage the attacker, if it still lives
                if not instignator.Dead then 
                    busyTicks = 6
                    local position = instignator:GetPosition()
                    EngageBehaviorA(platoon, position)
                    LOG("A platoon got engaged on")
                end
            end
        end,
        0.6
    )

    while PlatoonIsAlive(platoon) do

        ------------------ ------------------ ------------------ ------------------ ------------------   
        -- keep track of platoon status

        local position = platoon:GetPlatoonPosition();
        local units = platoon:GetPlatoonUnits();
        local count = table.getn(units);

        update = update + 1;
        if update >= updateAt then
            update = 0;

            range = 1.45 * PlatoonRange(platoon);
            value = 0.5 * PlatoonValue(platoon); 

            ------------------ ------------------ ------------------ ------------------ ------------------   
            -- check if the platoon is stuck, this can happen when they walk close to elevations

            if not stuck then
    
                -- do we have a previous position?
                if previousPosition then 
                    stuck = VDist2Sq(position[1], position[3], previousPosition[1], previousPosition[3]) < 0.1;
                end

                -- keep track of where we were
                previousPosition = position;
    
                if stuck then
                    LOG("A platoon got stuck.");
                    platoon:Stop();
                end
            end
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- do all kinds of surrounding checks.

        busyTicks = busyTicks - 1;

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- check for local oppertunities

        if busyTicks <= 0 then 

            -- check our surroundings
            local ops = oppertunities:FindLocalOppertunities(position, range, value);

            -- act on them
            if table.getn(ops) > 0 then 
                busyTicks = 6;

                -- determine where we'll engage
                local location = ComputeEngageLocation(position, ops);

                -- engaaaggeee!!
                EngageBehaviorA(platoon, location);

                -- wait a bit to allow the engagement to happen
                WaitSeconds(9.0);

                LOG("A platoon is engaging an oppertunity.");
            end 
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- check for local hazards

        if busyTicks <= 0 then 

            -- check our surroundings
            local threats = hazards:FindLocalHazards(position, range, value);

            -- act on them
            if table.getn(hazards) > 0 then  
                busyTicks = 6;

                -- determine where we'll engage
                local location = ComputeEngageLocation(position, threats);

                -- engaaaggeee!!
                EngageBehaviorA(platoon, location);

                -- wait a bit to allow the engagement to happen
                WaitSeconds(9.0);

                LOG("A platoon is engaging a hazard.");
            end
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- check for global targeting / pathing

        if busyTicks <= 0 then 

            if PlatoonIsIdle(platoon) then

                LOG("A platoon is idle.");

                -- check if we got anything to go to
                if not oppertunities:OppertunitiesAvailable() then 
                    if oppertunities == ScenarioInfo.GridObjective then 
                        LOG("Platoon cannot find new oppertunities - switching to player oppertunities.");
                        oppertunities = ScenarioInfo.GridPlayers;

                    else
                        LOG("Platoon cannot find new oppertunities.");
                        break;
                    end
                end

                -- we got something, go and find out where we want to go
                target = funcTarget(oppertunities, position);
    
                if target then 
                
                    local options = {
                        force = value,
                        smoothen = true,
                        hazardMultiplier = 1.0
                    }
                    
                    -- find our path towards it, returns a list of positions
                    local path = pathing:FindPath(position, target.position, options)

                    -- move over the path in a correct fashion
                    local first = 2;
                    local last = table.getn(path) - 1;
                    Utilities.MovePlatoon(platoon, path, "GrowthFormation", last, first);
    
                    -- move to our actual destination as our last position
                    IssueAttack(units, target.position);
                end
            else
    
                ------------------ ------------------ ------------------ ------------------ ------------------                              
                -- re-evaluate the target
    
                if target then 
                    local node = oppertunities:GetNode(target.position);
                    if node.oppertunity < 0.5 * target.oppertunity  then
                        -- if the target isn't what it was a few seconds ago, make the platoon stop
                        LOG("A platoon is reconsidering.");
                        platoon:Stop();
                    end
                end
            end
        end

        -- check for local targets
        WaitSeconds(2.0);
    end

    LOG("A platoon died.");
end

-- all units move in, keeping formation
function PlatoonMoveFormation (platoon)

end

-- TODO: refactor
function GuardUnit(platoon, unit, distance)

    -- as defined in GroundSetup.lua
    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;

    -- used to determine when the value and range of the platoon
    -- should be computed again.
    local update = 4;
    local updateAt = 4;

    local range = 0;
    local value = 0;

    local previousPosition = nil;

    -- debugging: do we show the range?
    if showRange then 
        ForkThread(
            function() 
                while PlatoonIsAlive(platoon) do 
                    WaitSeconds(0.1);
                    if previousPosition then
                        DrawCircle(previousPosition, range, "ffffff");
                    end
                end
            end
        );
    end

    while PlatoonIsAlive(platoon) do

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- check if our unit is still alive (e.g., that we properly guarded it)
        -- if not, turn into a regular move platoon that aims for objectives

        if unit.Dead then
            MoveNearestHighOppertunity(ScenarioInfo.GridObjective, platoon);
            break;
        end

        ------------------ ------------------ ------------------ ------------------ ------------------   
        -- keep track of platoon status

        local position = platoon:GetPlatoonPosition();
        previousPosition = position;

        update = update + 1;
        if update >= updateAt then
            update = 0;
            range = 1.75 * PlatoonRange(platoon);
            value = 0.25 * PlatoonValue(platoon); 
        end

        ------------------ ------------------ ------------------ ------------------ ------------------   
        -- check if we moved too far from the target
        local guardee = unit:GetPosition()
        if VDist2Sq (previousPosition[1], previousPosition[3], guardee[1], guardee[3]) > 2.5 * (distance * distance) then 
            -- stop our endeavor
            platoon:Stop();

            -- find our way back to the gaurded
            local target = GeneralFunctionality.RandomPointAroundTarget(unit:GetPosition(), distance);
            Utilities.MovePlatoon(platoon, {target}, "GrowthFormation");

            WaitSeconds(8.0);
            LOG("A platoon is heading back to the guarded.")
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- find local hazards

        local threats, hazardousness = hazards:FindLocalHazards(position, range, value);
        local count = table.getn(threats);

        if count == 0 then
            if PlatoonIsIdle(platoon) then
                local target = GeneralFunctionality.RandomPointAroundTarget(unit:GetPosition(), distance);
                Utilities.MovePlatoon(platoon, {target}, "GrowthFormation");
            end
        else
            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- Hostiles are enclosing!
            
            -- determine where we'll engage
            local target = ComputeEngageLocation(position, threats);

            -- engaaaggeee!!
            EngageBehaviorA(platoon, target);

            -- wait a bit to allow the engagement to happen
            WaitSeconds(12.0);
        end

        -- check for local targets
        WaitSeconds(5.0);
    end

end

-- TODO: refactor
function GuardLocation(platoon, location, keepGuarding, guardDistance)

    -- as defined in GroundSetup.lua
    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;

    -- used to determine when the value and range of the platoon
    -- should be computed again.
    local update = 4;
    local updateAt = 4;

    local range = 0;
    local value = 0;

    local previousPosition = nil;

    -- debugging: do we show the range?
    if showRange then 
        ForkThread(
            function() 
                while PlatoonIsAlive(platoon) do 
                    WaitSeconds(0.1);
                    if previousPosition then
                        DrawCircle(previousPosition, range, "ffffff");
                    end
                end
            end
        );
    end

    while PlatoonIsAlive(platoon) do

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- check if we still should guard the location

        if not keepGuarding() then
            MoveNearestHighOppertunity(ScenarioInfo.GridObjective, platoon);
            break;
        end

        ------------------ ------------------ ------------------ ------------------ ------------------   
        -- keep track of platoon status

        local position = platoon:GetPlatoonPosition();
        previousPosition = position;

        update = update + 1;
        if update >= updateAt then
            update = 0;
            range = 1.75 * PlatoonRange(platoon);
            value = 0.25 * PlatoonValue(platoon); 
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- find local hazards

        local threats = hazards:FindLocalHazards(position, range, value);
        local count = table.getn(threats);

        if count == 0 then

            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- there are no local hazards

            -- if we're idle
            if PlatoonIsIdle(platoon) then
                -- find a new target and move the platoon towards that target
                local target = GeneralFunctionality.RandomPointAroundTarget(location, guardDistance);
                Utilities.MovePlatoon(platoon, {target}, "GrowthFormation");
            end
        else
            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- Hostiles are enclosing!
            
            -- determine where we'll engage
            local target = ComputeEngageLocation(position, threats);

            -- engaaaggeee!!
            EngageBehaviorA(platoon, target);

            -- wait a bit to allow the engagement to happen
            WaitSeconds(12.0);
        end

        -- check for local targets
        WaitSeconds(2.0);
    end

end

--- The platoon will guard the location indefnitely.
-- @param platoon The platoon to guard the location.
-- @param formation The formation applied to the platoon, defaults to 'GrowthFormation'.
-- @param location The location to guard.
-- @param distance The distance the platoon can move away from the location
-- @param multipliers Various multipliers that can be used to customize the platoon:
--          range Used to influence the seeking range of the platoon, defaults to 1.5.
--          max Used to influence the maximum individual mass value of the platoon. Any threat smaller than this value will not be considered in the threat analysis. Defaults to 0.3.
--          total used to influence the total mass value of the platoon. Is used as a threshold as to when the platoon engages. Defaults to 0.5.
function GuardLocationIndefinite(platoon, formation, location, distance, multipliers)

    -- default values
    if not multipliers then 
        multipliers = { }
    end

    multipliers.range = multipliers.range or 1.5
    multipliers.max = multipliers.max or 0.3
    multipliers.total = multipliers.total or 0.5

    -- as defined in GroundSetup.lua
    local pathing = ScenarioInfo.Pathing;
    local hazards = ScenarioInfo.GridThreats;

    -- used to determine when the value and range of the platoon
    -- should be computed again.
    local update = 4;
    local updateAt = 4;

    local range = 0;
    local max = 0;
    local total = 0;

    local previousPosition = false;

    while PlatoonIsAlive(platoon) do

        ------------------ ------------------ ------------------ ------------------ ------------------   
        -- keep track of platoon status

        local position = platoon:GetPlatoonPosition();

        update = update + 1;
        if update >= updateAt then
            update = 0;

            -- compute the seek / check range of the platoon
            range = multipliers.range * PlatoonRange(platoon)

            -- compute the maximum individual and total value of the platoon
            max, total = PlatoonValue(platoon)
            total = multipliers.total * total 
            max = multipliers.max * max
        end

        ------------------ ------------------ ------------------ ------------------ ------------------                              
        -- find local hazards

        local threats, hazardousness = hazards:FindLocalHazards(position, range, max);

        if hazardousness < total then

            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- there are no significant local hazards

            if PlatoonIsIdle(platoon) then
                -- find a new target and move the platoon towards that target
                local target = GeneralFunctionality.RandomPointAroundTarget(location, distance);
                Utilities.MovePlatoon(platoon, {target}, formation);
            end
        else
            ------------------ ------------------ ------------------ ------------------ ------------------                              
            -- Hostiles are enclosing!
            
            -- determine where we'll engage
            local target = ComputeEngageLocation(position, threats);

            -- engaaaggeee!!
            EngageBehaviorA(platoon, target);

            -- wait a bit to allow the engagement to happen
            WaitSeconds(12.0);
        end

        -- check for local targets
        WaitSeconds(2.0);
    end

end