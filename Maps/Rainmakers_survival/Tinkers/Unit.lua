
local ScenarioFramework = import('/lua/ScenarioFramework.lua');

local Wander = import(ScenarioInfo.path .. 'Algorithms/WanderPath.lua');

------------------ ------------------ ------------------ ------------------ ------------------                              
-- units move casually towards their target

function ForkMove(oppertunities, unit)

    local switched = false

    -- merely moving for these type of units make no sense
    local categorySiege = categories.ARTILLERY + 
        (categories.MOBILE * categories.SILO * categories.INDIRECTFIRE); -- + 
        --categories.XAL0305 + categories.XSL0305;
    if EntityCategoryContains(categorySiege, unit) then
        ForkMoveAggressive(oppertunities, unit);
        return;
    end

    WaitSeconds(1.0);

    local unitTable = { unit };

    -- as long as the unit is not dead
    while not unit.Dead do

        -- if the unit is idle
        if unit:IsIdleState() then

            -- check if we got anything to go to
            if not oppertunities:OppertunitiesAvailable() then 

                -- we already switched previously
                if switched then
                    LOG("Unit cannot find new oppertunities.");
                    unit:Kill()
                end

                if oppertunities == ScenarioInfo.GridObjective then 
                    LOG("Unit cannot find new oppertunities - switching to player oppertunities.");
                    oppertunities = ScenarioInfo.GridPlayers;
                    switched = true
                else
                    LOG("Unit cannot find new oppertunities - switching to objective oppertunities.");
                    oppertunities = ScenarioInfo.GridObjective
                    switched = true
                end
            end

            -- determine some path that is different for all units
            local origin = unit:GetPosition();
            local target = oppertunities:GetNearestLowOppertunity(origin);

            if target then 
                local destination = target.position;
                local path = Wander.Path(origin, destination);
                for k, node in path do 
                    IssueMove(unitTable, node);
                end
            end
        end

        WaitSeconds(15.0);
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- units attack the target until the target is destroyed. Then they migrate into the
-- 'ForkMoveAggressive' behavior. Useful for walls.

function ForkTarget(oppertunities, unit, target, onDeath)

    WaitSeconds(1.0);

    -- issue to attack the target
    IssueAttack({ unit }, target);

    -- when the target dies: become 'typically' aggressive
    ScenarioFramework.CreateUnitDeathTrigger(
        function () ForkThread(ForkMoveAggressive, oppertunities, unit); end, 
        target
    );

end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- units stand still once their target is within fire range

function ForkMoveAggressive(oppertunities, unit)

    WaitSeconds(1.0);

    local unitTable = { unit };

    -- as long as the unit is not dead
    while not unit.Dead do

        -- if the unit is idle
        if unit:IsIdleState() then

            -- check if we got anything to go to
            if not oppertunities:OppertunitiesAvailable() then 
                if oppertunities == ScenarioInfo.GridObjective then 
                    LOG("Unit cannot find new oppertunities - switching to player oppertunities.");
                    oppertunities = ScenarioInfo.GridPlayers;

                else
                    LOG("Unit cannot find new oppertunities.");
                    break;
                end
            end

            -- determine some path that is different for all units by
            -- wandering off a bit
            local origin = unit:GetPosition();
            local target = oppertunities:GetNearestMediocreOppertunity(origin);

            if target then 
            local destination = target.position;
                local path = Wander.Path(origin, destination);
                for k, node in path do 
                    IssueAggressiveMove(unitTable, node);
                end 
            end
        end

        WaitSeconds(15.0);
    end
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- units (engineers?) may build buildings at the edge of threats

function ForkBuild(oppertunities, unit)

    -- one that cannot build, cannot build
    local categoryBuild = categories.ENGINEER + categories.COMMAND;
    if not EntityCategoryContains(categoryBuild, unit) then
        ForkMoveAggressive(oppertunities, unit);
        return;
    end

    -- todo
    ForkMove(oppertunities, unit);
end

------------------ ------------------ ------------------ ------------------ ------------------                              
-- units look around to find targets, may be too expensive

function ForkMoveSensor(oppertunities, unit)

    -- todo
    ForkMove(oppertunities, unit);

end