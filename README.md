# Rainmakers

![](/images/impression.png)

### Trailers
A trailer made by me, with music from Blodir:
 * https://youtu.be/1zPusXvOIxk

A trailer made by a friend, WasserMelon:
 * https://www.youtube.com/watch?v=fup7l7H8CQ8&feature=youtu.be

See also the channel of Wassermelon for more content:
 * https://www.youtube.com/channel/UCwKHJCmwDSfWswdeBEYsZlA

See Blodirs Soundcloud for more music:
 * https://soundcloud.com/pyry-m-kinen/a

### The story
Two high quality artillery installations provide support fire for a decisive operation in the south east. The enemy will start to triangulate the artillery installations the moment the first shells hit the ground. Their forces will be upon them right after.

You will warp in to protect the artillery installations until the operation is complete.

### Quotes from people
 * Ryfun: Made me love faf again beyond setons
 * Transandclips: thats just ridiculous, but good

### In-game user settings
This map contains various settings:
 * Objectives: The difficulty of the objectives: Easy, Normal, Heroic or Legendary.
 * Startup time: How much time you have before the enemy triangulates you: Easy, Normal, Heoric or Legendary.
 * Initial defenses: The amount of static defenses that are already on scene: Few, Moderate or Many.
 * Artillery Stream: The amount of mobile artillery that is send to you in a streaming fashion: Few, Moderate or Many.
 * Direct Stream: The amount of tanks, bots and other non-artillery units that is send to you in a streaming fashion: Few, Moderate or Many. 
 * Platoons: The amount of platoons that is send to you: Few, Moderate or Many
 * Mobile Stealth Field Generators: Whether or not platoons can spawn with Mobile Stealth Field Generators.
 * Mobile Shield Generators: Whether or not platoons can spawn with Mobile Shield Generators.

### Useful facts about the mod
The following units and / or buildings are disabled:
 * Naval units / buildings
 * Air units / buildings
 * Ground to air units / buildings

There are several generators that produce units:
 * Anti wall stream: A stream of units that will specifically attack walls. Walls are allowed, but in turn are a threat to the pathing of the AI. Therefore these units will attempt to take out a few wall segments, introducing gaps. Units are of type artillery or missile launcher. Units are spawned roughly every minute.
 * Anti shield stream: A stream of units that will specifically attack shields. Units are of type artillery or missile launcher. Units are spawned roughly every minute.
 * Artillery stream: A generic artillery stream. Units are of type artillery of missile launcher. The difficulty of this stream can be adjusted in the settings.
 * Direct fire stream: A generic stream of units. Units are of 
Take note that walls are not disabled. This fact can be abused by the players and the map will take that into account. There is a separate stream that aims to destroy wall sections.

There are two seperate AI's that are trying to destroy the installations:
 * Stream: Colored as orange. The stream sends units from all directions in a constant manner.
 * Platoon: Colored as bright yellow. The platoon sends concentration of units from all directions in a constant, but significantly slower, manner.
 
Platoons spawned by the map have their own AI to determine their objective. This can range from any high, mediocre or low level target between the players and the artillery installations.

Experimentals are spawned and gaurded by platoons. Experimentals of the AI's move slightly slower than regularly. All experimentals have their own behaviour:
 - The Monkey Lord marches straight in.
 - The Galactic Colossus attacks from a distance.
 - The Ythotha attacks from a distance, until his health is lower than 50%. Then he'll walk straigth in.
 - The Fatboy attacks from a distance. If a significant enough force is near, the Fatboy will hold its position. If a significant force is too close, the Fatboy will retreat. Two platoons of Titans are guarding the Fatboy.

Not all experimentals will be spawned if the objective time is short. HQ will inform you and give intel on where an experimental has spawned.

The map contains several secondary objective:
 - Mass: Significant mass has been found on-site. The mass is guarded and being reclaimed by the enemies. Secure it and reclaim the mass for your own.
 - Radar: Due to an error the radar schematic is missing. Upgrade the local radar in order to provide additional intel of your surroundings.
 - Launcher: A secret UEF operative base that is on-site has been compromised. Secure or destroy the base before the hostiles take over the launcher and use it against you.

### Installation guide
The standalone game Supreme Commander: Forged Alliance is required. This map / modification does not support the base game Supreme Commander. You do not need the base game.

Clone or download this repository. If downloaded, unzip the download. Open a new window and navigate towards your _My Documents_ folder. From there, navigate to: 
``` sh
".../My Documents/My Games/Gas Powered Games/Supreme Commander: Forged Alliance/"
```
If the _Maps_ folder does not exist, create one. Merge the _Maps_ folder of the .zip with your local _Maps_ folder.

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
